# Guest Desk
##

Guest Desk adalah aplikasi web yang di tujukan untuk membantu Receptionis me-monitor, me-manage session dari tamu yang melakukan visiting ke site.
dengan aplikasi ini, receptionis juga bisa membantu secara manual, jika tamu tidak membawa mobile device untuk dapat mengakses aplikasi Guest Log Book yang di sediakan.
Aplikasi ini berbasis web sehingga receptionist bisa dengan mudah mengakses via browser di komputer yang di sediakan di meja penerimaan tamu masing-masing site. 

## Demo 
untuk demo aplikasi bisa akses url berikut
[https://webggguestdesk.herokuapp.com/#/](https://webggguestdesk.herokuapp.com/#/)
<details>
<summary> Login Accont demo </summary>
password : demo
username : demo
</details>

## Screen Example 
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestdesk/guestdesk_login.png?raw=true)

![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestdesk/guestdesk_home.png?raw=true)

| Filter | Deskripsi |
| ------ | ------ |
| Total Visit | Jumlah Kunjungan di hari H yang di tunjuk oleh Date Filter keseluruhan terkecuali yang di cancel |
| Active | Jumlah Kunjungan di hari H yang di tunjuk oleh Date filter yang masih aktif/berlangsung |
| Ended | Jumlah kunjungan di hari H yang di tunjuk oleh Date filter yang sudah inactive/berakhir |
| Cancelled | Jumlah kunjungan di hari H yang di tunjuk oleh Date filter yang sudah inactive/berakhir karena di batalkan |
| Expired | Jumlah kunjungan sampai dengan hari sekarang yang belum di Ended |

## Guest Tracking
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestdesk/gustdesk_visitor_tracking.JPG?raw=true)

## Card Assignment
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestdesk/guestdesk_accesscard_assignment.JPG?raw=true)

## Session Visit
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestdesk/guestdesk_session_visit.JPG?raw=true)

## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
