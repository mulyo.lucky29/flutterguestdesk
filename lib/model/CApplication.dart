
/*
{
  "appsname": "GG Guest Desk",
  "appsdescription": "A Visitor Management Console Application For GuestLogBook",
  "appversion": "1.0.0",
  "apiversion": "1.0.0",
  "appscredit": "Powered By IT Directorate 2022 Gudang Garam Tbk - LM",
  "appsmobileurl": "https://webggguestlogbook.herokuapp.com/#/",
  "appsstatus": "Running"
}
 */

class CApplication  {
  final String appsname;
  final String appsdescription;
  final String appversion;
  final String apiversion;
  final String appscredit;
  final String appsmobileurl;
  final String appsstatus;

  const CApplication({
    this.appsname,
    this.appsdescription,
    this.appversion,
    this.apiversion,
    this.appscredit,
    this.appsmobileurl,
    this.appsstatus,
  });

  factory CApplication.fromJson(Map<String, dynamic> json) {
    CApplication result;
    try{
      result = CApplication(
          appsname: json['appsname'],
          appsdescription: json['appsdescription'],
          appversion: json['appversion'],
          apiversion:json['apiversion'],
          appscredit: json['appscredit'],
          appsmobileurl: json['appsmobileurl'],
          appsstatus: json['appsstatus'],
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'appsname': appsname,
        'appsdescription': appsdescription,
        'appversion': appversion,
        'apiversion': apiversion,
        'appscredit': appscredit,
        'appsmobileurl': appsmobileurl,
        'appsstatus': appsstatus,

      };
}