
class CVisitCardAssign {

  final int visitId;
  final String accessCardTag;
  final String accessCardPairNo;
  final String username;

  const CVisitCardAssign({
    this.visitId,
    this.accessCardTag,
    this.accessCardPairNo,
    this.username
  });

  factory CVisitCardAssign.fromJson(Map<String, dynamic> json) {
    CVisitCardAssign result;
    try{
      result = CVisitCardAssign(
          visitId: json['visitId'],
          accessCardTag: json['accessCardTag'],
          accessCardPairNo: json['accessCardPairNo'],
          username:json['username']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'visitId': visitId,
        'accessCardTag': accessCardTag,
        'accessCardPairNo': accessCardPairNo,
        'username': username,
      };
}