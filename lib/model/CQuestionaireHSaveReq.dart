
class CQuestionaireHSaveReq {
  final int quehid;
  final String quehname;
  final String quehdesc;
  final String createdBy;

  const CQuestionaireHSaveReq({
    this.quehid,
    this.quehname,
    this.quehdesc,
    this.createdBy
  });

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'quehid': quehid,
        'quehname': quehname,
        'quehdesc': quehdesc,
        'createdBy': createdBy
      };
}