import 'package:myggguestdesk/model/CSite.dart';

/*
{
  "userId": 3,
  "username": "GGT_DESK_01",
  "phonenumber": "081310267800",
  "emailaddress": "ggtdesk01@gudanggaramtbk.com",
  "objSite": {
    "siteId": 8,
    "siteCode": "JKP_GG_Tower",
    "siteName": "GG Tower",
    "isActive": "Y",
    "formaccessmentid": 1
  },
  "isAdmin": "N",
  "isActive": "Y"
}
 */

class CUser {
  final int userId;
  final String username;
  final String phonenumber;
  final String emailaddress;
  final CSite objSite;
  final String isAdmin;
  final String isActive;

  const CUser({
    this.userId,
    this.username,
    this.phonenumber,
    this.emailaddress,
    this.objSite,
    this.isAdmin,
    this.isActive
  });

  factory CUser.fromJson(Map<String, dynamic> json) {
    CUser result;
    try{
      result = CUser(
          userId: json['userId'],
          username: json['username'],
          phonenumber: json['phonenumber'],
          emailaddress: json['emailaddress'],
          objSite: CSite.fromJson(json['objSite']),
          isAdmin: json['isAdmin'],
          isActive:json['isActive'],
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'userId': userId,
        'username': username,
        'phonenumber': phonenumber,
        'emailaddress': emailaddress,
        'objSite': objSite,
        'isAdmin': isAdmin,
        'isActive': isActive,
      };


}