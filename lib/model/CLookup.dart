/*
[
  {
    "lookupId": 1,
    "lookupGroupCode": "AssuranceCard",
    "lookupValue": "KTP",
    "lookupDescription": "KTP",
    "lookupOrderNo": 1,
    "isActive": "Y"
  },
]
 */

class CLookup {
  final int lookupId;
  final String lookupGroupCode;
  final String lookupValue;
  final String lookupDescription;
  final int lookupOrderNo;
  final String isActive;

  const CLookup({
    this.lookupId,
    this.lookupGroupCode,
    this.lookupValue,
    this.lookupDescription,
    this.lookupOrderNo,
    this.isActive
  });

  factory CLookup.fromJson(Map<String, dynamic> json) {
    CLookup result;
    try{
      result = CLookup(
          lookupId: json['lookupId'],
          lookupGroupCode: json['lookupGroupCode'],
          lookupValue: json['lookupValue'],
          lookupDescription: json['lookupDescription'],
          lookupOrderNo: json['lookupOrderNo'],
          isActive:json['isActive']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'lookupId': lookupId,
        'lookupGroupCode': lookupGroupCode,
        'lookupValue': lookupValue,
        'lookupDescription': lookupDescription,
        'lookupOrderNo': lookupOrderNo,
        'isActive': isActive
      };

}