
/*
{
  "quehid": 0,
  "quedid": 0,
  "quelineno": 0,
  "quetext": "string",
  "createdBy": "string"
}
 */

class CQuestionaireSaveReq {
  final int quehid;
  final int quedid;
  final int quelineno;
  final String quetext;
  final String createdBy;

  const CQuestionaireSaveReq({
    this.quehid,
    this.quedid,
    this.quelineno,
    this.quetext,
    this.createdBy
  });

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'quehid': quehid,
        'quedid': quedid,
        'quelineno': quelineno,
        'quetext': quetext,
        'createdBy': createdBy
      };
}