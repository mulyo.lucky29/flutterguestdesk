import 'package:flutter/material.dart';

class CMenu {
  final int menuNum;
  final String menuName;
  final Icon menuIcon;
  final bool onlyAdmin;

  const CMenu({
    this.menuNum,
    this.menuName,
    this.menuIcon,
    this.onlyAdmin,
  });
}