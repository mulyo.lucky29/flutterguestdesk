
class CVisitEndReq {
  final int endVisitId;
  final String endVisitreason;
  final String endVisitby;

  const CVisitEndReq({
    this.endVisitId,
    this.endVisitreason,
    this.endVisitby
  });

  factory CVisitEndReq.fromJson(Map<String, dynamic> json) {
    CVisitEndReq result;
    try{
      result = CVisitEndReq(
          endVisitId: json['endVisitId'],
          endVisitreason: json['endVisitreason'],
          endVisitby: json['endVisitby']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'endVisitId': endVisitId,
        'endVisitreason': endVisitreason,
        'endVisitby': endVisitby
      };

}