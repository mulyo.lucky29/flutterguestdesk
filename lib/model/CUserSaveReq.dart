/*
{
  "dskusername": "string",
  "dskPhoneNo": "string",
  "dskEmail": "string",
  "dsksiteId": 0,
  "dskisAdmin": "string",
  "dskisActive": "string",
  "dskCreatedBy": "string"
}
 */

class CUserSaveReq {
  final String dskusername;
  final String dskPhoneNo;
  final String dskEmail;
  final int dsksiteId;
  final String dskisAdmin;
  final String dskisActive;
  final String dskCreatedBy;


  const CUserSaveReq({
    this.dskusername,
    this.dskPhoneNo,
    this.dskEmail,
    this.dsksiteId,
    this.dskisAdmin,
    this.dskisActive,
    this.dskCreatedBy,
  });

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'dskusername': dskusername,
        'dskPhoneNo': dskPhoneNo,
        'dskEmail': dskEmail,
        'dsksiteId': dsksiteId,
        'dskisAdmin': dskisAdmin,
        'dskisActive': dskisActive,
        'dskCreatedBy': dskCreatedBy
      };

}