/*
{
  "siteCode": "string",
  "siteName": "string",
  "isActive": "string",
  "formaccessmentid": 0,
  "createdBy": "string"
}
 */

class CSiteSaveReq {
  final String siteCode;
  final String siteName;
  final String isActive;
  final int formaccessmentid;
  final String createdBy;

  const CSiteSaveReq({
    this.siteCode,
    this.siteName,
    this.isActive,
    this.formaccessmentid,
    this.createdBy
  });

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'siteCode': siteCode,
        'siteName': siteName,
        'isActive': isActive,
        'formaccessmentid': formaccessmentid,
        'createdBy': createdBy,
      };
}