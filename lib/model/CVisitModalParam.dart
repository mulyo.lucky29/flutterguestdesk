import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/model/CQuestionaire.dart';
import 'package:myggguestdesk/model/CLookup.dart';

class CVisitModalParam extends ChangeNotifier {
  String strNoKTP;
  CGuest objGuest;
  CVisit objVisit;
  List<CQuestionaire> objQue;
  List<CLookup> objDoc;

   void setNoKTP(String strNoKTP){
     this.strNoKTP = strNoKTP;
   }

   void setObjGuest(CGuest objGuest){
     this.objGuest = objGuest;
   }

   void setObjVisit(CVisit objVisit){
     this.objVisit = objVisit;
   }

   void setObjQue(List<CQuestionaire> objQue){
     this.objQue   = objQue;
   }

   void setObjDoc(List<CLookup> objDoc){
     this.objDoc = objDoc;
   }
}