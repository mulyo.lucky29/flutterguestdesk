import 'package:equatable/equatable.dart';

class CQuestionaireH extends Equatable {
  final int quehid;
  final String quehname;
  final String quehdesc;

  const CQuestionaireH({
    this.quehid,
    this.quehname,
    this.quehdesc
  });

  factory CQuestionaireH.fromJson(Map<String, dynamic> json) {
    CQuestionaireH result;
    try{
      result = CQuestionaireH(
          quehid: json['quehid'],
          quehname: json['quehname'],
          quehdesc: json['quehdesc']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }


  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'quehid': quehid,
        'quehname': quehname,
        'quehdesc': quehdesc,
      };

  @override
  List<Object> get props => [quehname];

}