import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/model/CContact.dart';

/*
{
    "visitId": 106,
    "visitSessionCode": "1ea5908e3a14ae2aa2a4cf6bf363148d",
    "visitDate": "2021-10-28",
    "visitPurpose": "asdf",
    "checkIn": null,
    "checkOut": null,
    "startVisitDate": "2021-10-28 08:35:50",
    "endVisitDate": "2021-10-28 10:18:23",
    "endVisitFlag": "Y",
    "endVisitReason": "",
    "endVisitBy": "Lucky Mulyo",
    "objGuest": {
      "guestId": 54,
      "guestCardId": "12345",
      "guestName": "Lucky Mulyo",
      "guestPhone": "081310267800",
      "guestEmail": "lucky.mulyo@gudanggaramtbk.com",
      "guestCompany": "Gudang Garam",
      "guestPictureUrl": null,
      "guestPictureCardId": null,
      "isActive": null
    },
    "objSite": {
      "siteId": 8,
      "siteCode": "JKP_GG_Tower",
      "siteName": "GG Tower",
      "isActive": null,
      "formaccessmentid": null
    },
    "objContact": {
      "contactId": 1524,
      "siteCode": "JKP_GG_Tower",
      "siteSpecific": "Basement",
      "tenantCompany": null,
      "contactType": null,
      "contactName": "Hari Pramono ",
      "contactPhone": null,
      "contactEmail": null,
      "contactExt": "800305",
      "contactAltName": null
    },
    "assuranceCardType": "Pasport",
    "assuranceCardNo": "asdf",
    "accessCardTag": null,
    "accessCardPairNo": null,
    "formaccessment": "[{\"quehid\":1,\"quedid\":1,\"quelineno\":1,\"quetext\":\"Apakah pernah keluar rumah/tempat umum (pasar, fasyankes, kerumunan orang dan lain lain) ?\",\"quehname\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"quehdesc\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"queanswer\":\"Y\"},{\"quehid\":1,\"quedid\":2,\"quelineno\":2,\"quetext\":\"Apakah pernah menggunakan transportasi umum ?\",\"quehname\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"quehdesc\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"queanswer\":\"N\"},{\"quehid\":1,\"quedid\":3,\"quelineno\":3,\"quetext\":\"Apakah pernah melakukan perjalanan ke luar kota/internasional (wilayah yang terjangkit / zona merah ?\",\"quehname\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"quehdesc\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"queanswer\":\"Y\"},{\"quehid\":1,\"quedid\":4,\"quelineno\":4,\"quetext\":\"Apakah anda mengikuti kegiatan yang melibatkan orang banyak ?\",\"quehname\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"quehdesc\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"queanswer\":\"N\"},{\"quehid\":1,\"quedid\":5,\"quelineno\":5,\"quetext\":\"Apakah memiliki riwayat kontak erat dengan orang yang dinyatakan ODP, PDP atau konfirm COVID-19 (berjabat tangan, berbicara, berada dalam satu ruangan/satu rumah ?\",\"quehname\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"quehdesc\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"queanswer\":\"Y\"},{\"quehid\":1,\"quedid\":6,\"quelineno\":6,\"quetext\":\"Apakah pernah mengalami demam/batuk/pilek/sakit tenggorokan/ sesak dalam waktu 14 hari terakhir ?\",\"quehname\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"quehdesc\":\"FORM SELF ASSESMENT PENGUNJUNG/TRACING\",\"queanswer\":\"N\"}]"
  },
 */

class CVisit {
  final int visitId;
  final String visitSessionCode;
  final String visitDate;
  final String visitPurpose;
  final String checkIn;
  final String checkOut;
  final CSite objSite;
  final CGuest objGuest;
  final CContact objContact;
  final String startVisitDate;
  final String endVisitDate;
  final String endVisitFlag;
  final String endVisitReason;
  final String endVisitBy;
  final String assuranceCardType;
  final String assuranceCardNo;
  final String accessCardTag;
  final String accessCardPairNo;
  final String formaccessment;

  const CVisit({
    this.visitId,
    this.visitSessionCode,
    this.visitDate,
    this.visitPurpose,
    this.checkIn,
    this.checkOut,
    this.objSite,
    this.objGuest,
    this.objContact,
    this.startVisitDate,
    this.endVisitDate,
    this.endVisitFlag,
    this.endVisitReason,
    this.endVisitBy,
    this.assuranceCardType,
    this.assuranceCardNo,
    this.accessCardTag,
    this.accessCardPairNo,
    this.formaccessment
  });

  factory CVisit.fromJson(Map<String, dynamic> json) {
    CVisit result;
    try{
      result = CVisit(
          visitId: json['visitId'],
          visitSessionCode: json['visitSessionCode'],
          visitDate: json['visitDate'],
          visitPurpose:json['visitPurpose'],
          checkIn:json['checkIn'],
          checkOut:json['checkOut'],
          objSite: CSite.fromJson(json['objSite']),
          objGuest: CGuest.fromJson(json['objGuest']),
          objContact: CContact.fromJson(json['objContact']),
          startVisitDate: json['startVisitDate'],
          endVisitDate: json['endVisitDate'],
          endVisitFlag: json['endVisitFlag'],
          endVisitReason: json['endVisitReason'],
          endVisitBy: json['endVisitBy'],
          assuranceCardType: json['assuranceCardType'],
          assuranceCardNo: json['assuranceCardNo'],
          accessCardTag: json['accessCardTag'],
          accessCardPairNo: json['accessCardPairNo'],
          formaccessment: json['formaccessment'],
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'visitId': visitId,
        'visitSessionCode': visitSessionCode,
        'visitDate': visitDate,
        'visitPurpose': visitPurpose,
        'checkIn': checkIn,
        'checkOut': checkOut,
        'objSite': objSite,
        'objGuest': objGuest,
        'objContact': objContact,
        'startVisitDate': startVisitDate,
        'endVisitDate': endVisitDate,
        'endVisitFlag': endVisitFlag,
        'endVisitReason': endVisitReason,
        'endVisitBy': endVisitBy,
        'assuranceCardType': assuranceCardType,
        'assuranceCardNo': assuranceCardNo,
        'accessCardTag': accessCardTag,
        'accessCardPairNo': accessCardPairNo,
        'formaccessment': formaccessment
      };

}