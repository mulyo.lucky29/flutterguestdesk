
class CUserResetReq {
  final String dskusername;
  final String dskpassword;
  final String dskCreatedBy;


  const CUserResetReq({
    this.dskusername,
    this.dskpassword,
    this.dskCreatedBy,
  });

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'dskusername': dskusername,
        'dskpassword': dskpassword,
        'dskCreatedBy': dskCreatedBy
      };

}