

/*
sc_siteid,
sc_sitecode,
sc_sitename,
sc_person_visit,
sc_person_active_visit,
sc_person_ended_normal_visit,
sc_person_ended_cancelled_visit,
sc_session_visit,
sc_session_active_visit,
sc_session_ended_normal_visit,
sc_session_ended_cancelled_visit

	 */
class CVisitCounter {
  final int siteId;
  final String siteCode;
  final String siteName;
  final String visitDate;
  final int personVisit;
  final int personActiveVisit;
  final int personEndedNormalVisit;
  final int personEndedCancelledVisit;
  final int personVisitExpired;
  final int sessionVisit;
  final int sessionActiveVisit;
  final int sessionEndedNormalVisit;
  final int sessionEndedCancelledVisit;
  final int sessionVisitExpired;

  const CVisitCounter({
    this.siteId,
    this.siteCode,
    this.siteName,
    this.visitDate,
    this.personVisit,
    this.personActiveVisit,
    this.personEndedNormalVisit,
    this.personEndedCancelledVisit,
    this.personVisitExpired,
    this.sessionVisit,
    this.sessionActiveVisit,
    this.sessionEndedNormalVisit,
    this.sessionEndedCancelledVisit,
    this.sessionVisitExpired
  });

  factory CVisitCounter.fromJson(Map<String, dynamic> json) {
    CVisitCounter result;
    try{
      result = CVisitCounter(
          siteId: json['siteId'],
          siteCode: json['siteCode'],
          siteName: json['siteName'],
          visitDate: json['visitDate'],
          personVisit: json['cpersonVisit'],
          personActiveVisit: json['cpersonActiveVisit'],
          personEndedNormalVisit: json['cpersonEndedNormalVisit'],
          personEndedCancelledVisit: json['cpersonEndedCancelledVisit'],
          personVisitExpired: json['cpersonVisitExpired'],
          sessionVisit: json['csessionVisit'],
          sessionActiveVisit: json['csessionActiveVisit'],
          sessionEndedNormalVisit: json['csessionEndedNormalVisit'],
          sessionEndedCancelledVisit: json['csessionEndedCancelledVisit'],
          sessionVisitExpired: json['csessionVisitExpired']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'siteId': siteId,
        'siteCode': siteCode,
        'siteName': siteName,
        'visitDate': visitDate,
        'personVisit': personVisit,
        'personActiveVisit': personActiveVisit,
        'personEndedNormalVisit': personEndedNormalVisit,
        'personEndedCancelledVisit': personEndedCancelledVisit,
        'personVisitExpired': personVisitExpired,
        'sessionVisit': sessionVisit,
        'sessionActiveVisit': sessionActiveVisit,
        'sessionEndedNormalVisit': sessionEndedNormalVisit,
        'sessionEndedCancelledVisit': sessionEndedCancelledVisit,
        'sessionVisitExpired': sessionVisitExpired
};


}