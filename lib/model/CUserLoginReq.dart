

/*
request :
{
  "dskusername": "GGT_DESK_01",
  "dskpassword": "desk_01"
}
 */
class CUserLoginReq {
  final String dskUserName;
  final String dskPassword;

  const CUserLoginReq({
    this.dskUserName,
    this.dskPassword,
  });


  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'dskusername': dskUserName,
        'dskpassword': dskPassword,
      };


}