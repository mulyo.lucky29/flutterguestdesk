/*
  {
    "contactId": 1522,
    "siteCode": "JKP_GG_Tower",
    "siteSpecific": "Basement",
    "tenantCompany": null,
    "contactType": "Person",
    "contactName": "Deni Wahyu",
    "contactPhone": null,
    "contactEmail": null,
    "contactExt": "800210",
    "contactAltName": null
  },
 */

class CContact {
  final int contactId;
  final String siteCode;
  final String siteSpecific;
  final String tenantCompany;
  final String contactType;
  final String contactName;
  final String contactPhone;
  final String contactEmail;
  final String contactExt;
  final String contactAltName;

  const CContact({
    this.contactId,
    this.siteCode,
    this.siteSpecific,
    this.tenantCompany,
    this.contactType,
    this.contactName,
    this.contactPhone,
    this.contactEmail,
    this.contactExt,
    this.contactAltName
  });

  factory CContact.fromJson(Map<String, dynamic> json) {
    CContact result;
    try{
      result = CContact(
          contactId: json['contactId'],
          siteCode: json['siteCode'],
          siteSpecific: json['siteSpecific'],
          tenantCompany: json['tenantCompany'],
          contactType: json['contactType'],
          contactName:json['contactName'],
          contactPhone: json['contactPhone'],
          contactEmail: json['contactEmail'],
          contactExt: json['contactExt'],
          contactAltName: json['contactAltName']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'contactId': contactId,
        'siteCode': siteCode,
        'siteSpecific': siteSpecific,
        'tenantCompany': tenantCompany,
        'contactType': contactType,
        'contactName': contactName,
        'contactPhone': contactPhone,
        'contactEmail': contactEmail,
        'contactExt': contactExt,
        'contactAltName': contactAltName
      };


}