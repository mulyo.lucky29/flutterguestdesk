/*
[
  {
    "guestId": 1,
    "guestCardId": "12345",
    "guestName": "Lucky Mulyo",
    "guestPhone": "081310267800",
    "guestEmail": "lucky.mulyo@gudanggaramtbk.com",
    "guestCompany": "Gudang Garam",
    "guestPictureUrl": "",
    "guestPictureCardId": "",
    "isActive": "Y"
  }
]
 */

class CGuest {
  final int guestId;
  final String guestCardId;
  final String guestName;
  final String guestPhone;
  final String guestEmail;
  final String guestCompany;
  final String guestPictureUrl;
  final String guestPictureCardId;
  final String isActive;

  const CGuest({
    this.guestId,
    this.guestCardId,
    this.guestName,
    this.guestPhone,
    this.guestEmail,
    this.guestCompany,
    this.guestPictureUrl,
    this.guestPictureCardId,
    this.isActive
  });

  factory CGuest.fromJson(Map<String, dynamic> json) {
    CGuest result;
    try{
      result = CGuest(
          guestId: json['guestId'],
          guestCardId: json['guestCardId'],
          guestName: json['guestName'],
          guestPhone:json['guestPhone'],
          guestEmail: json['guestEmail'],
          guestCompany: json['guestCompany'],
          guestPictureUrl: json['guestPictureUrl'],
          guestPictureCardId: json['guestPictureCardId'],
          isActive: json['isActive']
      );
    }
    catch(e){
      print(e.toString());
    }
    return result;
  }

  // translate element into JSON Format for class
  Map<String, dynamic> toJson() =>
      {
        'guestId': guestId,
        'guestCardId': guestCardId,
        'guestName': guestName,
        'guestPhone': guestPhone,
        'guestEmail': guestEmail,
        'guestCompany': guestCompany,
        'guestPictureUrl': guestPictureUrl,
        'guestPictureCardId': guestPictureCardId,
        'isActive': isActive
      };


}