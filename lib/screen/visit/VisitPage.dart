import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisitCounter.dart';
import 'package:myggguestdesk/service/svcVisit.dart';
import 'package:myggguestdesk/service/svcVisitCounter.dart';
import 'package:myggguestdesk/widget/HorizontalCalendar.dart';
import 'package:myggguestdesk/widget/BadgeInfo.dart';
import 'package:myggguestdesk/screen/visit/guest/GuestModal.dart';
import 'package:myggguestdesk/screen/visit/session/VisitSessionModal.dart';


class VisitPage extends StatefulWidget {
  final TabController objTabMenuController;
  final CUser objLoginAcc;

  VisitPage({Key key,
    @required this.objTabMenuController,
    @required this.objLoginAcc
  }) : super(key: key);


  @override
  _VisitPageState createState() => new _VisitPageState();
}

class _VisitPageState extends State<VisitPage> {
  Future myFutureVisit;
  Future myFutureVisitCounter;
  var refreshKey = GlobalKey<RefreshIndicatorState>();


  bool _isFiltered = false;
  bool _isAscending = true;

  List<bool> _isCounterSelected = [true,false,false,false,false];
  List<String> counterSection = ["Total Visit","Active Visit","Ended Visit","Cancelled","Expired"];

  String strVisitDate;
  // list berdasarkan pencarian
  List<CVisit> _searchResult = [];
  // list original visit per date
  List<CVisit> _visitResultList = [];
  List<CVisit> _visitList = [];

  List<String> tableHeader = ['No KTP', 'Nama Visitor','No Telp Visitor','Company Visitor','Contact Name', 'Location', 'Id Type','Card No','Access Card No','Pair No'];
  //List<int> tablePortion   = [1,3,3,5,3,4,5,3,3,3,3];

  List<TextEditingController> TxtController = [];
  List<FocusNode> TxtNode = [];

  Future<CVisitCounter> loadVisitCounter(CSite objSite, String visitDate) async {
    print("loadVisitCounter ");

    CVisitCounter result;

    final svcVisitCounter visitCountService = svcVisitCounter();
    // load Visit
    await visitCountService.getVisitCounterByDate(objSite.siteId.toString(), visitDate)
        .then((objResult) {
            if (objResult != null) {
              setState(() {
                result = objResult;
              });
            }
        }).catchError((e) {
      print("Error Load Visit Counter -> " + e.toString());
    });
    return result;
  }
  Future<List<CVisit>> loadVisit(CSite objSite, String visitDate) async{
    List<CVisit> result = [];
    final svcVisit  visitService = svcVisit();
    // load Visit
    await visitService.getVisitByDate(objSite.siteCode,visitDate).then((objResult) {
      if (objResult != null) {
        setState(() {
          _visitList        = objResult;
          _visitResultList  = objResult;
          myFutureVisitCounter = loadVisitCounter(objSite, visitDate);
          result = objResult;
        });
        doFilter();
      }
    }).catchError((e) {
      print("Error Load Visit List -> " + e.toString());
    });
    return result;
  }


  Future<void> _showDialogGuest(BuildContext context, CSite objSite){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  //color: Colors.transparent,
                  width: 500.0,
                  height: double.infinity,
                  child: GuestModal(objSite: objSite) //GuestCheck(objSite: objSite)
                ),
              )
          );
        });
  }

  Future<void> _showDialogGuestSession(BuildContext context, CUser objLoginAcc, CVisit objVisit, int tabDefault){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                    margin: const EdgeInsets.all(10.0),
                    //color: Colors.transparent,
                    width: 500.0,
                    height: double.infinity,
                    child: VisitSessionModal(
                              objLoginAcc: objLoginAcc,
                              objVisit: objVisit,
                              defaultTab: tabDefault)
                ),
              )
          );
        }).whenComplete(() {
            doRefresh();
        });
  }

  onSearchTextChanged(int index, String str) async {
    doFilter();
    setState(() {});
  }

  doSelectedCounter(int idx){
    print("doDisplayOptionFromCounter > " + counterSection[idx]);
    // clear filter
    doClearFilter();

    setState(() {
      // clear search
      _searchResult.clear();
      // "Total Visit"
      if(idx == 0){
        _isCounterSelected = [true, false, false,false,false];
        _visitList = _visitResultList;
      }
      else {
        _visitList = _visitResultList;
        if(idx == 1){
          _isCounterSelected = [false, true, false,false,false];
          _searchResult = _visitList.where((CVisit) =>
          CVisit.endVisitDate == null).toList();
        }
        // "Ended Visit"
        else if(idx == 2){
          _isCounterSelected = [false, false, true,false,false];
          _searchResult = _visitList.where((CVisit) =>
          CVisit.endVisitDate != null &&
              CVisit.endVisitReason != "Cancelled").toList();
        }
        // "Cancelled"
        else if(idx == 3){
          _isCounterSelected = [false, false, false,true,false];
          _searchResult = _visitList.where((CVisit) =>
          CVisit.endVisitDate != null &&
              CVisit.endVisitReason == "Cancelled").toList();
        }
        // "Expired"
        else if(idx == 4){
          // redirect to menu
          widget.objTabMenuController.animateTo(5);
        }
        _visitList = _searchResult;
      }
      // "Active Visit"
    });
    print("_isCounterSelected -> " + _isCounterSelected.toString());
  }

  doFilter(){
    print("_isCounterSelected dof -> " + _isCounterSelected.toString());

    setState(() {
      _searchResult.clear();
      _searchResult = _visitList
          .where((CVisit) =>
          (TxtController[0].text.isEmpty ? true : CVisit.objGuest.guestCardId.toUpperCase().contains(TxtController[0].value.text.toUpperCase())) &&
          (TxtController[1].text.isEmpty ? true : (CVisit.objGuest.guestName != null ? CVisit.objGuest.guestName.toUpperCase() : '').contains(TxtController[1].value.text.toUpperCase())) &&
          (TxtController[2].text.isEmpty ? true : (CVisit.objGuest.guestPhone != null ? CVisit.objGuest.guestPhone.toUpperCase() : '').contains(TxtController[2].value.text.toUpperCase())) &&
          (TxtController[3].text.isEmpty ? true : (CVisit.objGuest.guestCompany != null ? CVisit.objGuest.guestCompany.toUpperCase() : '').contains(TxtController[3].value.text.toUpperCase())) &&
          (TxtController[4].text.isEmpty ? true : (CVisit.objContact.contactName != null ? CVisit.objContact.contactName.toUpperCase() :
                                                   (CVisit.objContact.contactAltName != null ? CVisit.objContact.contactAltName.toUpperCase() : '')).contains(TxtController[4].value.text.toUpperCase())) &&
          (TxtController[5].text.isEmpty ? true : (CVisit.objContact.siteSpecific != null ? CVisit.objContact.siteSpecific.toUpperCase() : '').contains(TxtController[5].value.text.toUpperCase())) &&
          (TxtController[6].text.isEmpty ? true : (CVisit.assuranceCardType != null ? CVisit.assuranceCardType.toUpperCase() : '').contains(TxtController[6].value.text.toUpperCase())) &&
          (TxtController[7].text.isEmpty ? true : (CVisit.assuranceCardNo != null ? CVisit.assuranceCardNo.toUpperCase() : '').contains(TxtController[7].value.text.toUpperCase())) &&
          (TxtController[8].text.isEmpty ? true : (CVisit.accessCardTag != null ? CVisit.accessCardTag.toUpperCase() : '').contains(TxtController[8].value.text.toUpperCase())) &&
          (TxtController[9].text.isEmpty ? true : (CVisit.accessCardPairNo != null ? CVisit.accessCardPairNo.toUpperCase() : '').toUpperCase().contains(TxtController[9].value.text.toUpperCase()))

      ).toList();
    });
  }

  doRefresh(){
    loadVisit(widget.objLoginAcc.objSite, strVisitDate);
    setState(() {});
  }
  doSort(){
    setState(() {
      if(_isAscending == true) {
        _visitList.sort((a, b) {
          return a.startVisitDate.compareTo(b.startVisitDate);
        });
      }
      else{
        _visitList.sort((a, b) {
          return b.startVisitDate.compareTo(a.startVisitDate);
        });
      }
    });
  }
  doClearFilter(){
    TxtController.forEach((txtController) {
      txtController.clear();
    });
    doFilter();
  }

  bool isFiltered(){
    int found = 0;
    TxtController.forEach((txtController) {
      if(txtController.text.isNotEmpty) {
        found = found +1;
      }
    });
    if(found > 0){
      return true;
    }
    else{
      return false;
    }
  }

  String getCurrentDate(String strFormat){
      var now = DateTime.now();
      return DateFormat(strFormat).format(now);
  }

  Widget separator(BuildContext context){
    return Column(
      children: [
        SizedBox(height: 5),
        Container(height: 5, color: Colors.grey[200]),
        SizedBox(height: 5),
      ],
    );
  }
  Widget Cmd_Add(BuildContext context, CUser objLoginAcc) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.person_add, color: Colors.blue),
        tooltip: 'Add Visitor Manual',
        onPressed: (){
          _showDialogGuest(context, objLoginAcc.objSite)
              .then((void value) => doRefresh());
        },
      ),
    );
  }
  Widget Cmd_Refresh(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.refresh, color: Colors.blue),
        tooltip: 'Refresh',
        onPressed: (){
          doRefresh();
        },
      ),
    );
  }
  Widget Cmd_ClearAllFilter(BuildContext context){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.tune),
        tooltip: 'Filter Clear',
        onPressed: (){
          doClearFilter();
        },
      ),
    );
  }
  Widget Cmd_EndVisit(BuildContext context, CUser objLoginAcc, CVisit objVisit){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.location_off,
          color: objVisit.endVisitDate == null ? Colors.red : Colors.grey,
        ),
        tooltip: 'End Visit',
        onPressed: (){
          objVisit.endVisitDate == null ?
          _showDialogGuestSession(context, objLoginAcc, objVisit, 5) : null;
          //_showDialogEndVisit(context, objVisit) : null;
        },
      ),
    );
  }
  Widget Cmd_AssignCard(BuildContext context, CUser objLoginAcc, CVisit objVisit){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.card_membership,
          color: objVisit.endVisitDate == null ? Colors.blue : Colors.grey,
        ),
        tooltip: 'Access Card',
        onPressed: (){
          objVisit.endVisitDate == null ? _showDialogGuestSession(context, objLoginAcc, objVisit, 4) : null;
        },
      ),
    );
  }

  Widget Calendar(BuildContext context){
    return HorizontalCalendar(
      width: 80,
      height: 40,
      //initialDate: DateTime.now(),
      date: DateTime.now(),
      textColor: Colors.black45,
      backgroundColor: Colors.white,
      selectedColor: Colors.blue,
      onDateSelected: (date) {
        setState(() {
          strVisitDate = date.toString(); //DateFormat('yyyy-MM-dd').format(date);
          loadVisit(widget.objLoginAcc.objSite, date.toString());
        });
        print(date.toString());
      },
    );
  }
  Widget CardVisitorCount(BuildContext context, Color boxColor, IconData boxIcon, int idx, CVisitCounter objVCounter) {
    return InkWell(
      child:  Card(
        elevation: 2,
        child: Container(
          padding: EdgeInsets.all(8),
          color: boxColor,
          //Colors.red,
          width: MediaQuery
              .of(context)
              .size
              .width < 1300 ?
          MediaQuery
              .of(context)
              .size
              .width - 400
              :
          MediaQuery
              .of(context)
              .size
              .width / 11,
          height: 80,
          //MediaQuery.of(context).size.height / 6,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(flex: 3,
                child: Icon(boxIcon, size: 36,
                    color: _isCounterSelected[idx] == true ? Colors.black :  Colors.white),
              ),
              Expanded(flex: 4,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(flex: 2,
                          child: Text(counterSection[idx], //strSection,
                              style: TextStyle(
                                  fontSize: 14,
                                  color:  _isCounterSelected[idx] == true ? Colors.black  :  Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Raleway'))
                      ),
                      Expanded(flex: 4,
                          child: Text("Person",
                              style: TextStyle(
                                  fontSize: 12,
                                  color:  _isCounterSelected[idx] == true ? Colors.black  :  Colors.white,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Raleway'))
                      ),
                      Expanded(flex: 4,
                          child: Text("Session",
                              style: TextStyle(
                                  fontSize: 12,
                                  color:  _isCounterSelected[idx] == true ? Colors.black  :  Colors.white,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Raleway'))
                      ),
                    ]
                ),
              ),
              Expanded(flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Expanded(flex: 6,
                        child: Text(
                          idx == 0 ? objVCounter.personVisit.toString() :
                          idx == 1 ? objVCounter.personActiveVisit.toString() :
                          idx == 2 ? objVCounter.personEndedNormalVisit.toString() :
                          idx == 3 ? objVCounter.personEndedCancelledVisit.toString() :
                          idx == 4 ? objVCounter.personVisitExpired.toString(): '-',
                          style: TextStyle(
                            fontSize: 24,
                            color: _isCounterSelected[idx] == true ? Colors.black  :  Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Raleway',
                          ),),
                      ),
                      Expanded(flex: 4,
                        child: Text(
                          idx == 0 ? objVCounter.sessionVisit.toString() :
                          idx == 1 ? objVCounter.sessionActiveVisit.toString() :
                          idx == 2 ? objVCounter.sessionEndedNormalVisit.toString() :
                          idx == 3 ? objVCounter.sessionEndedCancelledVisit.toString() :
                          idx == 4 ? objVCounter.sessionVisitExpired.toString() : '-',
                          style: TextStyle(
                            fontSize: 16,
                            color: _isCounterSelected[idx] == true ? Colors.black :  Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Raleway',
                          ),),
                      )
                    ],
                  )
              )
            ],
          ),
        ),
      ),
      onTap: () {
        doSelectedCounter(idx);
      },
    );
  }

  Widget _buildCounterContent(BuildContext context) {
    return FutureBuilder(
        future: myFutureVisitCounter,
        builder: (ctx, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          else if (snapshot.connectionState == ConnectionState.done) {
            print('snapshot: $snapshot');
            if (snapshot.hasData) {
              print('C');
              return Row(
                children: [
                  CardVisitorCount(context, Colors.indigoAccent, Icons.person, 0, snapshot.data),
                  CardVisitorCount(context, Colors.lightGreen, Icons.person, 1, snapshot.data),
                  CardVisitorCount(context, Colors.grey, Icons.person, 2, snapshot.data),
                  CardVisitorCount(context, Colors.purple, Icons.person, 3, snapshot.data),
                  CardVisitorCount(context, Colors.orange, Icons.person, 4, snapshot.data),
                ],
              );
            }
            else {
              print('D');
              return Text('No Data');
            }
          }
          else {
            print('E');
            return Center(child: CircularProgressIndicator());
          }
        }
    );
  }
  Widget _buildTitleVisit(BuildContext context, CUser objLoginAcc){
    return  Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  BadgeInfo(objSite: objLoginAcc.objSite),
                ]
            ),
            Row(
              children: [
                Cmd_Add(context, objLoginAcc),
                SizedBox(width: 10.0),
                Cmd_Refresh(context)
              ],
            )
          ],
        )
      ],
    );
  }
  Widget _buildTxtSearch(BuildContext context, int index){
    return
          Transform.translate(
              offset: Offset(-5, 0), //Offset(-16, 0),
              child: Transform.scale(
                scale: 0.9, //0.8,
                child: TextFormField(
                  focusNode:  TxtNode[index],
                  controller: TxtController[index],
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: tableHeader[index],
                      hintText: tableHeader[index],
                      isDense: true,
                      contentPadding: EdgeInsets.all(5),
                      suffixIcon: Transform.translate(
                          offset: Offset(5,0),
                          child:Transform.scale(
                            scale: 0.75,
                            child: IconButton(
                                icon: Icon(Icons.cancel),
                                tooltip: 'Clear',
                                onPressed: () {
                                      TxtController[index].clear();
                                      onSearchTextChanged(index, '');
                                }),
                          )
                      )
                  ),
                  style: TextStyle(fontSize: 14.0, height: 2.0, color: Colors.black),
                  onChanged: (string) {
                      onSearchTextChanged(index, string);
                  },
                  onFieldSubmitted: (_) {
                    if(index+1 == tableHeader.length) {
                      FocusScope.of(context).requestFocus(TxtNode[0]);
                    }
                    else{
                      FocusScope.of(context).requestFocus(TxtNode[index+1]);
                    }
                  },
                ),
              )
          );
  }
  Widget _buildRowHeaderVisit(BuildContext context){
    //List<String> tableHeader = ['Visit Date', 'No KTP', 'Nama Visitor','No Telp Visitor','Company Visitor','Contact Name', 'Assurance Card','Assurance No'];
    return  Padding(
        padding: EdgeInsets.only(top: 2.5),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                Column(
                  children: [
                        // group header
                        IntrinsicHeight(
                            child:Row(
                                  children: <Widget>[
                                        Expanded(flex: 1,
                                          child:  Container(height: 20, color: Colors.grey[200], child:
                                              Center(child: Text("No.", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                                        ),
                                        SizedBox(width: 5.0),
                                        Expanded(flex: 14,
                                          child:  Container(height: 20, color: Colors.grey[200], child:
                                              Center(child:Text("Visitor Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                                        ),
                                        SizedBox(width: 5.0),
                                        Expanded(flex: 7,
                                          child:  Container(height: 20, color: Colors.grey[200], child:
                                              Center(child: Text("Contact Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                                        ),
                                        SizedBox(width: 5.0),
                                        Expanded(flex: 12,
                                          child:  Container(height: 20, color: Colors.grey[200], child:
                                              Center(child: Text("Doc Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                                        ),
                                        SizedBox(width: 5.0),
                                        Expanded(flex: 7,
                                          child:  Container(height: 20, color: Colors.grey[200], child:
                                              Center(child: Text("Visit Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                                        ),
                                        SizedBox(width: 5.0),
                                        Expanded(flex: 3,
                                          child:  Container(height: 20, color: Colors.grey[200], child:
                                              Center(child: Text("Action", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                                        ),
                                  ]
                            )
                        ),
                    ],
                ),
                // search
                IntrinsicHeight(
                    child: Row(
                        children: <Widget>[
                            Expanded(flex: 1, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: Cmd_ClearAllFilter(context))),    // 1 : id
                            SizedBox(width: 5.0),

                            Expanded(flex: 3,
                                child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,0))),  // 2 : no ktp
                            Expanded(flex: 4, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,1))),  // 3 : nama visitor
                            Expanded(flex: 3, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,2))),  // 4 : no hp visitor
                            Expanded(flex: 4, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,3))),  // 5 : company visitor
                            SizedBox(width: 5.0),

                            Expanded(flex: 4, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,4))),  // 6 : contact name
                            Expanded(flex: 3, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,5))),  // 7 : contact spec
                            SizedBox(width: 5.0),

                            Expanded(flex: 3, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,6))),  // id type
                            Expanded(flex: 3, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,7))),  // id no
                            Expanded(flex: 3, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,8))),  // access card no
                            Expanded(flex: 3, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: _buildTxtSearch(context,9))),  // pair no
                            SizedBox(width: 5.0),

                            Expanded(flex: 2, child:
                                Container(height: 50, width: double.infinity,
                                          color: Colors.grey[200], child: Center(child: Text('Start Visit', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0))))),
                            Expanded(flex: 2, child:
                                  Container(height: 50, width: double.infinity,
                                            color: Colors.grey[200], child: Center(child: Text('End Visit', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0))))),
                            Expanded(flex: 3, child:
                                  Container(height: 50, width: double.infinity,
                                            color: Colors.grey[200], child: Center(child: Text('Ended Status', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0))))),
                            SizedBox(width: 5.0),

                            Expanded(flex: 3, child:
                                  Container(height: 50, width: double.infinity,
                                            color: Colors.grey[200], child: Center(child: Text("", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0,))))),
                        ]
                  ),
                )
            ]
        )
    );
  }
  Widget _buildRowListVisit(BuildContext context, CVisit objVisit, CUser objLoginAcc, int idx){
    return  Padding(
        padding: EdgeInsets.only(bottom: 0.5, top: 0.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(flex: 1, child:
                      InkWell(
                        child:
                          // Text(objVisit.visitId.toString(),
                          //      style: TextStyle(decoration: TextDecoration.underline,
                          //       color: Colors.blue,
                          //       fontWeight: FontWeight.w500,
                          //       fontSize: 12.0,)),
                          Text((idx + 1).toString(),
                              style: TextStyle(decoration: TextDecoration.underline,
                                color: Colors.blue,
                                fontWeight: FontWeight.w500,
                                fontSize: 12.0,)),
                          onTap: () {
                            _showDialogGuestSession(context, objLoginAcc, objVisit, 0);
                          }
                      ),
                  ),
                  SizedBox(width: 5.0),
                  Expanded(flex: 3, child:
                      InkWell(
                      child: Text(objVisit.objGuest.guestCardId == null ? '-'  : objVisit.objGuest.guestCardId,
                              style: TextStyle(decoration: TextDecoration.underline,
                                color: Colors.blue,
                                fontWeight: FontWeight.w500,
                                fontSize: 12.0,)),
                          onTap: () {
                            _showDialogGuestSession(context, objLoginAcc, objVisit, 3);

                            //_showDialogGuestSession(context, objLoginAcc, objVisit, 6);

                          }
                      ),
                  ),
                  Expanded(flex: 4, child:
                      InkWell(
                      child: Text(objVisit.objGuest.guestName == null ? '-'  : objVisit.objGuest.guestName,
                                style: TextStyle(decoration: TextDecoration.underline,
                                                 color: Colors.blue,
                                                 fontWeight: FontWeight.w500,
                                                 fontSize: 12.0,)),
                            onTap: () {
                            }
                        )
                  ),
                  Expanded(flex: 3, child: Text(objVisit.objGuest.guestPhone == null ? '-' : objVisit.objGuest.guestPhone,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  Expanded(flex: 4, child: Text(objVisit.objGuest.guestCompany == null ? '-' : objVisit.objGuest.guestCompany,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),
                  Expanded(flex: 4, child: Text(objVisit.objContact.contactName == null ?
                                               (objVisit.objContact.contactAltName == null ? '-' : objVisit.objContact.contactAltName) :
                                                objVisit.objContact.contactName,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.objContact.siteSpecific == null ? '-' : objVisit.objContact.siteSpecific,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),

                  Expanded(flex: 3, child: Text(objVisit.assuranceCardType == null ? '-' : objVisit.assuranceCardType,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.assuranceCardNo == null ? '-' : objVisit.assuranceCardNo,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.accessCardTag == null ? '-' : objVisit.accessCardTag,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.accessCardPairNo == null ? '-' : objVisit.accessCardPairNo,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),

                  Expanded(flex: 2, child: Text(objVisit.startVisitDate == null ? '-'  : objVisit.startVisitDate,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 2, child: Text(objVisit.endVisitDate == null ? '-' : objVisit.endVisitDate,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.endVisitReason == null ? '-' : objVisit.endVisitReason,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),

                  Expanded(flex: 3, child:
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Cmd_AssignCard(context, objLoginAcc, objVisit),
                        Cmd_EndVisit(context, objLoginAcc, objVisit), // objLoginAcc
                      ],
                  ))
                ]
            ),
            Divider(),
          ],
        )
    );
  }
  Widget _buildListViewVisit(BuildContext context, CUser objLoginAcc){
    return Expanded(
      //child: _searchResult.length != 0 || controller.text.isNotEmpty ?
      child: _searchResult.length != 0 || isFiltered() ?
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, index) {
          return _buildRowListVisit(context, _searchResult[index], objLoginAcc, index);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _visitList.length,
        itemBuilder: (context, index) {
          return _buildRowListVisit(context, _visitList[index], objLoginAcc, index);
        },
      ),
    );
  }

  Widget buildList(BuildContext context, CUser objLoginAcc) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildTitleVisit(context, objLoginAcc),
          separator(context),
          Row(
            children: [
                Calendar(context),
                SizedBox(width: 10),
                _buildCounterContent(context),
            ],
          ),
          //separator(context),
          _buildRowHeaderVisit(context),
          separator(context),
          _buildListViewVisit(context, objLoginAcc)
        ],
    );
  }


  @override
  void initState() {
    //var now = new DateTime.now();
    super.initState();
    // init txtController
    TxtController = List.generate(tableHeader.length, (i) => TextEditingController());
    // init focusNode
    TxtNode = List.generate(tableHeader.length, (i) => FocusNode());

    setState(() {
      strVisitDate = getCurrentDate('yyyy-MM-dd');
    });
    myFutureVisit = loadVisit(widget.objLoginAcc.objSite, strVisitDate);
  }

  @override
  void dispose() {
    // dispose txtController
    TxtController.forEach((c) => c.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      color: Colors.transparent,
      height: double.infinity,
      child:  buildList(context, widget.objLoginAcc),
    );
  }
}