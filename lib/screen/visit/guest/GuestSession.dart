import 'package:flutter/material.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:achievement_view/achievement_view.dart';
import 'package:myggguestdesk/widget/VisitorCard.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CVisitModalParam.dart';


class GuestSession extends StatefulWidget {
  final TabController objTabController;
  final CSite objSite;

  GuestSession({Key key,
    @required this.objTabController,
    @required this.objSite}) : super(key: key);

  @override
  _GuestSessionState createState() => new _GuestSessionState();
}

class _GuestSessionState extends State<GuestSession> {
  Scaffold stagger;
  Future myFutureVisit;
  CVisit objVisit;
  CGuest objGuest;


  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "Visit Check", //strTitle,
      icon: pIcon,
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {

    setState(() {
      this.objVisit = context.watch<CVisitModalParam>().objVisit;
    });

    return Container(
      margin: const EdgeInsets.all(10.0),
      color: Colors.transparent,
      width: 500.0,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
          // ambil apa adanya semua dari objVisit
          VisitorCard(objVisit: this.objVisit),
          //Card_visitor(context, this.objVisit.objSite, this.objVisit.objGuest, this.objVisit),
        ],
      ),
    );
  }

  /*
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: myFutureVisit,
        builder: (ctx, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          else if(snapshot.connectionState == ConnectionState.done){
            print('snapshot: $snapshot');
            if (snapshot.hasData) {
              print('C');
              return session_page(context, snapshot.data);
            }
            else{
              print('D');
              return Text('No Data');
            }
          }
          else {
            print('E');
            return Center(child: CircularProgressIndicator() );
          }
        });
  }
  */
}


