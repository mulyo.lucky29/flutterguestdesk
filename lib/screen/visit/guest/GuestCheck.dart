import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CVisitModalParam.dart';
import 'package:myggguestdesk/service/svcGuest.dart';
import 'package:myggguestdesk/service/svcVisit.dart';
import 'package:myggguestdesk/validation/valCekKTP.dart';
import 'package:myggguestdesk/tools/dialogBox.dart';

class GuestCheck extends StatefulWidget {
  final TabController objTabController;
  final CSite objSite;
  GuestCheck({Key key,
    @required this.objTabController,
    @required this.objSite}) : super(key: key);

  @override
  _GuestCheckState createState() => new _GuestCheckState();
}

class _GuestCheckState extends State<GuestCheck> with valCekKTP {
  final formInputKTPKey  = GlobalKey<FormState>();
  TextEditingController _ctrlNoKTP  = new TextEditingController();
  final dialogBox message           = dialogBox();
  FocusNode NodeNoKTP           = FocusNode();
  FocusNode NodeCmdCheckKTP     = FocusNode();
  String strNoKTP;
  String resultPage;


/*
  Future<void> _callInitServiceVisit(CGuest objGuest) async{
    print('_callInitServiceVisit');

    List<CLookup> listDocAssurance = [];
    List<CQuestionaire> listQuestionaire = [];
    svcLookup lookupService = new svcLookup();
    svcQuestionaire questionaireService = new svcQuestionaire();

    await lookupService.getListLookupByGroup('AssuranceCard')
        .then((List<CLookup> objDoc) {
              listDocAssurance = objDoc;
              questionaireService.getListQuestionaireById(widget.objSite.formaccessmentid.toString())
                  .then((List<CQuestionaire> objQue) {
                        listQuestionaire = objQue;
                            _goToGuestVisit(objGuest, objDoc, objQue);
                  }).catchError((e){
      });
    })
        .catchError((e){
    });
  }
*/

  Future<CVisit> _callServiceCheckActiveVisitByKTP(CGuest objGuest, String strNoKTP) async{
    print("_callServiceCheckActiveVisitByKTP");

    CVisit result;
    svcVisit visitService = new svcVisit();

    await visitService.getActiveVisitbyguestCardId(strNoKTP)
        .then((CVisit objVisit) {
              if(objVisit.visitId > 0){
                // kalo session visit masih active maka redirect ke session page
                _goToGuestSession(objVisit);
              }
              else{
                // jika belum ada jadwal kunjungan maka redirect ke Visit Page
                _goToGuestVisit(objGuest);
              }
              result = objVisit;
        })
        .catchError((e){ });
    return result;
  }

  Future<CGuest> _callServiceCheckKTP(String strNoKTP) async{
    print("_callServiceCheckKTP");

    CGuest result;
    svcGuest guestService = new svcGuest();
    await guestService.getGuestByKTP(strNoKTP)
        .then((CGuest objResult) {
      result = objResult;

      // kalo belum terdaftar maka redirect ke Guest Register
      if(objResult.guestId == -1){
        // go to GuestRegister
        _goToGuestRegister();
      }
      else{
        // kalo sudah terdaftar maka lanjutkan check di session visit
        _callServiceCheckActiveVisitByKTP(objResult,strNoKTP);
      }
    })
        .catchError((e){
    });
    return result;
  }

  _goToGuestRegister(){
    print("_goToGuestRegister");
    // passing param
    Provider.of<CVisitModalParam>(context, listen: false).setNoKTP(strNoKTP);
    // direct to tab page Register
    widget.objTabController.animateTo(1);
    /*
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) =>
                GuestRegister(objSite: widget.objSite,
                    noKtp: strNoKTP)), (
            route) => false);
        */
  }

  _goToGuestVisit(CGuest objGuest){
    print("_goToGuestVisit");
    // passing param
    Provider.of<CVisitModalParam>(context, listen: false).setNoKTP(strNoKTP);
    Provider.of<CVisitModalParam>(context, listen: false).setObjGuest(objGuest);
    // direct to tab page Visit
    widget.objTabController.animateTo(2);
    /*
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) =>
                  VisitPage(objSite: widget.objSite,
                      objGuest: objGuest,
                      objDoc: listDocAssurance,
                      objQue: listQuestionaire
                  )), (
              route) => false);
           */
  }

  _goToGuestSession(CVisit objVisit){
    print("_goToGuestSession");
    // passing param
    Provider.of<CVisitModalParam>(context, listen: false).setObjVisit(objVisit);
    // direct to tab page Register
    widget.objTabController.animateTo(3);
    /*
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) =>
                GuestRegister(objSite: widget.objSite,
                    noKtp: strNoKTP)), (
            route) => false);
        */

    /*
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) {
                      return SessionPage(
                          objSite:  objVisit.objSite, //widget.objSite,
                          objGuest:  objVisit.objGuest, //objGuest,
                          objContact: objVisit.objContact, //  objSelectedContact,
                          objVisitReq: CVisitSaveReq(
                              siteid: widget.objSite.siteId,
                              guestid: objVisit.objGuest.guestId,     //widget.objGuest.guestId,
                              contactid: objVisit.objContact.contactId,   // objSelectedContact.contactId,
                              visitid: objVisit.visitId,
                              visitpurpose: objVisit.visitPurpose,  //strVisitPurpose,
                              contactaltname:  null // (checkNotListed == true) ? strVisitContactName : null
                          )
                      );
                    }));
                   */


  }

  /*
  Widget ProgressIndicator(String strTitle){
    return //CircularProgressIndicator();
      Container(
          padding: EdgeInsets.all(16),
          color: Colors.black.withOpacity(0.8),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                    child: Container(
                        child: CircularProgressIndicator(
                            strokeWidth: 3
                        ),
                        width: 32,
                        height: 32
                    ),
                    padding: EdgeInsets.only(bottom: 16)
                ),
                Text(strTitle,
                  style: TextStyle(color: Colors.white, fontSize: 12),
                  textAlign: TextAlign.center,
                )
              ]
          )
      );
  }
  */

  Widget Img_Avatar(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(8.0),
        topRight: Radius.circular(8.0),
      ),
      child: Image.asset('assets/images/avatar.png',
          width: 200,
          height: 100,
          fit:BoxFit.fill
      ),
    );
  }
  Widget Lbl_Welcome1(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 16.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Registrasi Manual Visitor \n', style: new TextStyle(fontWeight: FontWeight.bold)),
          new TextSpan(text: 'PT Gudang Garam Tbk.\n'),
          new TextSpan(text: 'Site : '+ widget.objSite.siteName)

        ],
      ),
    );
  }
  Widget Lbl_Welcome2(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 16.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'check No KTP / Paspor Visitor dibawah'),
        ],
      ),
    );
  }
  Widget Txt_NoKTP(BuildContext context) {
    return TextFormField(
      controller: _ctrlNoKTP,
      focusNode: NodeNoKTP,
      keyboardType: TextInputType.text,
      validator: validateNoKTP,
      onSaved: (String value) {
        strNoKTP = value;
      },
      style: TextStyle(fontSize: 16.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.document_scanner_outlined),
        labelText: 'No KTP/ No Paspor',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdCheckKTP);
      },
    );
  }
  Widget Cmd_CheckKTP(BuildContext context) {
    return InkWell(
        focusNode: NodeCmdCheckKTP,
        onTap: () {
          if (formInputKTPKey.currentState.validate()) {
            formInputKTPKey.currentState.save();
            print(strNoKTP);
            //getFutureSubmit(context, strNoKTP);
            _callServiceCheckKTP(strNoKTP);
          }
        },
        child: new Container(
          width: 150.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Check Identitas',
                    style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget form_welcome(BuildContext context) {
    return Form(
        key: formInputKTPKey,
        child: Column(
          children: <Widget>[
            Lbl_Welcome1(context),
            SizedBox(height: 30.0),
            Img_Avatar(context),
            SizedBox(height: 20.0),
            Lbl_Welcome2(context),
            SizedBox(height: 10.0),
            Txt_NoKTP(context),
            SizedBox(height: 20.0),
            Cmd_CheckKTP(context)
          ],
        )
    );
  }

  @override
  void initState() {
    // check shared preference if exist then checked
    //do_check_session();
    super.initState();
  }

  @override
  void dispose() {
    _ctrlNoKTP.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
              margin: const EdgeInsets.all(10.0),
              color: Colors.transparent,
              width: 500.0,
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  form_welcome(context)
                ],
              ),
            );
  }
}