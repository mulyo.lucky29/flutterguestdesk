import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:achievement_view/achievement_view.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/model/CContact.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CLookup.dart';
import 'package:myggguestdesk/model/CQuestionaire.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CVisitModalParam.dart';
import 'package:myggguestdesk/model/CVisitSaveReq.dart';
import 'package:myggguestdesk/screen/visit/contact/ContactList.dart';
import 'package:myggguestdesk/service/svcVisit.dart';
import 'package:myggguestdesk/service/svcGuest.dart';
import 'package:myggguestdesk/service/svcLookup.dart';
import 'package:myggguestdesk/service/svcQuestionaire.dart';
import 'package:myggguestdesk/tools/dialogBox.dart';
import 'package:myggguestdesk/validation/valVisit.dart';

class GuestVisit extends StatefulWidget {
  final TabController objTabController;
  final CSite objSite;

  Future fVisitInfo;
  GuestVisit({Key key,
    @required this.objTabController,
    @required this.objSite}) : super(key: key);

  @override
  _GuestVisitState createState() => new _GuestVisitState();
}

class _GuestVisitState extends State<GuestVisit> with valVisit{
  int _currentstep = 0;
  List<Step> _stepListW;

  CGuest objGuest;
  CVisit objVisit;
  List<CQuestionaire> objQue = [];
  List<CLookup> objDoc = [];

  bool isSwitched = false;
  final dialogBox message           = dialogBox();
  final scaffoldStateKey = GlobalKey<ScaffoldState>();
  final formVisitKey      = GlobalKey<FormState>();
  final formDeklarasiKey  = GlobalKey<FormState>();
  final formAssuranceKey  = GlobalKey<FormState>();

  CContact objSelectedContact;
  FocusNode NodeVisitContactName    = FocusNode();
  FocusNode NodeVisitPurpose        = FocusNode();
  FocusNode NodeContactEmail        = FocusNode();
  FocusNode NodeSwitch              = FocusNode();
  FocusNode NodeCmdSubmit           = FocusNode();
  FocusNode NodeNoDocAssurance      = FocusNode();

  TextEditingController _ctrlVisitContactName     = TextEditingController();
  TextEditingController _ctrlVisitPurpose         = TextEditingController();
  TextEditingController _ctrlContactEmail         = TextEditingController();
  TextEditingController _ctrlNoDocAssurance       = TextEditingController();

  String strVisitContactName    = '';
  String strVisitPurpose        = '';
  String strContactEmail   = '';
  String strNoDocAssurance = '';
  String strQuestionaire;

  bool checkNotListed = false;
  String docValueSelected = '';
  int docIndexSelected = 1;

  List<String> radioValues = [];
  List<CQuestionaire> filledQue = [];

  // /////////////////////////////////////////////////////////////////////////////////////////
  showProgressDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5),child:Text("Please Wait ... Submit Inprogress")),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }
  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "Form Validation", //strTitle,
      icon: pIcon,
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }

  Future<CVisit> _callServiceRegisVisit(CVisitSaveReq objVisitSaveReq) async{
    print('_callServiceRegisVisit');

    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.setRegisterVisit(objVisitSaveReq)
        .then((CVisit objVisit) {
            // if succeed saved visit then redirect to tab session
            setState(() {
              this.objVisit = objVisit;
              if(objVisit != null){
                _goToGuestSession(objVisit);
              }
            });
            result = objVisit;
        })
        .catchError((e){ });
    return result;
  }

  Future<void> _callInitServiceVisit(CSite objSite) async{
    print('_callInitServiceVisit');
    print('Que ID Used -> ' + widget.objSite.formaccessmentid.toString());

    svcLookup lookupService             = new svcLookup();
    svcQuestionaire questionaireService = new svcQuestionaire();

    await lookupService.getListLookupByGroup('AssuranceCard')
        .then((List<CLookup> objDoc) {
      setState(() {
        this.objDoc = objDoc;
      });
      questionaireService.getListQuestionaireById(objSite.formaccessmentid.toString())
          .then((List<CQuestionaire> objQue) {
        setState(() {
          this.objQue = objQue;
        });
        // if site assigned by que form then do load init
        if (objSite.formaccessmentid > 0) {
          setState(() {
            objQue.forEach((CQuestionaire que) {
              radioValues.add('');
            });
          });
        }
        //_goToGuestVisit(objGuest, objDoc, objQue);
      }).catchError((e){ });
    }).catchError((e){ });
  }

  // void _stepOnContinue() {
  //   int que_blank = 0;
  //   int idx = 0;
  //   if(widget.objSite.formaccessmentid > 0){
  //     setState(() {
  //       // using que
  //       if (_currentstep < _stepListW.length - 1) {
  //         if (_currentstep == 0 && formVisitKey.currentState.validate()) {
  //           formVisitKey.currentState.save();
  //           _currentstep = _currentstep + 1;
  //         }
  //         else if (_currentstep == 1 && formDeklarasiKey.currentState.validate()) {
  //           formDeklarasiKey.currentState.save();
  //           idx = 0;
  //           objQue.forEach((CQuestionaire que) {
  //             // filled questionaire
  //             filledQue.add(CQuestionaire(
  //                 quehid: que.quehid,
  //                 quedid: que.quedid,
  //                 quelineno: que.quelineno,
  //                 quetext: que.quetext,
  //                 quehname: que.quehname,
  //                 quehdesc: que.quehdesc,
  //                 queanswer: radioValues[idx].toString()
  //             ));
  //
  //             if(radioValues[idx] == ''){
  //               que_blank = que_blank + 1;
  //             }
  //             idx = idx + 1;
  //           });
  //
  //           strQuestionaire = jsonEncode(filledQue);
  //           print(strQuestionaire);
  //           // if its all filled then allow to move next step
  //           if(que_blank == 0){
  //             _currentstep = _currentstep + 1;
  //           }
  //           else{
  //             Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Lengkap');
  //           }
  //         }
  //         else if (_currentstep == 2 && formAssuranceKey.currentState.validate()) {
  //           formAssuranceKey.currentState.save();
  //           print('docValueSelected  -> ' + docValueSelected);
  //           print('strNoDocAssurance -> ' + strNoDocAssurance.toString());
  //
  //           if(docValueSelected == ''){
  //             Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
  //           }
  //           else{
  //             _currentstep = _currentstep + 1;
  //           }
  //         }
  //       }
  //       else {
  //         if (_currentstep == 3) {
  //           print('submit');
  //           submitVisit(context);
  //         }
  //       }
  //       print(_currentstep);
  //     });
  //   }
  //   else{
  //     // this whithout que
  //     setState(() {
  //       if (_currentstep < _stepListW.length - 1) {
  //         if (_currentstep == 0 && formVisitKey.currentState.validate()) {
  //           //strQuestionaire = null;
  //           strQuestionaire = "-1";
  //
  //           formVisitKey.currentState.save();
  //           _currentstep = _currentstep + 1;
  //         }
  //         else if (_currentstep == 1 && formAssuranceKey.currentState.validate()) {
  //           formAssuranceKey.currentState.save();
  //           print('docValueSelected  -> ' + docValueSelected);
  //           print('strNoDocAssurance -> ' + strNoDocAssurance.toString());
  //
  //           if(docValueSelected == ''){
  //             Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
  //           }
  //           else{
  //             _currentstep = _currentstep + 1;
  //           }
  //         }
  //       }
  //       else {
  //         if (_currentstep == 2) {
  //           print('submit');
  //           submitVisit(context);
  //         }
  //       }
  //       print(_currentstep);
  //     });
  //   }
  // }

  void _stepOnContinue() {
    int que_blank = 0;
    int idx = 0;
    if(widget.objSite.formaccessmentid > 0){
      setState(() {
        // using que
        if (_currentstep < _stepListW.length - 1) {
          if (_currentstep == 0 && formVisitKey.currentState.validate()) {
            formVisitKey.currentState.save();
            _currentstep = _currentstep + 1;
          }
          else if (_currentstep == 1 && formDeklarasiKey.currentState.validate()) {
            formDeklarasiKey.currentState.save();
            idx = 0;
            objQue.forEach((CQuestionaire que) {
              // filled questionaire
              filledQue.add(CQuestionaire(
                  quehid: que.quehid,
                  quedid: que.quedid,
                  quelineno: que.quelineno,
                  quetext: que.quetext,
                  quehname: que.quehname,
                  quehdesc: que.quehdesc,
                  queanswer: radioValues[idx].toString()
              ));

              if(radioValues[idx] == ''){
                que_blank = que_blank + 1;
              }
              idx = idx + 1;
            });

            strQuestionaire = jsonEncode(filledQue);
            print(strQuestionaire);
            // if its all filled then allow to move next step
            if(que_blank == 0){
              _currentstep = _currentstep + 1;
            }
            else{
              Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Lengkap');
            }
          }
        }
        else {
          if (_currentstep == 2 && formAssuranceKey.currentState.validate()) {
              formAssuranceKey.currentState.save();
              print('docValueSelected  -> ' + docValueSelected);
              print('strNoDocAssurance -> ' + strNoDocAssurance.toString());

              if(docValueSelected == ''){
                Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
              }
              else{
                submitVisit(context);
              }
            }
        }
        print(_currentstep);
      });
    }
    else{
      // this whithout que
      setState(() {
        if (_currentstep < _stepListW.length - 1) {
          if (_currentstep == 0 && formVisitKey.currentState.validate()) {
            //strQuestionaire = null;
            strQuestionaire = "-1";

            formVisitKey.currentState.save();
            _currentstep = _currentstep + 1;
          }
        }
        else {
          if (_currentstep == 1 && formAssuranceKey.currentState.validate()) {
            formAssuranceKey.currentState.save();
            print('docValueSelected  -> ' + docValueSelected);
            print('strNoDocAssurance -> ' + strNoDocAssurance.toString());

            if(docValueSelected == ''){
              Toast_Popup(context, Icon(Icons.error, color: Colors.white), Colors.red, 'Belum Dipilih');
            }
            else{
              submitVisit(context);
            }
          }
        }
        print(_currentstep);
      });
    }
  }

  void _stepOnCancel() {
    setState(() {
      if (_currentstep > 0) {
        _currentstep = _currentstep - 1;
      }
      else {
        _currentstep = 0;
        _goToGuestCheck();
      }
      print(_currentstep);
    });
  }
  void _stepOnSelect(int step) {
    setState(() {
      if (_currentstep == 0 && formVisitKey.currentState.validate()) {
        _currentstep = step;
      }
      print(_currentstep);
    });
  }


  void _goToContactList(BuildContext context)async {
    /*
    CContact selectedContact = await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
            ListContactPage(objSite: widget.objSite
            )));
    */
    CContact selectedContact = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ContactList(objSite: widget.objSite);
        }

    );
    setState(() {
      _ctrlVisitContactName.text = selectedContact.contactName;
      objSelectedContact = CContact(
          contactId: selectedContact.contactId,
          siteCode: selectedContact.siteCode,
          siteSpecific: selectedContact.siteSpecific,
          contactName: _ctrlVisitContactName.text,
          contactEmail: selectedContact.contactEmail,
          contactExt: selectedContact.contactExt
      );
    });
  }
  void _goToGuestCheck() async {
    // navigate to GuestCheck tab
    widget.objTabController.animateTo(0);
    /*
    await Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) {
          return HomePage(objSite: widget.objSite);
        }));
    */
  }
  void _goToGuestSession(CVisit objVisit) {
    print("_goToGuestSession");
    // passing param
    Provider.of<CVisitModalParam>(context, listen: false).setObjVisit(objVisit);
    // direct to tab page Register
    widget.objTabController.animateTo(3);
  }

  void submitVisit(BuildContext context) async {
    // service to save regis visit
    print('submitVisit');
    print('-------------------------------');
    print('siteid            : ' + widget.objSite.siteId.toString());
    print('guestid           : ' + objGuest.guestId.toString());
    print('contactid         : ' + objSelectedContact.contactId.toString());
    print('visitpurpose      : ' + strVisitPurpose);
    print('contactaltname    : ' + strVisitContactName);
    print('assuranceCardType : ' + docValueSelected);
    print('assuranceCardNo   : ' + strNoDocAssurance);
    print('formaccessment    : ' + strQuestionaire);

      _callServiceRegisVisit(
          CVisitSaveReq(
            siteid: widget.objSite.siteId,
            guestid: objGuest.guestId,
            contactid: objSelectedContact.contactId,
            visitid: -1,
            // to ensure this is new request not reload
            visitpurpose: strVisitPurpose,
            contactaltname: (checkNotListed == true)
                ? strVisitContactName
                : null,
            assuranceCardType: docValueSelected,
            assuranceCardNo: strNoDocAssurance,
            formaccessment: strQuestionaire
          )
      );

    // navigate to Parent Page GuestModal

    /*
    await Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) {
          return SessionPage(
              objSite: widget.objSite,
              objGuest: widget.objGuest,
              objContact: objSelectedContact,
              objVisitReq: CVisitSaveReq(
                  siteid: widget.objSite.siteId,
                  guestid: widget.objGuest.guestId,
                  contactid: objSelectedContact.contactId,
                  visitid: -1,
                  // to ensure this is new request not reload
                  visitpurpose: strVisitPurpose,
                  contactaltname: (checkNotListed == true)
                      ? strVisitContactName
                      : null,
                  assuranceCardType: docValueSelected,
                  assuranceCardNo: strNoDocAssurance,
                  formaccessment: strQuestionaire
              )
          );
        }));
       */
  }


  void clearContactSelected(BuildContext context){
    setState(() {
      // clear text input
      _ctrlVisitContactName.clear();
      // clear object selected
      objSelectedContact = CContact(
          contactId: null,
          siteCode: null,
          siteSpecific: null,
          contactName: null,
          contactEmail: null,
          contactExt: null,
          contactAltName: null
      );
    });
  }

  Widget Lbl_VisitInfo(BuildContext context, CGuest objGuest) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 16.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'INFORMASI KUNJUNGAN\n'),
          new TextSpan(text: objGuest.guestName == null ? '' : objGuest.guestName)
        ],
      ),
    );
  }
  Widget Lbl_FormDeklarasi(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 16.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'FORM SELF ASSESMENT PENGUNJUNG/TRACING')
        ],
      ),
    );
  }
  Widget Lbl_DocumentAssurance(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 16.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Penukaran identitas dengan access card')
        ],
      ),
    );
  }
  Widget Lbl_SubmitAsk(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Lanjutkan dengan mensubmit data \n dan mendapatkan Visit Session Kunjungan Anda')
        ],
      ),
    );
  }
  Widget Txt_Visit_Contact(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          controller: _ctrlVisitContactName,
          focusNode: NodeVisitContactName,
          keyboardType: TextInputType.text,
          validator: validateContactName,
          onSaved: (String value) {
            strVisitContactName = value;
          },
          readOnly: (checkNotListed == true) ? false : true,
          autofocus: false,
          decoration: InputDecoration(
            icon: const Icon(Icons.person),
            prefixIcon: checkNotListed == true ? null :
            IconButton(
              onPressed: () {
                // navigate to contact list
                _goToContactList(context);
              },
              icon: Icon(Icons.list),
            ),
            suffixIcon: IconButton(
              onPressed: () {
                clearContactSelected(context);
              },
              icon: Icon(Icons.clear),
            ),
            labelText: 'Nama orang yang di kunjungi',
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(NodeVisitPurpose);
          },
        ),
        Chk_Contact_Not_In_The_List(context),
      ],
    );
  }
  Widget Txt_Visit_Purpose(BuildContext context) {
    return TextFormField(
      controller: _ctrlVisitPurpose,
      focusNode: NodeVisitPurpose,
      validator: validateVisitPurpose,
      onSaved: (String value) {
        strVisitPurpose = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.paid),
        labelText: 'Tujuan kunjungan',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSubmit);
      },
    );
  }
  Widget Txt_No_DocumentAssurance(BuildContext context) {
    return TextFormField(
      controller: _ctrlNoDocAssurance,
      focusNode: NodeNoDocAssurance,
      validator: validateNoDocAssurance,
      onSaved: (String value) {
        strNoDocAssurance = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.card_membership),
        labelText: 'Nomor Identitas',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        //FocusScope.of(context).requestFocus(NodeCmdSubmit);
      },
    );
  }
  Widget Chk_Contact_Not_In_The_List(BuildContext context){
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(top: 5.0, left: 45.0, bottom: 5.0),
            child: Row(
              children: [
                Checkbox(
                  value: checkNotListed, //false, //this.value,
                  onChanged: (bool value) {
                    setState(() {
                      clearContactSelected(context);
                      checkNotListed = value;
                    });
                  },
                ),
                Text('Contact tidak ada di list',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12.0,
                  ),
                ),
              ],
            )
        )
      ],
    );
  }
  Widget Cmd_Next(BuildContext context, VoidCallback onStepContinue) {
    return InkWell(
        onTap: onStepContinue,
        child: new Container(
          width: 90.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Lanjut', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Back(BuildContext context, VoidCallback onStepCancel) {
    return InkWell(
        onTap: onStepCancel,
        child: new Container(
          width: 90.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Kembali',
                    style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }

  Widget _buildRadioGroup(BuildContext context, List<CLookup> _docAssurance){
    print('_buildRadioGroup');
    print("_docAssurance.length -> " + _docAssurance.length.toString() );

    return Column(
      children:
      _docAssurance.map((doc) => RadioListTile(
        title: Text(doc.lookupValue,  style: TextStyle(fontSize: 14.0, color: Colors.black )),
        subtitle: Text(doc.lookupDescription,  style: TextStyle(fontSize: 9.0, color: Colors.black )),
        value: doc.lookupValue,
        groupValue:  docValueSelected,
        dense: true,
        onChanged: (value) {
          setState(() {
            docValueSelected = value;
            // clear inputan jika pindah
            _ctrlNoDocAssurance.clear();
            // if doc selected as KTP then filled default with guest card id
            if(value.toString().toUpperCase() == 'KTP' || value.toString().toUpperCase() == 'PASPORT'){
              _ctrlNoDocAssurance  = TextEditingController(text: objGuest.guestCardId);
            }
          });
        },
      )).toList(),
    );
  }
  Widget _buildQuestionaire(BuildContext context, List<CQuestionaire> _queList){
    print('_buildQuestionaire -> ' + _queList.length.toString());
    return DataTable(
        columnSpacing: 10,
        dataRowHeight: 100,
        columns: [
          DataColumn(
            label: Text("No",
                style: new TextStyle(
                  fontSize: 14.0,
                  color: Colors.black,
                )
            ),
            numeric: true,
          ),
          DataColumn(
            label: Text("Pertanyaan",
              style: new TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          DataColumn(
            label: Text("Ya",
              style: new TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          DataColumn(
            label: Text("Tidak",
              style: new TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
        rows: _queList.map((que) =>
            DataRow(
                cells: [
                  DataCell(
                      Container(
                        width: 15,
                        child: Text(que.quelineno.toString()),
                      )
                  ),
                  DataCell(
                      Container(
                          width: 200,
                          child:  Text(que.quetext.toString())
                      )
                  ),
                  DataCell(
                    Container(
                      width: 15,
                      child:  Radio(
                        value: 'Y',
                        groupValue: radioValues[que.quelineno-1],
                        onChanged: (value) {
                          setState(() {
                            print('Y -> ' + value);
                            radioValues[que.quelineno-1] = value;
                          });
                        },
                      ),
                    ),
                  ),
                  DataCell(
                    Container(
                      width: 15,
                      child: Radio(
                        value: 'N',
                        groupValue: radioValues[que.quelineno-1],
                        onChanged: (value) {
                          setState(() {
                            print('N -> ' + value);
                            radioValues[que.quelineno-1] = value;
                          });
                        },
                      ),
                    ),
                  )
                ]
            )).toList()
    );
  }

  Widget form_Visit(BuildContext context) {
    print('form_Visit');
    return Form(
        key: formVisitKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 15.0),
            Txt_Visit_Contact(context),
            SizedBox(height: 5.0),
            Txt_Visit_Purpose(context),
            SizedBox(height: 10.0),
          ],
        )
    );
  }
  Widget form_deklarasi(BuildContext context) {
    print('form_deklarasi');
    return Form(
        key: formDeklarasiKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildQuestionaire(context, objQue),
          ],
        )
    );
  }
  Widget form_doc_assurance(BuildContext context) {
    print('form_doc_assurance');
    print(objDoc.length.toString());
    return Form(
        key: formAssuranceKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildRadioGroup(context,objDoc),
            SizedBox(height: 10.0),
            Txt_No_DocumentAssurance(context),
            SizedBox(height: 10.0),
          ],
        )
    );
  }

  Widget SectionVisitInfo(BuildContext context, CGuest objGuest, CSite objSite){
    return Column(
      children: [
        Lbl_VisitInfo(context, objGuest),
        SizedBox(height: 10.0),
        form_Visit(context),
      ],
    );
  }
  Widget SectionDeklarasi(BuildContext context, CSite objSite){
    return Column(
      children: [
        Lbl_FormDeklarasi(context),
        SizedBox(height: 10.0),
        form_deklarasi(context),
      ],
    );
  }
  Widget SectionDocumentAssurance(BuildContext context, CSite objSite){
    return Column(
      children: [
        Lbl_DocumentAssurance(context),
        SizedBox(height: 10.0),
        form_doc_assurance(context),
      ],
    );
  }

////////////////////////////////////////////////////////////////////////////////////////////
  // PRUNE Trail Step Submit Wizard
  // List<Step> _createStepsW(BuildContext context, int formId, CGuest objGuest, CSite objSite) {
  //   List<Step> _steps;
  //
  //   if(formId > 0){
  //     // with questionaire
  //     print("with questionaire");
  //
  //     _steps = [
  //       Step(
  //         state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
  //         title: new Text('Info'),
  //         content: SectionVisitInfo(context, objGuest, objSite),
  //         isActive: true,
  //       ),
  //       Step(
  //         state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
  //         title: new Text('Deklarasi'),
  //         content: SectionDeklarasi(context, objSite),
  //         isActive: true,
  //       ),
  //       Step(
  //         state: _currentstep == 2 ? StepState.editing : _currentstep > 2 ? StepState.complete : StepState.indexed,
  //         title: new Text('Dokumen'),
  //         content: SectionDocumentAssurance(context, objSite),
  //         isActive: true,
  //       ),
  //       Step(
  //         state: StepState.complete,
  //         title: new Text('Submit'),
  //         content: Text('Submit'), //Lbl_SubmitAsk(context),
  //         isActive: true,
  //       ),
  //     ];
  //   }
  //   else{
  //     // without questionaire
  //     print("without questionaire");
  //     _steps = [
  //       Step(
  //         state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
  //         title: new Text('Info'),
  //         content: SectionVisitInfo(context, objGuest, objSite),
  //         isActive: true,
  //       ),
  //       Step(
  //         state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
  //         title: new Text('Dokumen'),
  //         content: SectionDocumentAssurance(context, objSite),
  //         isActive: true,
  //       ),
  //       Step(
  //         state: StepState.complete,
  //         title: new Text('Submit'),
  //         content: Text('Submit'), //Lbl_SubmitAsk(context),
  //         isActive: true,
  //       ),
  //     ];
  //   }
  //   return _steps;
  // }

  List<Step> _createStepsW(BuildContext context, int formId, CGuest objGuest, CSite objSite) {
    List<Step> _steps;

    if(formId > 0){
      // with questionaire
      print("with questionaire");

      _steps = [
        Step(
          state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
          title: new Text('Info'),
          content: SectionVisitInfo(context, objGuest, objSite),
          isActive: true,
        ),
        Step(
          state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
          title: new Text('Deklarasi'),
          content: SectionDeklarasi(context, objSite),
          isActive: true,
        ),
        Step(
          state: _currentstep == 2 ? StepState.editing : _currentstep > 2 ? StepState.complete : StepState.indexed,
          title: new Text('Dokumen'),
          content: SectionDocumentAssurance(context, objSite),
          isActive: true,
        ),
      ];
    }
    else{
      // without questionaire
      print("without questionaire");
      _steps = [
        Step(
          state: _currentstep == 0 ? StepState.editing : _currentstep > 0 ? StepState.complete : StepState.indexed,
          title: new Text('Info'),
          content: SectionVisitInfo(context, objGuest, objSite),
          isActive: true,
        ),
        Step(
          state: _currentstep == 1 ? StepState.editing : _currentstep > 1 ? StepState.complete : StepState.indexed,
          title: new Text('Dokumen'),
          content: SectionDocumentAssurance(context, objSite),
          isActive: true,
        ),
      ];
    }
    return _steps;
  }

  Widget _buildContent(BuildContext context){
    print('_buildContent');
    return Center(
        child: Container(
            margin: const EdgeInsets.all(5.0),
            color: Colors.transparent,
            width: 500.0,
            child:  Stepper(
              controlsBuilder: (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                return Row(
                  children: <Widget>[
                    Cmd_Back(context, onStepCancel),
                    Cmd_Next(context, onStepContinue),
                  ],
                );
              },
              steps:_stepListW,
              type: StepperType.horizontal,
              currentStep: _currentstep,
              onStepContinue: _stepOnContinue,
              onStepCancel: _stepOnCancel,
              onStepTapped: _stepOnSelect,
            )
        )
    );
  }

  @override
  void initState() {
    super.initState();
    print(widget.objSite.siteCode);
    _callInitServiceVisit(widget.objSite);
  }

  @override
  void dispose() {
    _ctrlVisitContactName.dispose();
    _ctrlVisitPurpose.dispose();
    _ctrlContactEmail.dispose();
    _ctrlNoDocAssurance.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      this.objGuest = context.watch<CVisitModalParam>().objGuest;
    });
    // init step list
    _stepListW = _createStepsW(context, widget.objSite.formaccessmentid, objGuest, widget.objSite);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => svcGuest(),
        )
      ],
      child:  _buildContent(context)
    );

  }
}