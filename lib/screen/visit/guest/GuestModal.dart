import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/screen/visit/guest/GuestCheck.dart';
import 'package:myggguestdesk/screen/visit/guest/GuestRegister.dart';
import 'package:myggguestdesk/screen/visit/guest/GuestVisit.dart';
import 'package:myggguestdesk/screen/visit/guest/GuestSession.dart';

class GuestModal extends StatefulWidget {
  final CSite objSite;
  GuestModal({Key key, @required this.objSite}) : super(key: key);

  @override
  _GuestModalState createState() => new _GuestModalState();
}

class _GuestModalState extends State<GuestModal> with SingleTickerProviderStateMixin {
  TabController tabController;
  int active = 0;

  @override
  void initState() {
    super.initState();
    tabController = TabController(
        vsync: this,
        length: 4,
        initialIndex: 0
    )
      ..addListener(() {
        setState(() {
          active = tabController.index;
        });
      });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    return Container(
      margin: const EdgeInsets.all(10.0),
      color: Colors.transparent,
      width: 500.0,
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: tabController,
        children: [
            GuestCheck(objTabController: tabController,objSite: widget.objSite),
            GuestRegister(objTabController: tabController, objSite: widget.objSite),
            GuestVisit(objTabController: tabController, objSite: widget.objSite),
            GuestSession(objTabController: tabController, objSite: widget.objSite),
        ],
      )
    );
  }
}
