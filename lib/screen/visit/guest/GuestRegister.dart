import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';
import 'package:myggguestdesk/screen/visit/guest/GuestCheck.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CVisitModalParam.dart';
import 'package:myggguestdesk/validation/valRegistration.dart';
import 'package:myggguestdesk/tools/dialogBox.dart';
import 'package:myggguestdesk/service/svcGuest.dart';
import 'package:myggguestdesk/service/svcVisit.dart';


class GuestRegister extends StatefulWidget {
  final TabController objTabController;
  final CSite objSite;

  GuestRegister({Key key,
    @required this.objTabController,
    @required this.objSite}) : super(key: key);

  @override
  _GuestRegister createState() => new _GuestRegister();
}

class _GuestRegister extends State<GuestRegister> with valRegistration {
  Scaffold stagger;
  final dialogBox message           = dialogBox();

  final scaffoldStateKey = GlobalKey<ScaffoldState>();
  final formRegisterKey  = GlobalKey<FormState>(); // globalkey for validation

  FocusNode NodeNama        = FocusNode();
  FocusNode NodeNip         = FocusNode();
  FocusNode NodePhoneno     = FocusNode();
  FocusNode NodeEmail       = FocusNode();
  FocusNode NodeCompany     = FocusNode();
  FocusNode NodeSwitch      = FocusNode();
  FocusNode NodeCmdRegister = FocusNode();
  FocusNode NodeCmdCancel   = FocusNode();

  final TextEditingController _ctrlName    = TextEditingController();
  final TextEditingController _ctrlNip     = TextEditingController();
  final TextEditingController _ctrlPhoneno = TextEditingController();
  final TextEditingController _ctrlEmail   = TextEditingController();
  final TextEditingController _ctrlCompany = TextEditingController();

  String strName    = '';
  String strNip     = '';
  String strPhoneno = '';
  String strEmail   = '';
  String strCompany = '';

  bool _isLoading = false;

  _goToGuestSession(CVisit objVisit) {
    print("_goToGuestSession");
    // passing param
    Provider.of<CVisitModalParam>(context, listen: false).setObjVisit(objVisit);
    // direct to tab page Register
    widget.objTabController.animateTo(3);
  }

  _goToGuestVisit(CGuest objGuest, String strNoKTP){
    print("_goToGuestVisit");
    // passing param
    Provider.of<CVisitModalParam>(context, listen: false).setNoKTP(strNoKTP);
    Provider.of<CVisitModalParam>(context, listen: false).setObjGuest(objGuest);
    // direct to tab page Visit
    widget.objTabController.animateTo(2);
  }


  Future<CVisit> _callServiceCheckActiveVisitByKTP(CGuest objGuest, String strNoKTP) async{
    print("_callServiceCheckActiveVisitByKTP");

    CVisit result;
    svcVisit visitService = new svcVisit();

    await visitService.getActiveVisitbyguestCardId(strNoKTP)
        .then((CVisit objVisit) {
      if(objVisit.visitId > 0){
        // kalo session visit masih active maka redirect ke session page
        _goToGuestSession(objVisit);
      }
      else{
        // jika belum ada jadwal kunjungan maka redirect ke Visit Page
        _goToGuestVisit(objGuest, strNoKTP);
      }
      result = objVisit;
    })
        .catchError((e){ });
    return result;
  }


  Future<CGuest> _callServiceRegister(CGuest objGuest, String strNoKTP) async{
    print("_callServiceRegister");

    CGuest result;
    svcGuest guestService = new svcGuest();
    await guestService.setRegisterGuest(objGuest)
        .then((CGuest objResult) {

        // no need to display registrasi sukses
        //message.showMessageDialog(context, "Guest Registration","Registrasi Sukses");

        // change 2022-06-20: setelah register tidak usah redirect ke home check
        // redirect to Guest Check
        // widget.objTabController.animateTo(0);

        _callServiceCheckActiveVisitByKTP(objResult, strNoKTP);

        result = objResult;
    })
        .catchError((e){
    });
    return result;
  }

  void EH_Cmd_Register(BuildContext context) {
      if (!_isLoading) {
        // check trough validation first
        if (formRegisterKey.currentState.validate()) {
          formRegisterKey.currentState.save();
          // init loading to true
          setState(() { _isLoading = true; });
          try {
            // save info in regis page1 into class
            _callServiceRegister(
                CGuest(
                    guestId: 0,
                    //widget.siteparam.toString(),
                    guestCardId: strNip,
                    guestName: strName,
                    guestPhone: strPhoneno,
                    guestEmail: strEmail,
                    guestCompany: strCompany,
                    isActive: 'Y'
                ), strNip
            );
          }
          catch(e){
            print(e);
          }
          // set loading to false
          setState(() { _isLoading = false; });
        } // end if
      } // end if
      else{
        // clear progress dialog
        Navigator.pop(context);
      }
  }
  Widget Lbl_Welcome(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 20.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Registrasi Data Tamu')
        ],
      ),
    );
  }
  Widget Img_Logo(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(8.0),
        topRight: Radius.circular(8.0),
      ),
      child: Image.asset('assets/images/register.png',
          width: 300,
          height: 120,
          fit:BoxFit.fill
      ),
    );
  }
  Widget Txt_Nama(BuildContext context) {
    return TextFormField(
      controller: _ctrlName,
      focusNode: NodeNama,
      keyboardType: TextInputType.text,
      validator: validateName,
      onSaved: (String value) {
        strName = value;
      },
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.person),
        labelText: 'Full Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePhoneno);
      },
    );
  }
  Widget Txt_Nip(BuildContext context) {
    return TextFormField(
      enabled: false,
      readOnly: true,
      controller: _ctrlNip,
      focusNode: NodeNip,
      validator: validateNip,
      onSaved: (String value) {
        strNip = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.perm_identity),
        labelText: 'No KTP',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePhoneno);
      },
    );
  }
  Widget Txt_PhoneNo(BuildContext context) {
    return TextFormField(
      controller: _ctrlPhoneno,
      focusNode: NodePhoneno,
      validator: validatePhoneNo,
      onSaved: (String value) {
        strPhoneno = value;
      },
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly
      ],
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.phone),
        labelText: 'Mobile Phone Number',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeEmail);
      },
    );
  }
  Widget Txt_Email(BuildContext context) {
    return TextFormField(
      controller: _ctrlEmail,
      focusNode: NodeEmail,
      validator: validateEmail,
      onSaved: (String value) {
        strEmail = value;
      },
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.email),
        labelText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCompany);
      },
    );
  }
  Widget Txt_Company(BuildContext context) {
    return TextFormField(
      controller: _ctrlCompany,
      focusNode: NodeCompany,
      validator: validateCompany,
      onSaved: (String value) {
        strCompany = value;
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.business),
        labelText: 'Company Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeSwitch);
      },
    );
  }
  Widget Cmd_Register(BuildContext context) {
    return InkWell(
        focusNode: NodeCmdRegister,
        onTap: () => EH_Cmd_Register(context),
        child: new Container(
          width: 200.0,
          height: 40.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.login),
                Text('Submit',
                    style: TextStyle(fontSize: 16.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        focusNode: NodeCmdCancel,
        onTap: () {
          // back to Guest Check
          widget.objTabController.animateTo(0);
        },
        child: new Container(
          width: 200.0,
          height: 40.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel',
                    style: TextStyle(fontSize: 16.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget form_Register(BuildContext context) {
    return Column(
      children: [
        Lbl_Welcome(context),
        SizedBox(height: 10.0),
        Form(
            key: formRegisterKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Txt_Nama(context),
                SizedBox(height: 10.0),
                Txt_Nip(context),
                SizedBox(height: 10.0),
                Txt_PhoneNo(context),
                SizedBox(height: 10.0),
                Txt_Email(context),
                SizedBox(height: 10.0),
                Txt_Company(context),
                SizedBox(height: 10.0),
                Cmd_Register(context),
                Cmd_Cancel(context)
              ],
            )
        )
      ],
    );
  }

  @override
  void initState() {
    //_ctrlNip.text = widget.noKtp;
    super.initState();
  }

  @override
  void dispose() {
    _ctrlName.dispose();
    _ctrlNip.dispose();
    _ctrlPhoneno.dispose();
    _ctrlEmail.dispose();
    _ctrlCompany.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _ctrlNip.text = context.watch<CVisitModalParam>().strNoKTP;

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => svcGuest(),
        )
      ],
      child: Center(
                  child: Container(
                    margin: const EdgeInsets.all(10.0),
                    color: Colors.transparent,
                    width: 500.0,
                    child: ListView(
                      shrinkWrap: false,
                      padding: EdgeInsets.only(left: 24.0, right: 24.0),
                      children: <Widget>[
                        form_Register(context)
                      ],
                    ),
                  )
              )
    );

  }
}