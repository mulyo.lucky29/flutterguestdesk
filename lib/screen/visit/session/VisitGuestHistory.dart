import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/service/svcVisit.dart';


class VisitGuestHistory extends StatefulWidget {
  final TabController objTabController;
  final CUser objLoginAcc;
  final CVisit objVisit;

  VisitGuestHistory({Key key,
    @required this.objTabController,
    @required this.objLoginAcc,
    @required this.objVisit}) : super(key: key);

  @override
  _VisitGuestHistory createState() => new _VisitGuestHistory();
}

class _VisitGuestHistory extends State<VisitGuestHistory> {
  TextEditingController controller = new TextEditingController();
  List<CVisit> _searchResult = [];

  List<CVisit> _visitResultList = [];
  List<CVisit> _visitList   = [];

  List<String> _timeFilterChip = ['Last 7 Days', 'Last 14 Days', 'Last 30 Days','All'];
  List<int> _timeFilterValue = [7,14,30,0];
  int _selectedFilterIndex;

  var refreshKey = GlobalKey<RefreshIndicatorState>();
  Future<List<CVisit>> _futureVisitHistory;


  Future<List<CVisit>> getLoadVisitHistoryList(CVisit objVisit) async{
    final svcVisit  visitService = svcVisit();
    List<CVisit> result;
    var startDate;
    var endDate;
    final currentDate = DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now()));

    await visitService.getVisitListByGuestId(objVisit.objGuest.guestId.toString())
        .then((objResultHistDoc) {
          if (objResultHistDoc != null) {
            setState(() {
              // sort desc
              objResultHistDoc.sort((a, b) {
                return b.visitId.compareTo(a.visitId);
              });
              _visitResultList  = objResultHistDoc;
              _visitList        = objResultHistDoc;
              result = objResultHistDoc;
            });
            // set selected filter 30 days as default
            doSelectFilter(3);
          }
    }).catchError((e) {
      print("Error Load Visit History List -> " + e.toString());
    });

    return result;
  }


  doSelectFilter(int idx){
    DateTime currentDate;
    DateTime startDate;
    DateTime endDate;

    setState(() {
      // set selected filter to index
      _selectedFilterIndex = idx;
      print("doSelectFilter -> " + _timeFilterChip[idx]);

      // temp first
      _visitList = _visitResultList;
      // clear search
      _searchResult.clear();

      if(_timeFilterValue[idx] == 0) {
        _searchResult = _visitList
            .where((CVisit) => CVisit.visitDate != null).toList();
      }
      else{
        currentDate = DateFormat("yyyy-MM-dd").parse(DateFormat('yyyy-MM-dd').format(DateTime.now()));
        startDate   = currentDate.subtract(Duration(days: _timeFilterValue[idx])).add(Duration(days: 1));
        endDate     = currentDate.add(Duration(days: 1));

        print("currentDate    -> " + DateFormat('yyyy-MM-dd').format(currentDate));
        print("startdate      -> " + DateFormat('yyyy-MM-dd').format(startDate));
        print("endDate        -> " + DateFormat('yyyy-MM-dd').format(endDate));

        _searchResult = _visitList
            .where((CVisit) =>
        (DateFormat("yyyy-MM-dd").parse(CVisit.visitDate).isAfter(startDate) &&
            DateFormat("yyyy-MM-dd").parse(CVisit.visitDate).isBefore(endDate))
        ).toList();
      }
      _visitList = _searchResult;
    });
    setState(() { });
  }

  Widget Cmd_Back(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.blue),
        tooltip: 'Back',
        onPressed: (){
          // direct to tab page VisitGuestInfo(3)
          widget.objTabController.animateTo(3);
        },
      ),
    );
  }

  Widget Lbl_Title(BuildContext context) {
    return Row(
        children: [
          Expanded(flex: 1,
              child: Cmd_Back(context)
          ),
          Expanded(flex: 9,
              child: RichText(
                textAlign: TextAlign.center,
                text: new TextSpan(
                  style: new TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    new TextSpan(text: 'Visit History')
                  ],
                ),
              )
          ),
        ]
    );
  }

  Widget _buildTimeFilterChip(BuildContext context) {
    return Wrap(
      spacing: 6.0,
      runSpacing: 6.0,
      children: List<Widget>.generate(_timeFilterChip.length, (int index) {
        return ChoiceChip(
          label: Text(_timeFilterChip[index], style: TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0,)),
          selected: _selectedFilterIndex == index,
          selectedColor: Colors.green,
          onSelected: (bool selected) {
              if(selected == true){
                print('idx (' + index.toString() + ') -> ' + selected.toString());
                  doSelectFilter(index);
              }
          },
          backgroundColor: Colors.blue,
          labelStyle: TextStyle(color: Colors.white),
        );
      }),
    );
  }

  Widget _buildRowListLog(BuildContext context, CVisit objVisit){
    return Padding(
      padding: EdgeInsets.only(bottom: 5.0),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        elevation: 3.0,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          child: Container(
            //height: deviceSize.height * 0.10,
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(
                    width: 10.0,
                    color: Colors.green
                    //objCheck.checkTyped == 'CHECK-IN' ? Colors.blue : Colors.red  //strContent.bgStatus == 'ACT' ? Colors.green : Colors.red,
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Text(objVisit.visitDate,
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,),
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(objVisit.objSite.siteName,
                              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none),
                            ),
                          ]
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(objVisit.objContact.contactName,
                              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none),
                            ),
                          ]
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(objVisit.objContact.siteSpecific,
                              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none),
                            ),
                          ]
                      ),
                    ),

                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget ListViewHistory(BuildContext context){
    return Expanded(
      child: _searchResult.length != 0 || controller.text.isNotEmpty ?
      ListView.builder(
        padding: EdgeInsets.only(top: 5.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, i) {
          return _buildRowListLog(context, _searchResult[i]);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 5.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _visitList.length,
        itemBuilder: (context, index) {
          return _buildRowListLog(context, _visitList[index]);
        },
      ),
    );
  }

  Widget buildList(BuildContext context) {
    return  Column(
      children: <Widget>[
        Lbl_Title(context),
        Divider(),
        _buildTimeFilterChip(context),
        Divider(),
        ListViewHistory(context),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _futureVisitHistory = getLoadVisitHistoryList(widget.objVisit);
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Center(
      child: Container(
        margin: const EdgeInsets.all(10.0),
        color: Colors.transparent,
        width: 500.0,
        height: double.infinity,
        child: buildList(context),
      ),
    );
  }
}


