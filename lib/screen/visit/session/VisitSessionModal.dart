import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/screen/visit/session/VisitSession.dart';
import 'package:myggguestdesk/screen/visit/session/VisitCheckLog.dart';
import 'package:myggguestdesk/screen/visit/session/VisitQuestionaire.dart';
import 'package:myggguestdesk/screen/visit/session/VisitCardAssignment.dart';
import 'package:myggguestdesk/screen/visit/session/VisitEndSession.dart';
import 'package:myggguestdesk/screen/visit/session/VisitGuestInfo.dart';
import 'package:myggguestdesk/screen/visit/session/VisitPLCheck.dart';
import 'package:myggguestdesk/screen/visit/session/VisitGuestHistory.dart';

class VisitSessionModal extends StatefulWidget {
  final CUser objLoginAcc;
  final CVisit objVisit;
  final int defaultTab;

  VisitSessionModal({Key key,
    this.defaultTab,
    @required this.objLoginAcc,
    @required this.objVisit
  }) : super(key: key);

  @override
  _VisitSessionModal createState() => new _VisitSessionModal();
}

class _VisitSessionModal extends State<VisitSessionModal> with SingleTickerProviderStateMixin {
  TabController tabController;
  int active = 0;

  @override
  void initState() {
    super.initState();

    tabController = TabController(
        vsync: this,
        length: 8,
        initialIndex: widget.defaultTab == null ? 0 : widget.defaultTab
    )
      ..addListener(() {
        setState(() {
          active = tabController.index;
        });
      });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
        margin: const EdgeInsets.all(10.0),
        color: Colors.transparent,
        width: 500.0,
        child: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: tabController,
          children: [
            VisitSession(objTabController: tabController, objLoginAcc: widget.objLoginAcc ,objVisit: widget.objVisit),
            VisitCheckLog(objTabController: tabController, objLoginAcc: widget.objLoginAcc,objVisit: widget.objVisit),
            VisitQuestionaire(objTabController: tabController,objLoginAcc: widget.objLoginAcc,objVisit: widget.objVisit),
            VisitGuestInfo(objTabController: tabController,objLoginAcc: widget.objLoginAcc, objVisit: widget.objVisit),
            VisitCardAssignment(objTabController: tabController,objLoginAcc: widget.objLoginAcc, objVisit: widget.objVisit),
            VisitEndSession(objTabController: tabController,objLoginAcc: widget.objLoginAcc, objVisit: widget.objVisit),
            VisitPLCheck(objTabController: tabController,objLoginAcc: widget.objLoginAcc, objVisit: widget.objVisit),
            VisitGuestHistory(objTabController: tabController,objLoginAcc: widget.objLoginAcc, objVisit: widget.objVisit),
          ],
        )
    );
  }
}
