import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CCheckLog.dart';
import 'package:myggguestdesk/service/svcCheckLog.dart';

class VisitCheckLog extends StatefulWidget {
  final TabController objTabController;
  final CUser objLoginAcc;
  final CVisit objVisit;

  VisitCheckLog({Key key,
    @required this.objTabController,
    @required this.objLoginAcc,
    @required this.objVisit}) : super(key: key);

  /*
  final CSite objSite;
  VisitCheckLog({Key key,
    @required this.objTabController,
    @required this.objSite,
    @required this.objVisit}) : super(key: key);
  */

  @override
  _VisitCheckLogState createState() => new _VisitCheckLogState();
}

class _VisitCheckLogState extends State<VisitCheckLog> {
  TextEditingController controller = new TextEditingController();
  List<CCheckLog> _searchResult = [];
  List<CCheckLog> _logList = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<void> getLoadInitService(strVisitId) async{
    final svcCheckLog  logService = svcCheckLog();
    // load service log
    await logService.getListCheckLogByVisitId(strVisitId).then((objResult) {
      if (objResult != null) {
        setState(() {
          _logList = objResult;
          _logList.sort((a, b) {
            return b.checkLogId.compareTo(a.checkLogId);
          });
        });
      }
    }).catchError((e) {
      print("Error Load Log List -> " + e.toString());
    });
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _logList.forEach((CCheckLog) {
      if (CCheckLog.checkTyped.toUpperCase().contains(text.toUpperCase()))
        _searchResult.add(CCheckLog);
    });
    setState(() {});
  }

  Widget Cmd_Back(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.blue),
        tooltip: 'Back',
        onPressed: (){
          // direct to tab page Visit Session (0)
          widget.objTabController.animateTo(0);
        },
      ),
    );
  }

  Widget Lbl_Title(BuildContext context) {
    return Row(
        children: [
          Expanded(flex: 1,
              child: Cmd_Back(context)
          ),
          Expanded(flex: 9,
              child: RichText(
                textAlign: TextAlign.center,
                text: new TextSpan(
                  style: new TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    new TextSpan(text: 'Visit Check Log')
                  ],
                ),
              )
          ),
        ]
    );
  }

  Widget SearchBar(BuildContext context){
    return Container(
      child: new Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Card(
          child: new ListTile(
            leading: new Icon(Icons.search),
            title: new TextField(
              controller: controller,
              decoration: new InputDecoration(
                  isDense: true,
                  hintText: 'Cari Log', border: InputBorder.none),
              onChanged: onSearchTextChanged,
            ),
            trailing: new IconButton(icon: new Icon(Icons.cancel),
              onPressed: () {
                controller.clear();
                onSearchTextChanged('');
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildRowListLog(BuildContext context, CCheckLog objCheck){

    return Padding(
      padding: EdgeInsets.only(bottom: 5.0),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        elevation: 3.0,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          child: Container(
            //height: deviceSize.height * 0.10,
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(
                    width: 10.0,
                    color: objCheck.checkTyped == 'CHECK-IN' ? Colors.blue : Colors.red  //strContent.bgStatus == 'ACT' ? Colors.green : Colors.red,
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Text(objCheck.checkTyped,
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,),
                      ),
                    ),
                    Expanded(
                      flex: 7,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(objCheck.checkedTime,
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0, decoration: TextDecoration.none),
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }

  Widget ListViewLog(BuildContext context){
    return Expanded(
      child: _searchResult.length != 0 || controller.text.isNotEmpty ?
      ListView.builder(
        padding: EdgeInsets.only(top: 5.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, i) {
          return _buildRowListLog(context, _searchResult[i]);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 5.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _logList.length,
        itemBuilder: (context, index) {
          return _buildRowListLog(context, _logList[index]);
        },
      ),
    );
  }

  Widget buildList() {
    return  Column(
      children: <Widget>[
        Lbl_Title(context),
        Divider(),
        SearchBar(context),
        ListViewLog(context),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    getLoadInitService(widget.objVisit.visitId.toString());
  }

  @override
  Widget build(BuildContext context) {
    return  Center(
      child: Container(
        margin: const EdgeInsets.all(10.0),
        color: Colors.transparent,
        width: 500.0,
        height: double.infinity,
        child: buildList(),
      ),
    );
  }
}


