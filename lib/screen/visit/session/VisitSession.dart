import 'package:flutter/material.dart';
import 'dart:async';
import 'package:achievement_view/achievement_view.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CCheckLog.dart';
import 'package:myggguestdesk/model/CCheckLogReq.dart';
import 'package:myggguestdesk/service/svcCheckLog.dart';
import 'package:myggguestdesk/widget/VisitorCard.dart';

class VisitSession extends StatefulWidget {
  final TabController objTabController;
  final CUser objLoginAcc;
  final CVisit objVisit;

  VisitSession({Key key,
    @required this.objTabController,
    @required this.objLoginAcc,
    @required this.objVisit}) : super(key: key);

  @override
  _VisitSessionState createState() => new _VisitSessionState();
}

class _VisitSessionState extends State<VisitSession> {
  Scaffold stagger;
  Future myFutureVisit;
  CVisit objVisit;
  bool _isLoading = false;

  /*
  Future<CVisit> _callServiceReloadVisit(CVisitSaveReq objVisitSaveReq) async{
    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.getVisitById(objVisitSaveReq.visitid.toString())
        .then((CVisit objResult) {
      setState(() {
        objVisit = objResult;
      });
      result = objResult;
    })
        .catchError((e){
    });
    return result;
  }
  */

  Future<CCheckLog> _do_checkInOut(CCheckLogReq objCheckLogReq) async{
    print("_do_checkInOut");

    CCheckLog result;
    svcCheckLog checkLogService = new svcCheckLog();
    await checkLogService.setDoCheckLog(objCheckLogReq)
        .then((CCheckLog objResult) {
            result = objResult;
            if(objResult.checkLogId > 0){
              Toast_Popup(context, Icon(Icons.done, color: Colors.white,), Colors.blue, objResult.checkTyped + " Success");
            }
            else{
              Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Visit Already Ended\nCheck Log Failed");
              // delay 3 sec
              Timer(Duration(seconds: 3), () {
                Navigator.of(context).pop();
              });
            }
    }).catchError((e){ });
    return result;
  }

  void goToLogList(BuildContext context, String strVisitId)async {
    // redirect to CheckLog Tab
    widget.objTabController.animateTo(1);
    /*
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
            VisitCheckLog(strVisitId: strVisitId)
        ));
    */
  }
  void goToQueForm(BuildContext context, String strVisitId)async {
    // redirect to CheckLog Tab
    widget.objTabController.animateTo(2);
    /*
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) =>
            VisitCheckLog(strVisitId: strVisitId)
        ));
    */
  }
  Widget Lbl_VisitDate(BuildContext context, CVisit objVisit){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
            RichText(
            textAlign: TextAlign.center,
            text: new TextSpan(
                style: new TextStyle(
                  fontSize: 12.0,
                  color: Colors.black,
                ),
                children: <TextSpan>[
                  new TextSpan(text: 'Visit Date : ' + objVisit.visitDate)
                ],
              ),
            )
      ],
    );
  }
  Widget Lbl_Title(BuildContext context, CUser objLoginAcc, CVisit objVisit) {
    return Row(
      children: [
            Expanded(flex: 1,
                child: Cmd_Back(context, objVisit)
            ),
            Expanded(flex: 6,
                child: RichText(
                  textAlign: TextAlign.center,
                  text: new TextSpan(
                    style: new TextStyle(
                      fontSize: 20.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(text: 'Visitor Detail Session')
                    ],
                  ),
                )
            ),
            Expanded(flex: 3,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                        Cmd_ViewLog(context, objVisit),
                        Cmd_ViewQue(context, objVisit),
                    ],
                ),
            ),
      ]
    );
  }


  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "", //strTitle,
      icon: pIcon, // Icon(Icons.done, color: Colors.white,)
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }

  Widget Cmd_Back(BuildContext context, CVisit objVisit) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.blue),
        tooltip: 'Back',
        onPressed: (){
          Navigator.pop(context);
        },
      ),
    );
  }
  Widget Cmd_ViewLog(BuildContext context, CVisit objVisit) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.history, color: Colors.blue),
        tooltip: 'View Check Log',
        onPressed: (){
            goToLogList(context, objVisit.visitId.toString());
        },
      ),
    );
  }

  Widget  Cmd_Check(BuildContext context, CUser objLoginAcc, CVisit objVisit) {
    return InkWell(
        //focusNode: NodeCmdSave,
        onTap: () {
          if (!_isLoading) {
            print("objVisit.objSite.siteCode ->" + objVisit.objSite.siteCode);
            print("objSite.siteCode          ->" + objLoginAcc.objSite.siteCode);

            // init loading to true
            setState(() { _isLoading = true; });

            // tidak bisa check in/out di site lain selain di site yang sedang active
            if(objVisit.objSite.siteCode != objLoginAcc.objSite.siteCode) {
              Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Guest Session Still Active In Other Site");
            }
            else{
              _do_checkInOut(
                  CCheckLogReq(
                      visitSessionCode: objVisit.visitSessionCode,
                      terminalId: objLoginAcc.username
                  ));
            }
              // set loading to false
              setState(() { _isLoading = false; });
          } // end if
          else{
            // clear progress dialog
            Navigator.pop(context);
          }
        },
        child: new Container(
          width: 130.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Check-In/Out',
                    style: TextStyle(fontSize: 10.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }

  /*
  Widget Cmd_Check(BuildContext context, CUser objLoginAcc, CVisit objVisit) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.lock_clock, color: Colors.blue),
        tooltip: 'Check-In/Out',
        onPressed: (){
          print("objVisit.objSite.siteCode ->" + objVisit.objSite.siteCode);
          print("objSite.siteCode          ->" + objLoginAcc.objSite.siteCode);

          // tidak bisa check in/out di site lain selain di site yang sedang active
          if(objVisit.objSite.siteCode != objLoginAcc.objSite.siteCode) {
              Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Guest Session Still Active In Other Site");
          }
          else{
            _do_checkInOut(
                CCheckLogReq(
                    visitSessionCode: objVisit.visitSessionCode,
                    terminalId: objLoginAcc.username
                ));
          }
        },
      ),
    );
  }
  */

  Widget Cmd_ViewQue(BuildContext context, CVisit objVisit) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.question_answer, color: Colors.blue),
        tooltip: 'View Visit Questionaire',
        onPressed: (){
          goToQueForm(context, objVisit.visitId.toString());
        },
      ),
    );
  }

  Widget session_page(BuildContext context,  CUser objLoginAcc, CVisit objVisit){
    return Center(
        child: Container(
          //margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          width: 500.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                          Lbl_Title(context, objLoginAcc, objVisit),
                          Divider(),
                          Lbl_VisitDate(context, objVisit),
                          Divider(),
                          VisitorCard(objVisit: objVisit),
                          SizedBox(height: 5),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              (objVisit.endVisitDate == null ? Cmd_Check(context,objLoginAcc,objVisit) : Column()),
                            ],
                          ),
                      ]
                  )
              ),
            ],
          ),
        )
    ); // end container
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //return Text("");
    return Container(
      margin: const EdgeInsets.all(10.0),
      color: Colors.transparent,
      width: 500.0,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        children: <Widget>[
            session_page(context, widget.objLoginAcc ,widget.objVisit),
        ],
      ),
    );
  }
}