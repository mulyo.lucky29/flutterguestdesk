import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CVisitCardAssign.dart';
import 'package:myggguestdesk/service/svcVisit.dart';
import 'package:myggguestdesk/validation/valCardAssignment.dart';
import 'package:myggguestdesk/tools/dialogBox.dart';


class VisitCardAssignment extends StatefulWidget {
  final TabController objTabController;
  final CUser objLoginAcc;
  final CVisit objVisit;

  VisitCardAssignment({Key key,
    @required this.objTabController,
    @required this.objLoginAcc,
    @required this.objVisit}) : super(key: key);


  /*
  final CSite objSite;
  VisitCardAssignment({Key key,
    @required this.objTabController,
    @required this.objSite,
    @required this.objVisit}) : super(key: key);
  */

  @override
  _VisitCardAssignment createState() => new _VisitCardAssignment();
}

class _VisitCardAssignment extends State<VisitCardAssignment> with valCardAssignment {
  final formCardAssign  = GlobalKey<FormState>();
  final dialogBox message           = dialogBox();

  bool _isLoading = false;

  FocusNode NodeAccessCardNo  = FocusNode();
  FocusNode NodePairNo        = FocusNode();
  FocusNode NodeCmdSave       = FocusNode();
  FocusNode NodeCmdCancel     = FocusNode();

  TextEditingController _ctrlAccessCardNo;
  TextEditingController _ctrlPairNo;

  String strAccessCardNo = '';
  String strPairNo = '';


  Future<CVisit> _callAssignAccessCard(CVisitCardAssign objCardAssign) async{
    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.setAssignAccessCardVisit(objCardAssign)
        .then((CVisit objResult) {
              result = objResult;
        }).catchError((e){ });
    return result;
  }


  void EH_Cmd_Save(BuildContext context) {
    if (!_isLoading) {
      // check trough validation first
      if (formCardAssign.currentState.validate()) {
        formCardAssign.currentState.save();
        // init loading to true
        setState(() { _isLoading = true; });
        try {
            _callAssignAccessCard(
                CVisitCardAssign(
                    visitId: widget.objVisit.visitId,
                    accessCardTag: strAccessCardNo,
                    accessCardPairNo: strPairNo,
                    username: ""
                )
            ).then((value) => Navigator.pop(context));
        }
        catch(e){
          print(e);
        }
        // set loading to false
        setState(() { _isLoading = false; });
      } // end if
    } // end if
    else{
      // clear progress dialog
      Navigator.pop(context);
    }
  }

  Widget Avatar(BuildContext context){
    return Container(
      width: 200.0,
      height: 120.0,
      decoration: new BoxDecoration(
        color: Colors.transparent,
        border: new Border.all(color: Colors.white, width: 2.0),
        borderRadius: new BorderRadius.circular(10.0),
      ),
      child: new Center(
        child: Image.asset('assets/images/nametag.png')
      ),
    );
  }
  Widget Lbl_Title(BuildContext context) {
    return Row(
        children: [
        //Expanded(flex: 2,
        //    child:
        //),
        Expanded(flex: 5,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 24.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: 'Access Card Assignment')
                        ],
                      ),
                    )
            )
        ],
      );
  }

  Widget Txt_AccessCardNo(BuildContext context) {
    return TextFormField(
      controller: _ctrlAccessCardNo,
      focusNode: NodeAccessCardNo,
      keyboardType: TextInputType.text,
      validator: validateAccessCardNo,
      onSaved: (String value) {
        strAccessCardNo = value;
      },
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.card_membership),
        labelText: 'Access Card No',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePairNo);
      },
    );
  }
  Widget Txt_PairNo(BuildContext context) {
    return TextFormField(
      controller: _ctrlPairNo,
      focusNode: NodePairNo,
      keyboardType: TextInputType.text,
      validator: validatePairNo,
      onSaved: (String value) {
        strPairNo = value;
      },
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.assignment),
        labelText: 'Access Card Pair Tag',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSave);
      },
    );
  }

  Widget Cmd_Save(BuildContext context) {
    return InkWell(
        focusNode: NodeCmdSave,
        onTap: () => EH_Cmd_Save(context),
        child: new Container(
          width: 100.0,
          height: 40.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.save),
                Text('Save',
                    style: TextStyle(fontSize: 16.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        focusNode: NodeCmdCancel,
        onTap: () {
          Navigator.pop(context);
        },
        child: new Container(
          width: 100.0,
          height: 40.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel',
                    style: TextStyle(fontSize: 16.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget _buildFormCardAssign(BuildContext context) {
    return Form(
        key: formCardAssign,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Avatar(context),
            SizedBox(height: 10.0),
            Lbl_Title(context),
            Divider(),
            SizedBox(height: 15.0),
            Txt_AccessCardNo(context),
            SizedBox(height: 15.0),
            Txt_PairNo(context),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Cmd_Cancel(context),
                Cmd_Save(context),
              ],
            ),
          ],
        )
    );
  }


  @override
  void initState() {
    super.initState();
    //  init state
    setState(() {
      if(widget.objVisit == null){
        _ctrlAccessCardNo       = TextEditingController();
        _ctrlPairNo             = TextEditingController();
      }
      else{
        _ctrlAccessCardNo       = TextEditingController(text: widget.objVisit.accessCardTag);
        _ctrlPairNo             = TextEditingController(text: widget.objVisit.accessCardPairNo);
      }
    });
  }

  @override
  void dispose() {
    _ctrlAccessCardNo.dispose();
    _ctrlPairNo.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _buildFormCardAssign(context);

    /*
    return  Center(
      child: Container(
        margin: const EdgeInsets.all(10.0),
        color: Colors.transparent,
        width: 500.0,
        height: double.infinity,
        child: buildContent(),
      ),
    );
    */

  }
}


