import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:web_browser/web_browser.dart';
import 'package:easy_web_view/easy_web_view.dart';
import 'dart:ui' as ui;

class VisitPLCheck extends StatefulWidget {
  final TabController objTabController;
  final CUser objLoginAcc;
  final CVisit objVisit;

  VisitPLCheck({Key key,
    @required this.objTabController,
    @required this.objLoginAcc,
    @required this.objVisit}) : super(key: key);

  @override
  _VisitPLCheckState createState() => new _VisitPLCheckState();
}

class _VisitPLCheckState extends State<VisitPLCheck> {
  final GlobalKey webViewKey = GlobalKey();

  String url = "";
  double progress = 0;
  final urlController = TextEditingController();

  /*
  InAppWebViewController webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));


  //late PullToRefreshController pullToRefreshController;
  */

  Widget buildContent() {
    return  Column(
      children: <Widget>[

      ],
    );
  }

  Widget _buildWebView(BuildContext context) {
    /*
      return Expanded(
        child: Stack(
          children: [
            InAppWebView(
              key: webViewKey,
              initialUrlRequest:
              URLRequest(url: Uri.parse("https://inappwebview.dev/")),
              initialOptions: options,
              //pullToRefreshController: pullToRefreshController,
              onWebViewCreated: (controller) {
                webViewController = controller;
              },
              onLoadStart: (controller, url) {
                setState(() {
                  this.url = url.toString();
                  urlController.text = this.url;
                });
              },
              androidOnPermissionRequest: (controller, origin, resources) async {
                return PermissionRequestResponse(
                    resources: resources,
                    action: PermissionRequestResponseAction.GRANT);
              },
              shouldOverrideUrlLoading: (controller, navigationAction) async {
                var uri = navigationAction.request.url;

                if (![ "http", "https", "file", "chrome",
                "data", "javascript", "about"].contains(uri.scheme)) {
                if (await canLaunch(url)) {
                // Launch the App
                await launch(url,);
                // and cancel the request
                return NavigationActionPolicy.CANCEL;
                }
                }

                return NavigationActionPolicy.ALLOW;
              },
              onLoadStop: (controller, url) async {
                //pullToRefreshController.endRefreshing();
                setState(() {
                  this.url = url.toString();
                  urlController.text = this.url;
                });
              },
              onLoadError: (controller, url, code, message) {
                //pullToRefreshController.endRefreshing();
              },
              onProgressChanged: (controller, progress) {
                if (progress == 100) {
                  //pullToRefreshController.endRefreshing();
                }
                setState(() {
                  this.progress = progress / 100;
                  urlController.text = this.url;
                });
              },
              onUpdateVisitedHistory: (controller, url, androidIsReload) {
                setState(() {
                  this.url = url.toString();
                  urlController.text = this.url;
                });
              },
              onConsoleMessage: (controller, consoleMessage) {
                print(consoleMessage);
              },
            ),
            progress < 1.0
                ? LinearProgressIndicator(value: progress)
                : Container(),
          ],
        ),
      );
    */
    return Container(
        height: double.infinity,
        width: 500, //double.infinity,
        child: EasyWebView(
          src: 'https://cekmandiri.pedulilindungi.id/',
          isHtml: false,
          isMarkdown: false,
          convertToWidgets: false,
          onLoaded: () {  },
        )

        /*
        WebBrowser(
          initialUrl: 'https://cekmandiri.pedulilindungi.id/',
          javascriptEnabled: true,
        )
        */
        );
  }

  @override
  Widget build(BuildContext context) {

    /*
    return  Center(
      child: Container(
        margin: const EdgeInsets.all(10.0),
        color: Colors.transparent,
        width: 500.0,
        height: double.infinity,
        child: buildContent(),
      ),
    );
    */
    return _buildWebView(context);

    /*
      Container(
      height: MediaQuery.of(context).size.height / 4,
      child: new Container(
        decoration: new BoxDecoration(color: Colors.amber, borderRadius: new BorderRadius.only(topLeft: const Radius.circular(20.0), topRight: const Radius.circular(20.0))),
        child: Container(
            child:  EasyWebView(
              src: "https://cekmandiri.pedulilindungi.id",
              isHtml: false, // Use Html syntax
              isMarkdown: false, // Use markdown syntax
              convertToWidgets: false, // Try to convert to flutter widgets
              //width: 500,
              //height: 800,
            )
        ),
      ),
    );
    */
  }
}


