import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';
import 'dart:convert';
import 'package:myggguestdesk/tools/dialogBox.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CQuestionaire.dart';

class VisitQuestionaire extends StatefulWidget {
  final TabController objTabController;
  final CUser objLoginAcc;
  final CVisit objVisit;

  VisitQuestionaire({Key key,
    @required this.objTabController,
    @required this.objLoginAcc,
    @required this.objVisit}) : super(key: key);

  /*
  final CSite objSite;
  VisitQuestionaire({Key key,
    @required this.objTabController,
    @required this.objSite,
    @required this.objVisit}) : super(key: key);
  */

  @override
  _VisitQuestionaire createState() => new _VisitQuestionaire();
}

class _VisitQuestionaire extends State<VisitQuestionaire> { //}with valRegistration {
  Scaffold stagger;
  final dialogBox message           = dialogBox();

  Widget Cmd_Back(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.blue),
        tooltip: 'Back',
        onPressed: (){
          // direct to tab page Visit Session (0)
          widget.objTabController.animateTo(0);
        },
      ),
    );
  }

  Widget Lbl_Title(BuildContext context, String strTitle) {
    return Row(
        children: [
          Expanded(flex: 1,
              child: Cmd_Back(context)
          ),
          Expanded(flex: 9,
              child: RichText(
                textAlign: TextAlign.center,
                text: new TextSpan(
                  style: new TextStyle(
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    new TextSpan(text: strTitle)
                  ],
                ),
              )
          ),
        ]
    );
  }

  Widget _buildQueInfo(BuildContext context, CVisit objVisit){
    return Column(
      children: [
          Column(
              children: [
                  Row(
                      children: [
                        Expanded(flex: 3, child: Text("Tanggal" , style: TextStyle(fontSize: 12))),
                        Expanded(flex: 7, child: Text(objVisit.visitDate == null ? '-' : objVisit.visitDate,style: TextStyle(fontSize: 12))),
                      ],
                  )
              ],
          ),
          Column(
              children: [
                Row(
                  children: [
                    Expanded(flex: 3, child: Text("Nama", style: TextStyle(fontSize: 12))),
                    Expanded(flex: 7, child: Text(objVisit.objGuest.guestName == null ? '-' : objVisit.objGuest.guestName, style: TextStyle(fontSize: 12))),
                  ],
                )
              ],
          ),
          Column(
            children: [
              Row(
                children: [
                  Expanded(flex: 3, child: Text("No KTP", style: TextStyle(fontSize: 12))),
                  Expanded(flex: 7, child: Text(objVisit.objGuest.guestCardId == null ? '-' : objVisit.objGuest.guestCardId, style: TextStyle(fontSize: 12))),
                ],
              )
            ],
          ),
          Column(
            children: [
              Row(
                children: [
                  Expanded(flex: 3, child: Text("No HP", style: TextStyle(fontSize: 12))),
                  Expanded(flex: 7, child: Text(objVisit.objGuest.guestPhone == null ? '-' : objVisit.objGuest.guestPhone, style: TextStyle(fontSize: 12))),
                ],
              )
            ],
          ),
          Column(
            children: [
              Row(
                children: [
                  Expanded(flex: 3, child: Text("Jam Datang", style: TextStyle(fontSize: 12))),
                  Expanded(flex: 7, child: Text(objVisit.startVisitDate == null ? '-' : objVisit.startVisitDate, style: TextStyle(fontSize: 12))),
                ],
              )
            ],
          ),
          Column(
            children: [
              Row(
                children: [
                  Expanded(flex: 3, child: Text("Jam Keluar", style: TextStyle(fontSize: 12))),
                  Expanded(flex: 7, child: Text(objVisit.endVisitDate == null ? '-' : objVisit.endVisitDate, style: TextStyle(fontSize: 12))),
                ],
              )
            ],
          ),
      ],
    );
  }
  Widget _buildQuestionaire(BuildContext context, List<CQuestionaire> _queList){
    print('_buildQuestionaire -> ' + _queList.length.toString());

    return DataTable(
        columnSpacing: 10,
        dataRowHeight: 100,
        columns: [
          DataColumn(
            label: Text("No",
                style: new TextStyle(
                  fontSize: 14.0,
                  color: Colors.black,
                )
            ),
            numeric: true,
          ),
          DataColumn(
            label: Text("Pertanyaan",
              style: new TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          DataColumn(
            label: Text("Ya",
              style: new TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          DataColumn(
            label: Text("Tidak",
              style: new TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
        rows: _queList.map((que) =>
            DataRow(
                cells: [
                  DataCell(
                      Container(
                        width: 15,
                        child: Text(que.quelineno.toString()),
                      )
                  ),
                  DataCell(
                      Container(
                          width: 200,
                          child: Text(que.quetext.toString())
                      )
                  ),
                  DataCell(
                    Container(
                      width: 15,
                      child:  Radio(
                        value: "Y",
                        groupValue: que.queanswer,
                      ),
                    ),
                  ),
                  DataCell(
                    Container(
                      width: 15,
                      child: Radio(
                        value: "N",
                        groupValue: que.queanswer,
                      ),
                    ),
                  )
                ]
            )
          ).toList()
    );
  }


  @override
  void initState() {
    super.initState();
    print(widget.objVisit.formaccessment);
  }


  @override
  void dispose() {
    super.dispose();
  }

  Widget _buildContent(BuildContext context){
    Iterable l =  json.decode(widget.objVisit.formaccessment);
    //List<Post> posts = List<Post>.from(l.map((model)=> Post.fromJson(model)));
    List<CQuestionaire> que = List<CQuestionaire>.from(l.map((model)=> CQuestionaire.fromJson(model)));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
          Lbl_Title(context,que[0].quehdesc),
          Divider(),
          _buildQueInfo(context, widget.objVisit),
          Divider(),
          _buildQuestionaire(context, que),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {

    return Container(
         margin: const EdgeInsets.all(10.0),
         color: Colors.transparent,
         width: 500.0,
         child: ListView(
            shrinkWrap: false,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
                _buildContent(context)
            ],
         ),
    );
  }
}