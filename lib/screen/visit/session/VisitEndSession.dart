import 'dart:async';
import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/model/CVisitEndReq.dart';
import 'package:myggguestdesk/service/svcVisit.dart';
import 'package:achievement_view/achievement_view.dart';

class VisitEndSession extends StatefulWidget {
  final TabController objTabController;
  final CUser objLoginAcc;
  final CVisit objVisit;

  VisitEndSession({Key key,
    @required this.objTabController,
    @required this.objLoginAcc,
    @required this.objVisit}) : super(key: key);

  @override
  _VisitEndSession createState() => new _VisitEndSession();
}

class _VisitEndSession extends State<VisitEndSession> {
  final formEndSession  = GlobalKey<FormState>();
  //final dialogBox message           = dialogBox();

  bool _isLoading = false;

  FocusNode NodeCmdEndVisit   = FocusNode();
  FocusNode NodeCmdCancel     = FocusNode();
  FocusNode NodeTermination   = FocusNode();

  String _dropdownTerminationType = 'Normal Termination';
  List<String> TerminationListType = ['Normal Termination','Cancelled Termination'];

  Future<CVisit> endVisit(BuildContext context, CVisitEndReq objEndVisitReq) async{
    CVisit result;
    svcVisit visitService = new svcVisit();
    await visitService.setEndVisit(objEndVisitReq)
        .then((CVisit objResult) {
          // ended session success
          if(objResult.visitId > 0){
            Toast_Popup(context, Icon(Icons.done, color: Colors.white,), Colors.blue, "Visit Ended Successfully");
          }
          else{
            // session already inactive
            Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Visit Already Ended");
          }
          // delay for 3 second then popup
        Timer(Duration(seconds: 3), () {
          Navigator.of(context).pop();
        });

      result = objResult;
    }).catchError((e){
      print("endVisit exception -> " + e.toString());
    });
    return result;
  }

  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "", //strTitle,
      icon: pIcon, // Icon(Icons.done, color: Colors.white,)
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }


  Widget Avatar(BuildContext context){
    return Container(
      width: 200.0,
      height: 120.0,
      decoration: new BoxDecoration(
        color: Colors.transparent,
        border: new Border.all(color: Colors.white, width: 2.0),
        borderRadius: new BorderRadius.circular(10.0),
      ),
      child: new Center(
          child: Image.asset('assets/images/endsession.png')
      ),
    );
  }

  Widget Lbl_Title(BuildContext context, CUser objLoginAcc, CVisit objVisit) {
    return Row(
        children: [
          Expanded(flex: 1,
              child: Cmd_Back(context)
          ),
          Expanded(flex: 8,
              child: RichText(
                textAlign: TextAlign.center,
                text: new TextSpan(
                  style: new TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    new TextSpan(text: 'Visit Termination')
                  ],
                ),
              )
          ),
          Expanded(flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Column(),
                //(objVisit.endVisitDate == null ? Cmd_EndVisit(context, objLoginAcc, objVisit) : Column()),
              ],
            ),
          ),
        ]
    );
  }

  Widget Lbl_VisitDate(BuildContext context, CVisit objVisit){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        RichText(
          textAlign: TextAlign.center,
          text: new TextSpan(
            style: new TextStyle(
              fontSize: 12.0,
              color: Colors.black,
            ),
            children: <TextSpan>[
              new TextSpan(text: 'Visit Date : ' + objVisit.visitDate)
            ],
          ),
        )
      ],
    );
  }

  Widget _buildVisitInfo(BuildContext context, CVisit objVisit){
    return Column(
      children: [
        Column(
          children: [
            Row(
              children: [
                Expanded(flex: 3, child: Text("Tanggal" , style: TextStyle(fontSize: 12))),
                Expanded(flex: 7, child: Text(objVisit.visitDate == null ? '-' : objVisit.visitDate,style: TextStyle(fontSize: 12))),
              ],
            )
          ],
        ),
        Column(
          children: [
            Row(
              children: [
                Expanded(flex: 3, child: Text("Nama", style: TextStyle(fontSize: 12))),
                Expanded(flex: 7, child: Text(objVisit.objGuest.guestName == null ? '-' : objVisit.objGuest.guestName, style: TextStyle(fontSize: 12))),
              ],
            )
          ],
        ),
        Column(
          children: [
            Row(
              children: [
                Expanded(flex: 3, child: Text("No KTP", style: TextStyle(fontSize: 12))),
                Expanded(flex: 7, child: Text(objVisit.objGuest.guestCardId == null ? '-' : objVisit.objGuest.guestCardId, style: TextStyle(fontSize: 12))),
              ],
            )
          ],
        ),
        Column(
          children: [
            Row(
              children: [
                Expanded(flex: 3, child: Text("No HP", style: TextStyle(fontSize: 12))),
                Expanded(flex: 7, child: Text(objVisit.objGuest.guestPhone == null ? '-' : objVisit.objGuest.guestPhone, style: TextStyle(fontSize: 12))),
              ],
            )
          ],
        ),
        Column(
          children: [
            Row(
              children: [
                Expanded(flex: 3, child: Text("Jam Datang", style: TextStyle(fontSize: 12))),
                Expanded(flex: 7, child: Text(objVisit.startVisitDate == null ? '-' : objVisit.startVisitDate, style: TextStyle(fontSize: 12))),
              ],
            )
          ],
        ),
        Column(
          children: [
            Row(
              children: [
                Expanded(flex: 3, child: Text("Jam Keluar", style: TextStyle(fontSize: 12))),
                Expanded(flex: 7, child: Text(objVisit.endVisitDate == null ? '-' : objVisit.endVisitDate, style: TextStyle(fontSize: 12))),
              ],
            )
          ],
        ),
      ],
    );
  }

  Widget Cbo_TerminationType(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          InputDecorator(
            decoration: InputDecoration(
              filled: false,
              icon: const Icon(Icons.adjust),
              labelText: 'Termination Type',
              //errorText:  _dropdownTerminationType == null ? 'Required': "",
            ),
            //isEmpty: _dropdownTerminationType == null,
            child: DropdownButton<String>(
                focusNode: NodeTermination,
                value: _dropdownTerminationType,
                isDense: true,
                items: TerminationListType.map((String value) {
                  return DropdownMenuItem<String>(
                    child: Text(value, style: TextStyle(fontSize: 12.0, color: Colors.black)),
                    value: value,
                  );
                }).toList(),
                onChanged: (String newValue) {
                  print('value change -> ' + newValue);
                  setState(() {
                    _dropdownTerminationType = newValue;
                  });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget Cmd_Back(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.blue),
        tooltip: 'Back',
        onPressed: (){
          Navigator.pop(context);
        },
      ),
    );
  }
  /*
  Widget Cmd_EndVisit(BuildContext context, CUser objLoginAcc, CVisit objVisit) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.label_off , color: Colors.blue),
        tooltip: 'Terminate Visit',
        onPressed: (){
          endVisit(context,
              CVisitEndReq(
                  endVisitId: objVisit.visitId,
                  endVisitreason: _dropdownTerminationType == 'Cancelled Termination' ? 'Cancelled' : null,
                  endVisitby: objLoginAcc.username
              ));
          //Navigator.pop(context);
        },
      ),
    );
  }
  */

  Widget Cmd_EndVisit(BuildContext context, CUser objLoginAcc, CVisit objVisit) {
    return InkWell(
        //focusNode: NodeCmdSave,
        onTap: () {
          if (!_isLoading) {
            // check trough validation first
            endVisit(context,
                CVisitEndReq(
                    endVisitId: objVisit.visitId,
                    endVisitreason: _dropdownTerminationType == 'Cancelled Termination' ? 'Cancelled' : null,
                    endVisitby: objLoginAcc.username
                ));
              // set loading to false
              setState(() { _isLoading = false; });
          } // end if
          else{
            // clear progress dialog
            Navigator.pop(context);
          }
        },
        child: new Container(
          width: 150.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('End Visit',
                    style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }

  Widget _buildFormEndSession(BuildContext context, CUser objLoginAcc, CVisit objVisit) {
    return Form(
        key: formEndSession,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Avatar(context),
            SizedBox(height: 10.0),
            Lbl_Title(context, objLoginAcc, objVisit),
            Divider(),
            _buildVisitInfo(context, objVisit),
            Divider(),
            Cbo_TerminationType(context),
            SizedBox(height: 10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                (objVisit.endVisitDate == null ? Cmd_EndVisit(context, objLoginAcc, objVisit) : Column()),
              ],
            ),
          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildFormEndSession(context, widget.objLoginAcc ,widget.objVisit);
  }
}


