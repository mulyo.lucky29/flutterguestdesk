import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/screen/visit/session/VisitSessionModal.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:myggguestdesk/widget/BadgeInfo.dart';
import 'package:myggguestdesk/service/svcVisit.dart';

class ExpiredPage extends StatefulWidget {
  final TabController objTabMenuController;
  final CUser objLoginAcc;

  ExpiredPage({Key key,
    @required this.objTabMenuController,
    @required this.objLoginAcc
  }) : super(key: key);

  @override
  _ExpiredPage createState() => new _ExpiredPage();
}

class _ExpiredPage extends State<ExpiredPage> {
  bool _isFiltered = false;
  List<CVisit> _searchResult = [];
  List<CVisit> _visitExpiredList = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  List<String> tableHeader = ['No KTP', 'Nama Visitor','No Telp Visitor','Company Visitor','Contact Name', 'Location', 'Id Type','Card No','Access Card No','Pair No'];
  List<TextEditingController> TxtController = [];
  List<FocusNode> TxtNode = [];


  Future<void> loadVisitExpiredList(CSite objSite) async{
    final svcVisit  visitService = svcVisit();
    // load service
    await visitService.getVisitExpired(objSite.siteCode).then((objResult) {
      if (objResult != null) {
        setState(() {
          _visitExpiredList = objResult;
        });
        doFilter();
      }
    }).catchError((e) {
      print("Error Load Visit Expired List -> " + e.toString());
    });
  }

  Widget separator(BuildContext context){
    return Column(
      children: [
        SizedBox(height: 8),
        Container(height: 8, color: Colors.grey[200]),
        SizedBox(height: 8),
      ],
    );
  }
  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Visit Expired List')
        ],
      ),
    );
  }

  Widget Cmd_Refresh(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.refresh, color: Colors.blue),
        tooltip: 'Refresh',
        onPressed: (){
          doRefresh();
        },
      ),
    );
  }
  Widget Cmd_ClearAllFilter(BuildContext context){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.tune),
        tooltip: 'Filter Clear',
        onPressed: (){
          doClearFilter();
        },
      ),
    );
  }
  Widget Cmd_EndVisit(BuildContext context, CUser objLoginAcc, CVisit objVisit){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.location_off,
          color: objVisit.endVisitDate == null ? Colors.red : Colors.grey,
        ),
        tooltip: 'End Visit',
        onPressed: (){
          objVisit.endVisitDate == null ?
          _showDialogGuestSession(context, objLoginAcc, objVisit, 5) : null;
          //_showDialogEndVisit(context, objVisit) : null;
        },
      ),
    );
  }

  Future<void> _showDialogGuestSession(BuildContext context, CUser objLoginAcc, CVisit objVisit, int tabDefault){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                    margin: const EdgeInsets.all(10.0),
                    //color: Colors.transparent,
                    width: 500.0,
                    height: double.infinity,
                    child: VisitSessionModal(
                        objLoginAcc: objLoginAcc,
                        objVisit: objVisit,
                        defaultTab: tabDefault)
                ),
              )
          );
        }).whenComplete(() {
      doRefresh();
    });
  }


  Widget _buildTitleContact(BuildContext context, CUser objLoginAcc){
    return  Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      //Lbl_Title(context),
                      BadgeInfo(objSite: objLoginAcc.objSite),
                    ]
                ),
              ],
            ),
            Row(
              children: [
                Cmd_Refresh(context)
              ],
            )
          ],
        )
      ],
    );
  }

  onSearchTextChanged(int index, String str) async {
    doFilter();
    setState(() {});
  }

  doFilter(){
    setState(() {
      _searchResult.clear();
      _searchResult = _visitExpiredList
          .where((CVisit) =>
      (TxtController[0].text.isEmpty ? true : CVisit.objGuest.guestCardId.toUpperCase().contains(TxtController[0].value.text.toUpperCase())) &&
          (TxtController[1].text.isEmpty ? true : (CVisit.objGuest.guestName != null ? CVisit.objGuest.guestName.toUpperCase() : '').contains(TxtController[1].value.text.toUpperCase())) &&
          (TxtController[2].text.isEmpty ? true : (CVisit.objGuest.guestPhone != null ? CVisit.objGuest.guestPhone.toUpperCase() : '').contains(TxtController[2].value.text.toUpperCase())) &&
          (TxtController[3].text.isEmpty ? true : (CVisit.objGuest.guestCompany != null ? CVisit.objGuest.guestCompany.toUpperCase() : '').contains(TxtController[3].value.text.toUpperCase())) &&
          (TxtController[4].text.isEmpty ? true : (CVisit.objContact.contactName != null ? CVisit.objContact.contactName.toUpperCase() :
          (CVisit.objContact.contactAltName != null ? CVisit.objContact.contactAltName.toUpperCase() : '')).contains(TxtController[4].value.text.toUpperCase())) &&
          (TxtController[5].text.isEmpty ? true : (CVisit.objContact.siteSpecific != null ? CVisit.objContact.siteSpecific.toUpperCase() : '').contains(TxtController[5].value.text.toUpperCase())) &&
          (TxtController[6].text.isEmpty ? true : (CVisit.assuranceCardType != null ? CVisit.assuranceCardType.toUpperCase() : '').contains(TxtController[6].value.text.toUpperCase())) &&
          (TxtController[7].text.isEmpty ? true : (CVisit.assuranceCardNo != null ? CVisit.assuranceCardNo.toUpperCase() : '').contains(TxtController[7].value.text.toUpperCase())) &&
          (TxtController[8].text.isEmpty ? true : (CVisit.accessCardTag != null ? CVisit.accessCardTag.toUpperCase() : '').contains(TxtController[8].value.text.toUpperCase())) &&
          (TxtController[9].text.isEmpty ? true : (CVisit.accessCardPairNo != null ? CVisit.accessCardPairNo.toUpperCase() : '').toUpperCase().contains(TxtController[9].value.text.toUpperCase()))

      ).toList();

    });
  }

  doRefresh(){
    loadVisitExpiredList(widget.objLoginAcc.objSite);
    setState(() {});
  }

  doSort(){
    setState(() {
      _visitExpiredList.sort((a, b) {
        return b.visitDate.compareTo(a.visitDate);
      });
    });
  }

  doClearFilter(){
    TxtController.forEach((txtController) {
      txtController.clear();
    });
    doFilter();
  }

  bool isFiltered(){
    int found = 0;
    TxtController.forEach((txtController) {
      if(txtController.text.isNotEmpty) {
        found = found +1;
      }
    });
    if(found > 0){
      return true;
    }
    else{
      return false;
    }
  }

  Widget _buildTxtSearch(BuildContext context, int index){
    return  Transform.translate(
      offset: Offset(-5, 0), //Offset(-16, 0),
      child: Transform.scale(
        scale: 0.9, //0.8,
        child: TextFormField(
          controller: TxtController[index],
          focusNode:  TxtNode[index],
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: tableHeader[index],
              hintText: tableHeader[index],
              isDense: true,
              contentPadding: EdgeInsets.all(5),
              suffixIcon: Transform.translate(
                  offset: Offset(0,0),
                  child:Transform.scale(
                    scale: 0.75,
                    child: IconButton(
                        icon: Icon(Icons.cancel),
                        tooltip: 'Clear',
                        onPressed: () {
                          TxtController[index].clear();
                          onSearchTextChanged(index, '');
                        }),
                  )
              )
          ),
          style: TextStyle(fontSize: 12.0, height: 2.0, color: Colors.black),
          onChanged: (string) {
            onSearchTextChanged(index, string);
          },
          onFieldSubmitted: (_) {
            if(index+1 == tableHeader.length) {
              FocusScope.of(context).requestFocus(TxtNode[0]);
            }
            else{
              FocusScope.of(context).requestFocus(TxtNode[index+1]);
            }
          },
        ),
      ),
    );
  }

  Widget _buildRowHeaderVisitExpired(BuildContext context){
    //List<String> tableHeader = ['Visit Date', 'No KTP', 'Nama Visitor','No Telp Visitor','Company Visitor','Contact Name', 'Assurance Card','Assurance No'];
    return  Padding(
        padding: EdgeInsets.only(top: 2.5),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: [
                  // group header
                  IntrinsicHeight(
                      child:Row(
                          children: <Widget>[
                            Expanded(flex: 1,
                              child:  Container(height: 20, color: Colors.grey[200], child:
                              Center(child: Text("No.", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                            ),
                            SizedBox(width: 5.0),
                            Expanded(flex: 14,
                              child:  Container(height: 20, color: Colors.grey[200], child:
                              Center(child:Text("Visitor Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                            ),
                            SizedBox(width: 5.0),
                            Expanded(flex: 7,
                              child:  Container(height: 20, color: Colors.grey[200], child:
                              Center(child: Text("Contact Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                            ),
                            SizedBox(width: 5.0),
                            Expanded(flex: 12,
                              child:  Container(height: 20, color: Colors.grey[200], child:
                              Center(child: Text("Doc Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                            ),
                            SizedBox(width: 5.0),
                            Expanded(flex: 7,
                              child:  Container(height: 20, color: Colors.grey[200], child:
                              Center(child: Text("Visit Info", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                            ),
                            SizedBox(width: 5.0),
                            Expanded(flex: 3,
                              child:  Container(height: 20, color: Colors.grey[200], child:
                              Center(child: Text("Action", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0)))),
                            ),
                          ]
                      )
                  ),
                ],
              ),
              // search
              IntrinsicHeight(
                child: Row(
                    children: <Widget>[
                      Expanded(flex: 1, child:
                      Container(height: 50, color: Colors.grey[200], child: Cmd_ClearAllFilter(context))),    // 1 : id
                      SizedBox(width: 5.0),

                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,0))),  // 2 : no ktp
                      Expanded(flex: 4, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,1))),  // 3 : nama visitor
                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,2))),  // 4 : no hp visitor
                      Expanded(flex: 4, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,3))),  // 5 : company visitor
                      SizedBox(width: 5.0),

                      Expanded(flex: 4, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,4))),  // 6 : contact name
                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,5))),  // 7 : contact spec
                      SizedBox(width: 5.0),

                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,6))),  // id type
                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,7))),  // id no
                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,8))),  // access card no
                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child: _buildTxtSearch(context,9))),  // pair no
                      SizedBox(width: 5.0),

                      Expanded(flex: 2, child:
                      Container(height: 50, color: Colors.grey[200], child:
                      Center(child: Text('Start Visit', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0))))),
                      Expanded(flex: 2, child:
                      Container(height: 50, color: Colors.grey[200], child:
                      Center(child: Text('End Visit', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0))))),
                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child:
                      Center(child: Text('Ended Status', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0))))),
                      SizedBox(width: 5.0),

                      Expanded(flex: 3, child:
                      Container(height: 50, color: Colors.grey[200], child:
                      Center(child: Text("", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0,))))),
                    ]
                ),
              )
            ]
        )
    );
  }

  Widget _buildRowListVisitExpired(BuildContext context, CVisit objVisit, CUser objLoginAcc){
    return  Padding(
        padding: EdgeInsets.only(bottom: 0.5, top: 0.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(flex: 1, child:  Text(objVisit.visitId.toString(),
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  SizedBox(width: 5.0),
                  Expanded(flex: 3, child: Text(objVisit.objGuest.guestCardId == null ? '-'  : objVisit.objGuest.guestCardId,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  Expanded(flex: 4, child: Text(objVisit.objGuest.guestName == null ? '-'  : objVisit.objGuest.guestName,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  Expanded(flex: 3, child: Text(objVisit.objGuest.guestPhone == null ? '-' : objVisit.objGuest.guestPhone,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  Expanded(flex: 4, child: Text(objVisit.objGuest.guestCompany == null ? '-' : objVisit.objGuest.guestCompany,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),
                  Expanded(flex: 4, child: Text(objVisit.objContact.contactName == null ?
                  (objVisit.objContact.contactAltName == null ? '-' : objVisit.objContact.contactAltName) :
                  objVisit.objContact.contactName,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.objContact.siteSpecific == null ? '-' : objVisit.objContact.siteSpecific,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),

                  Expanded(flex: 3, child: Text(objVisit.assuranceCardType == null ? '-' : objVisit.assuranceCardType,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.assuranceCardNo == null ? '-' : objVisit.assuranceCardNo,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.accessCardTag == null ? '-' : objVisit.accessCardTag,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.accessCardPairNo == null ? '-' : objVisit.accessCardPairNo,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),

                  Expanded(flex: 2, child: Text(objVisit.startVisitDate == null ? '-'  : objVisit.startVisitDate,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 2, child: Text(objVisit.endVisitDate == null ? '-' : objVisit.endVisitDate,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objVisit.endVisitReason == null ? '-' : objVisit.endVisitReason,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  SizedBox(width: 5.0),

                  Expanded(flex: 3, child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Cmd_EndVisit(context, objLoginAcc, objVisit), // objLoginAcc
                    ],
                  ))
                ]
            ),
            Divider(),
          ],
        )
    );
  }

  Widget _buildListViewVisitExpired(BuildContext context, CUser objLoginAcc){
    return Expanded(
      child: _searchResult.length != 0 || isFiltered() ?
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, i) {
          return _buildRowListVisitExpired(context, _searchResult[i], objLoginAcc);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _visitExpiredList.length,
        itemBuilder: (context, index) {
          return _buildRowListVisitExpired(context, _visitExpiredList[index], objLoginAcc);
        },
      ),
    );
  }

  Widget buildList(BuildContext context, CUser objLoginAcc) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTitleContact(context, objLoginAcc),
        separator(context),
        _buildRowHeaderVisitExpired(context),
        separator(context),
        _buildListViewVisitExpired(context, objLoginAcc)
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    // init txtController
    TxtController = List.generate(tableHeader.length, (i) => TextEditingController());
    // init focusNode
    TxtNode = List.generate(tableHeader.length, (i) => FocusNode());
    loadVisitExpiredList(widget.objLoginAcc.objSite);
  }

  @override
  void dispose() {
    // dispose txtController
    TxtController.forEach((c) => c.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          height: double.infinity,
          child: buildList(context, widget.objLoginAcc),
        ),
      ),
    );
  }
}