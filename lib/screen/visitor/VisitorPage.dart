import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/screen/visitor/VisitorFormPage.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/widget/BadgeInfo.dart';
import 'package:myggguestdesk/service/svcGuest.dart';


class VisitorPage extends StatefulWidget {
  final TabController objTabMenuController;
  final CUser objLoginAcc;

  VisitorPage({Key key,
    @required this.objTabMenuController,
    @required this.objLoginAcc
  }) : super(key: key);


  @override
  _VisitorPageState createState() => new _VisitorPageState();
}

class _VisitorPageState extends State<VisitorPage> {
  bool _isFiltered = false;
  List<CGuest> _searchResult = [];
  List<CGuest> _guestList = [];

  var refreshKey = GlobalKey<RefreshIndicatorState>();
  List<String> tableHeader = ['No KTP', 'Nama', 'Company', 'Phone','Email','IsActive'];
  List<TextEditingController> TxtController = [];
  List<FocusNode> TxtNode = [];


  Future<void> loadAllGuest(CUser objLoginAcc) async{
    final svcGuest  guestService = svcGuest();
    // load service log
    await guestService.getListAllGuest().then((objResult) {
      if (objResult != null) {
        setState(() {
          _guestList = objResult;
        });
        doFilter();
      }
    }).catchError((e) {
      print("Error Load Contact List -> " + e.toString());
    });
  }

  Widget separator(BuildContext context){
    return Column(
      children: [
        SizedBox(height: 8),
        Container(height: 8, color: Colors.grey[200]),
        SizedBox(height: 8),
      ],
    );
  }
  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Site List')
        ],
      ),
    );
  }

  Widget Cmd_Refresh(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.refresh, color: Colors.blue),
        tooltip: 'Refresh',
        onPressed: (){
          doRefresh();
        },
      ),
    );
  }
  Widget Cmd_ClearAllFilter(BuildContext context){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.tune),
        tooltip: 'Filter Clear',
        onPressed: (){
          doClearFilter();
        },
      ),
    );
  }
  Widget Cmd_Edit(BuildContext context, CGuest objGuest, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.edit),
        tooltip: 'Edit',
        onPressed: (){
          _buildMessageDialog(context, objGuest, objLoginAcc).then((void value) => doRefresh());
        },
      ),
    );
  }
  Widget Cmd_Delete(BuildContext context, CSite objSite, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.delete),
        tooltip: 'Delete',
        onPressed: (){
          //deleteSite(objContact);
        },
      ),
    );
  }

  Future<void> _buildMessageDialog(BuildContext context, CGuest objGuest, CUser objLoginAcc){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  //color: Colors.transparent,
                  width: 500.0,
                  height: double.infinity,
                  child: VisitorFormPage(
                      objGuest: objGuest,
                      objLoginAcc: objLoginAcc),
                ),
              )
          );
        });
  }

  Widget _buildTitleSite(BuildContext context, CUser objLoginAcc){
    return  Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      //Lbl_Title(context),
                      BadgeInfo(objSite: objLoginAcc.objSite),
                    ]
                ),
              ],
            ),
            Row(
              children: [
                Cmd_Refresh(context)
              ],
            )
          ],
        )
      ],
    );
  }

  onSearchTextChanged(int index, String str) async {
    doFilter();
    setState(() {});
  }

  doFilter(){
    setState(() {
      _searchResult.clear();
      _searchResult = _guestList
          .where((CGuest) =>
              //CGuest.guestId != null
              (TxtController[0].text.isEmpty ? true : CGuest.guestCardId.toUpperCase().contains(TxtController[0].value.text.toUpperCase())) &&
              (TxtController[1].text.isEmpty ? true : CGuest.guestName.toUpperCase().contains(TxtController[1].value.text.toUpperCase())) &&
              (TxtController[2].text.isEmpty ? true : CGuest.guestCompany.toUpperCase().contains(TxtController[2].value.text.toUpperCase())) &&
              (TxtController[3].text.isEmpty ? true : CGuest.guestPhone.toUpperCase().contains(TxtController[3].value.text.toUpperCase())) &&
              (TxtController[4].text.isEmpty ? true : CGuest.guestEmail.toUpperCase().contains(TxtController[4].value.text.toUpperCase())) &&
              (TxtController[5].text.isEmpty ? true : CGuest.isActive.toUpperCase().contains(TxtController[5].value.text.toUpperCase()))
      ).toList();
    });
  }

  doRefresh(){
    loadAllGuest(widget.objLoginAcc);
    setState(() {});
  }

  doSort(){
    setState(() {
      _guestList.sort((a, b) {
        return b.guestName.compareTo(a.guestName);
      });
    });
  }

  doClearFilter(){
    TxtController.forEach((txtController) {
      txtController.clear();
    });
    doFilter();
  }

  bool isFiltered(){
    int found = 0;
    TxtController.forEach((txtController) {
      if(txtController.text.isNotEmpty) {
        found = found +1;
      }
    });
    if(found > 0){
      return true;
    }
    else{
      return false;
    }
  }

  Widget _buildTxtSearch(BuildContext context, int index){
    return  Transform.translate(
      offset: Offset(-5, 0), //Offset(-16, 0),
      child: Transform.scale(
        scale: 0.9, //0.8,
        child:  TextFormField(
          controller: TxtController[index],
          focusNode:  TxtNode[index],
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: tableHeader[index],
              hintText: tableHeader[index],
              isDense: true,
              contentPadding: EdgeInsets.all(5),
              suffixIcon: Transform.translate(
                  offset: Offset(0,0),
                  child:Transform.scale(
                    scale: 0.75,
                    child: IconButton(icon: Icon(Icons.cancel),
                        tooltip: 'Clear',
                        onPressed: () {
                          TxtController[index].clear();
                          onSearchTextChanged(index, '');
                        }),
                  )
              )
          ),
          style: TextStyle(fontSize: 12.0, height: 2.0, color: Colors.black),
          onChanged: (string) {
            onSearchTextChanged(index, string);
          },
          onFieldSubmitted: (_) {
            if(index+1 == tableHeader.length) {
              FocusScope.of(context).requestFocus(TxtNode[0]);
            }
            else{
              FocusScope.of(context).requestFocus(TxtNode[index+1]);
            }
          },
        ),
      ),
    );
  }

  Widget _buildRowHeaderContact(BuildContext context){
    return  Padding(
        padding: EdgeInsets.only(top: 2.5),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(flex: 1, child: Cmd_ClearAllFilter(context)),
                    Expanded(flex: 2, child: _buildTxtSearch(context,0)),
                    Expanded(flex: 4, child: _buildTxtSearch(context,1)),
                    Expanded(flex: 4, child: _buildTxtSearch(context,2)),
                    Expanded(flex: 2, child: _buildTxtSearch(context,3)),
                    Expanded(flex: 4, child: _buildTxtSearch(context,4)),
                    Expanded(flex: 2, child: _buildTxtSearch(context,5)),
                    Expanded(flex: 3, child: Text("Action", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0,))),
                  ]
              ),
            ]
        )
    );
  }


  Widget _buildRowListContact(BuildContext context, CGuest objGuest, CUser objLoginAcc){

    return  Padding(
        padding: EdgeInsets.only(bottom: 0.5, top: 0.5),
        child:
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(flex: 1, child: Text(objGuest.guestId.toString(),
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 2, child: Text(objGuest.guestCardId == null ? '-'  : objGuest.guestCardId,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 4, child: Text(objGuest.guestName == null ? '-' : objGuest.guestName,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 4, child: Text(objGuest.guestCompany == null ? '-' : objGuest.guestCompany,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0))),
                  Expanded(flex: 2, child: Text(objGuest.guestPhone == null ? '-' : objGuest.guestPhone,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0))),
                  Expanded(flex: 4, child: Text(objGuest.guestEmail == null ? '-' : objGuest.guestEmail,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0))),
                  Expanded(flex: 2, child: Text(objGuest.isActive == null ? '-' : objGuest.isActive,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0))),
                  Expanded(flex: 3, child: Row(
                    children: [
                      Cmd_Edit(context, objGuest, objLoginAcc),
                      //SizedBox(width: 15.0),
                      //Cmd_Delete(context, objSite, objLoginAcc)
                    ],
                  ))
                ]
            ),
            Divider(),
          ],
        )
    );
  }

  Widget _buildListViewGuest(BuildContext context, CUser objLoginAcc){
    return Expanded(
      //child: _searchResult.length != 0 || controller.text.isNotEmpty ?
      child: _searchResult.length != 0 || isFiltered() ?
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, i) {
          return _buildRowListContact(context, _searchResult[i], objLoginAcc);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _guestList.length,
        itemBuilder: (context, index) {
          return _buildRowListContact(context, _guestList[index], objLoginAcc);
        },
      ),
    );
  }

  Widget buildList(BuildContext context, CUser objLoginAcc) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTitleSite(context, objLoginAcc),
        separator(context),
        _buildRowHeaderContact(context),
        separator(context),
        _buildListViewGuest(context, objLoginAcc)
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    // init txtController
    TxtController = List.generate(tableHeader.length, (i) => TextEditingController());
    // init focusNode
    TxtNode = List.generate(tableHeader.length, (i) => FocusNode());
    loadAllGuest(widget.objLoginAcc);

  }

  @override
  void dispose() {
    // dispose txtController
    TxtController.forEach((c) => c.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          height: double.infinity,
          child: buildList(context, widget.objLoginAcc),
        ),
      ),
    );
    //);
  }
}