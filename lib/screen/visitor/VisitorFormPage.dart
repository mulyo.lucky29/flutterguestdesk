import 'dart:ui';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/rendering.dart';
import 'package:achievement_view/achievement_view.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CGuest.dart';
import 'package:myggguestdesk/validation/valRegistration.dart';
import 'package:myggguestdesk/service/svcGuest.dart';
import 'package:myggguestdesk/tools/dialogBox.dart';

class VisitorFormPage extends StatefulWidget {
  final CGuest objGuest;
  final CUser objLoginAcc;

  // constructor
  VisitorFormPage({Key key,
    @required this.objGuest,
    @required this.objLoginAcc}) : super(key: key);

  @override
  _VisitorFormPage createState() => new _VisitorFormPage();
}

class _VisitorFormPage extends State<VisitorFormPage> with valRegistration {
  final scaffoldStateKey = GlobalKey<ScaffoldState>();
  final formGuestData  = GlobalKey<FormState>(); // globalkey for validation
  final dialogBox message           = dialogBox();
  bool _isEdit = true;
  bool _isLoading = false;

  FocusNode NodeGuestId     = FocusNode();
  FocusNode NodeNama        = FocusNode();
  FocusNode NodeNip         = FocusNode();
  FocusNode NodePhoneno     = FocusNode();
  FocusNode NodeEmail       = FocusNode();
  FocusNode NodeCompany     = FocusNode();
  FocusNode NodeCmdSave     = FocusNode();
  FocusNode NodeCmdCancel   = FocusNode();

  TextEditingController _ctrlGuestId;
  TextEditingController _ctrlName;
  TextEditingController _ctrlNip;
  TextEditingController _ctrlPhoneno;
  TextEditingController _ctrlEmail;
  TextEditingController _ctrlCompany;

  String strGuestId = '';
  String strName    = '';
  String strNip     = '';
  String strPhoneno = '';
  String strEmail   = '';
  String strCompany = '';

  Future<CGuest> saveGuestInfo(BuildContext context, CGuest objGuest) async{
    CGuest result;
    svcGuest guestService = new svcGuest();
    await guestService.setRegisterGuest(objGuest)
        .then((CGuest objResult) {
      // save success
      if(objResult.guestId > 0){
        Toast_Popup(context, Icon(Icons.done, color: Colors.white,), Colors.blue, "Data Saved");
      }
      // delay for 3 second then popup
      Timer(Duration(seconds: 3), () {
        Navigator.of(context).pop();
      });

      result = objResult;
    }).catchError((e){
      print("saveGuestInfo exception -> " + e.toString());
    });
    return result;
  }

  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "", //strTitle,
      icon: pIcon, // Icon(Icons.done, color: Colors.white,)
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }

  Widget Txt_GuestId(BuildContext context) {
    return Visibility(
        visible: false,
        child: TextFormField(
          enabled: false,
          readOnly: true,
          controller: _ctrlGuestId,
          focusNode: NodeGuestId,
          keyboardType: TextInputType.number,
          //validator: validateName,
          onSaved: (String value) {
            strGuestId = value;
          },
          style: TextStyle(fontSize: 12.0, color: Colors.black),
          autofocus: false,
          decoration: InputDecoration(
            icon: const Icon(Icons.format_list_numbered),
            labelText: 'Id',
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          ),
          onFieldSubmitted: (_) {
            FocusScope.of(context).requestFocus(NodeNama);
          },
        )
    );
  }
  Widget Txt_Nama(BuildContext context) {
    return TextFormField(
      enabled: _isEdit,
      readOnly: !_isEdit,
      controller: _ctrlName,
      focusNode: NodeNama,
      keyboardType: TextInputType.text,
      validator: validateName,
      onSaved: (String value) {
        strName = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.person),
        labelText: 'Full Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeNip);
      },
    );
  }
  Widget Txt_Nip(BuildContext context) {
    return TextFormField(
      enabled: _isEdit,
      readOnly: !_isEdit,
      controller: _ctrlNip,
      focusNode: NodeNip,
      validator: validateNip,
      onSaved: (String value) {
        strNip = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.card_membership),
        labelText: 'No KTP',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePhoneno);
      },
    );
  }
  Widget Txt_PhoneNo(BuildContext context) {
    return TextFormField(
      enabled: _isEdit,
      readOnly: !_isEdit,
      controller: _ctrlPhoneno,
      focusNode: NodePhoneno,
      validator: validatePhoneNo,
      onSaved: (String value) {
        strPhoneno = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly
      ],
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.phone),
        labelText: 'Phone Number',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeEmail);
      },
    );
  }
  Widget Txt_Email(BuildContext context) {
    return TextFormField(
      enabled: _isEdit,
      readOnly: !_isEdit,
      controller: _ctrlEmail,
      focusNode: NodeEmail,
      validator: validateEmail,
      onSaved: (String value) {
        strEmail = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.email),
        labelText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCompany);
      },
    );
  }
  Widget Txt_Company(BuildContext context) {
    return TextFormField(
      enabled: _isEdit,
      readOnly: !_isEdit,
      controller: _ctrlCompany,
      focusNode: NodeCompany,
      validator: validateCompany,
      onSaved: (String value) {
        strCompany = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.business),
        labelText: 'Company Name',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSave);
      },
    );
  }

  Widget Cmd_Save(BuildContext context, CUser objLoginAcc) {
    print("save");
    return InkWell(
        focusNode: NodeCmdSave,
        onTap: () {
          if (!_isLoading) {
            // check trough validation first
            if (formGuestData.currentState.validate()) {
              formGuestData.currentState.save();
              // init loading to true
              setState(() { _isLoading = true; });
              try {
                // save guest info data
                /*
                print("guestId      ->" + strGuestId);
                print("guestCardId  ->" + strNip);
                print("guestName    ->" + strName);
                print("guestPhone   ->" + strPhoneno);
                print("guestEmail   ->" + strEmail);
                print("guestCompany ->" + strCompany);
                */

                if(strGuestId != ''){
                  saveGuestInfo(
                      context,
                      CGuest(
                          guestId: int.parse(strGuestId),
                          guestCardId: strNip,
                          guestName: strName,
                          guestPhone: strPhoneno,
                          guestEmail: strEmail,
                          guestCompany: strCompany,
                          isActive: 'Y'
                      )
                  );
                }

              }
              catch(e){
                print(e);
              }
            } // end if
            setState(() { _isLoading = false; });
          } // end if
          else{
            // clear progress dialog
            Navigator.pop(context);
          }
        },
        child: new Container(
          width: 150.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Save',
                    style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        focusNode: NodeCmdCancel,
        onTap: () {
          //Navigator.of(context).pop();
          Navigator.pop(context);
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Back(BuildContext context, CUser objVisit) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.blue),
        tooltip: 'Back',
        onPressed: (){
          Navigator.pop(context);
        },
      ),
    );
  }

  Widget Lbl_Title(BuildContext context, CUser objLoginAcc) {
    return Row(
        children: [
          Expanded(flex: 1,
              child: Cmd_Back(context, objLoginAcc)
          ),
          Expanded(flex: 6,
              child: RichText(
                textAlign: TextAlign.center,
                text: new TextSpan(
                  style: new TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    new TextSpan(text: 'Visitor Info')
                  ],
                ),
              )
          ),
          Expanded(flex: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
              ],
            ),
          ),
        ]
    );
  }

  Widget _buildFormInput(BuildContext context, CUser objLoginAcc) {
    return Form(
        key: formGuestData,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Txt_GuestId(context),
            SizedBox(height: 5.0),
            Txt_Nama(context),
            SizedBox(height: 5.0),
            Txt_Nip(context),
            SizedBox(height: 5.0),
            Txt_PhoneNo(context),
            SizedBox(height: 5.0),
            Txt_Email(context),
            SizedBox(height: 5.0),
            Txt_Company(context),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Cmd_Cancel(context),
                Cmd_Save(context, objLoginAcc),
              ],
            ),
          ],
        )
    );
  }

  @override
  void initState() {
    super.initState();

    //  init state
    setState(() {
      if(widget.objGuest == null){
        _ctrlGuestId    = TextEditingController();
        _ctrlName       = TextEditingController();
        _ctrlNip        = TextEditingController();
        _ctrlPhoneno    = TextEditingController();
        _ctrlEmail      = TextEditingController();
        _ctrlCompany    = TextEditingController();
      }
      else{
        _ctrlGuestId    = TextEditingController(text: widget.objGuest.guestId.toString());
        _ctrlName       = TextEditingController(text: widget.objGuest.guestName);
        _ctrlNip        = TextEditingController(text: widget.objGuest.guestCardId);
        _ctrlPhoneno    = TextEditingController(text: widget.objGuest.guestPhone);
        _ctrlEmail      = TextEditingController(text: widget.objGuest.guestEmail);
        _ctrlCompany    = TextEditingController(text: widget.objGuest.guestCompany);
      }
    });
  }

  @override
  void dispose() {
    _ctrlGuestId.dispose();
    _ctrlName.dispose();
    _ctrlNip.dispose();
    _ctrlPhoneno.dispose();
    _ctrlEmail.dispose();
    _ctrlCompany.dispose();
    super.dispose();
  }


  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(0.0),
      color: Colors.transparent,
      width: 500.0,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 5.0, right: 5.0),
        children: <Widget>[
          Lbl_Title(context, widget.objLoginAcc),
          Divider(),
          _buildFormInput(context, widget.objLoginAcc)
        ],
      ),
    );
  }

}
