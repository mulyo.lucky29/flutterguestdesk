import 'dart:ui';
import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/rendering.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/validation/valContactForm.dart';
import 'package:myggguestdesk/widget/HorizontalCalendar.dart';
import 'package:myggguestdesk/tools/generatePSK.dart';
import 'package:qr_flutter/qr_flutter.dart';


class KeyFormPage extends StatefulWidget {
  final CSite objSite;
  final CUser objLoginAcc;

  // constructor
  KeyFormPage({Key key,
    @required this.objSite,
    @required this.objLoginAcc
  }) : super(key: key);

  @override
  _KeyFormPage createState() => new _KeyFormPage();
}

class _KeyFormPage extends State<KeyFormPage> with valContactForm {
  final key = GlobalKey();
  //final _formSiteKey  = GlobalKey<FormState>(); // globalkey for validation
  final generatePSK generatorKey = generatePSK();

  FocusNode NodeCmdCancel        = FocusNode();
  String strKey          = '';

  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          //strOperation == 'Edit' ? TextSpan(text: 'Edit Site') :  TextSpan(text: 'Site')
          TextSpan(text: 'Key Generator')
        ],
      ),
    );
  }

  Widget Cmd_Close(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.green, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Close', style: TextStyle(fontSize: 14.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }

  Widget Cmd_CopyToClipboard(BuildContext context, String strData) {
    return
      Transform.scale(
        scale: 1,
        child: IconButton(
          icon: Icon(Icons.copy, color: Colors.grey),
          tooltip: 'Copy to clipboard',
          onPressed: (){
            Clipboard.setData(ClipboardData(text: strData));
          },
        ),
      );
  }

  Widget Calendar(BuildContext context, CSite objSite){
    return HorizontalCalendar(
      width: 60,
      height: 40,
      //initialDate: DateTime.now(),
      date: DateTime.now(),
      textColor: Colors.black45,
      backgroundColor: Colors.white,
      selectedColor: Colors.blue,
      onDateSelected: (date) {
        try{
          setState(() {
            DateTime tgl = DateTime.parse(date);
            strKey = generatorKey.generate(tgl,objSite);
            //print('strKey -> ' + strKey);
          });
        }
        catch(e){
          print('Exception strKey -> ' + e.toString());
        }
        print(date.toString());
      },
    );
  }

  Widget _buildFormGenerateKey(BuildContext context, CSite objSite, CUser objLoginAcc){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          children: [
            Lbl_Title(context),
            Divider(),
            SizedBox(height: 15.0),
            Text('Pilih Tanggal di kalender untuk generate key'),
            SizedBox(height: 15.0),
            Calendar(context, objSite),
            SizedBox(height: 15.0),
            Text('Authentication Key',
            style: TextStyle(
              fontSize: 14,
              color: Colors.black,
              fontWeight: FontWeight.normal,
              fontFamily: 'HelveticaNeue',
            )),
            SizedBox(height: 15.0),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 10.0),
                Expanded(flex: 3,
                child:  strKey == null ?
                        Column() :
                        RepaintBoundary(
                            key: key,
                            child: Container(
                              color: Colors.white,
                              child: QrImage(
                                data: strKey,
                                version: QrVersions.auto,
                                size: 98.0,
                                gapless: false,
                              ),
                            )
                        ),
                ),
                strKey == null ?
                Column() :
                Expanded(flex: 7,
                    child: Text(strKey,
                        style: TextStyle(
                          fontSize: 36,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'HelveticaNeue',
                        )
                    )
                ),
                strKey == null ?
                Column() :
                Cmd_CopyToClipboard(context, strKey)
              ],
            ),
            SizedBox(height: 10.0),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Cmd_Close(context),
              ],
            ),
            SizedBox(height: 5.0),
          ],
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      strKey = generatorKey.generate(DateTime.now(),widget.objSite);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('build');
    return _buildFormGenerateKey(context, widget.objSite, widget.objLoginAcc);
  }
}
