import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CUserSaveReq.dart';
import 'package:myggguestdesk/model/CUserResetReq.dart';
import 'package:myggguestdesk/service/svcUser.dart';
import 'package:myggguestdesk/service/svcSite.dart';
import 'package:myggguestdesk/validation/valUserProfile.dart';

class UserFormPage extends StatefulWidget {
  final CUser objUser;
  final CUser objLoginAcc;

  UserFormPage({Key key,
    @required this.objUser,
    @required this.objLoginAcc,
  }) : super(key: key);

  @override
  _UserFormPage createState() => _UserFormPage();
}

class _UserFormPage extends State<UserFormPage> with valUserProfile{
  final formUserInfoKey = GlobalKey<FormState>();
  Future<List<CSite>> _futureSite;
  CSite _dropdownValueSite;

  CSite _dropdownNullValueSite = new CSite(
      siteId: -1,
      siteName: "Choose Site",
      siteCode: "Choose Site"
  );


  TextEditingController _ctrlUsername   = new TextEditingController();
  TextEditingController _ctrlPassword   = new TextEditingController();
  TextEditingController _ctrlPhone      = new TextEditingController();
  TextEditingController _ctrlEmail      = new TextEditingController();
  TextEditingController _ctrlSiteAssign = new TextEditingController();

  FocusNode NodeCmdSave     = FocusNode();
  FocusNode NodeUsername    = FocusNode();
  FocusNode NodePassword    = FocusNode();
  FocusNode NodePhone       = FocusNode();
  FocusNode NodeEmail       = FocusNode();
  FocusNode NodeSiteAssign  = FocusNode();

  String strUsername;
  String strPassword;
  String strPhoneNo;
  String strEmail;
  int intSiteAssign;
  bool isAdmin  = false;
  bool isActive = false;
  bool checkChangePassword = false;
  bool _passwordVisible;

  Future<List<CSite>> loadSiteList(CUser objLoginAcc) async{
    List<CSite> result = [];
    print(objLoginAcc.username);
    svcSite siteService = new svcSite();
    await siteService.getSiteList()
        .then((List<CSite> objListSite) {
      if(objLoginAcc.username == 'admin'){
        print('here');
        result = objListSite;
      }
      else{
        result =  objListSite.where((CSite) => CSite.siteCode == objLoginAcc.objSite.siteCode).toList();
      }
    }).catchError((e){
      print('loadSiteList Err -> ' + e);
    });
    return result;
  }
  Future<void> saveUser(CUserSaveReq objLoginSave, CUser objUser, CUser objLoginAcc) async{
    final svcUser userService = svcUser();
    // save data user using service
    userService.saveUser(objLoginSave).then((objUserAcc) {
      if (objUserAcc != null) {
          if(objUser == null || checkChangePassword == true) {
            changePasswordUser(
                CUserResetReq(
                  dskusername : objUserAcc.username,
                  dskpassword : strPassword,
                  dskCreatedBy : objLoginAcc.username,
                )
            );
          }
          Navigator.of(context).pop();
      }
    }).catchError((e) {
      print("Error Save User -> " + e.toString());
    });
  }

  Future<void> changePasswordUser(CUserResetReq objLoginReset) async{
    final svcUser userService = svcUser();
    userService.changePassword(objLoginReset).then((objUserAcc) {
      if (objUserAcc != null) {
      }
    }).catchError((e) {
      print("Error Change Password User -> " + e.toString());
    });
  }

  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          TextSpan(text: 'User')
        ],
      ),
    );
  }
  Widget Txt_Username(BuildContext context) {
    return TextFormField(
      readOnly: widget.objUser == null ? false : true,
      controller: _ctrlUsername,
      focusNode: NodeUsername,
      keyboardType: TextInputType.text,
      validator: validateUsername,
      onSaved: (String value) {
        strUsername = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.verified_user),
        labelText: 'Username',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePhone);
      },
    );
  }
  Widget Txt_Password(BuildContext context, CUser objUser, CUser objLoginAcc) {
    return (objUser == null || checkChangePassword == true) ?
            TextFormField(
                controller: _ctrlPassword,
                focusNode: NodePassword,
                keyboardType: TextInputType.text,
                obscureText: !_passwordVisible,
                validator: (objUser == null || checkChangePassword == true) ? validatePassword : null,
                onSaved: (String value) {
                  strPassword = value;
                },
                style: TextStyle(fontSize: 12.0, color: Colors.black),
                autofocus: false,
                decoration: InputDecoration(
                  icon: const Icon(Icons.password),
                  labelText: objUser == null ? 'Enter Password' : 'Enter New Password',
                  suffixIcon: IconButton(
                    icon: Icon(
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Theme.of(context).primaryColorDark,
                    ),
                    onPressed: () {
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    },
                  ),
                ),
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(NodePhone);
                },
            )
            :
            Column();
  }
  Widget Txt_Phone(BuildContext context) {
    return TextFormField(
      controller: _ctrlPhone,
      focusNode: NodePhone,
      keyboardType: TextInputType.text,
      validator: validatePhoneNo,
      onSaved: (String value) {
        strPhoneNo = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.phone),
        labelText: 'Phone',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeEmail);
      },
    );
  }
  Widget Txt_Email(BuildContext context) {
    return TextFormField(
      controller: _ctrlEmail,
      focusNode: NodeEmail,
      keyboardType: TextInputType.text,
      validator: validateEmail,
      onSaved: (String value) {
        strEmail = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.email),
        labelText: 'Email',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeSiteAssign);
      },
    );
  }

  /*
  @override
  void didUpdateWidget(UserFormPage oldWidget) {
    if (this.currentItem != widget.currentItem) {
      setState(() {
        this.currentItem = widget.currentItem;
      });
    }
    super.didUpdateWidget(oldWidget);
  }
*/

  Widget Cbo_SiteAssign(BuildContext context, CUser objUser) {
    return FutureBuilder(
        future: _futureSite,
        builder: (BuildContext context, AsyncSnapshot<List<CSite>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          else if(snapshot.connectionState == ConnectionState.done){
            print('snapshot: $snapshot');
            if(snapshot.hasData){
              return DropdownButtonHideUnderline(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    InputDecorator(
                      decoration: InputDecoration(
                        filled: false,
                        icon: const Icon(Icons.my_location),
                        labelText: 'Site Assign',
                      ),
                      //isEmpty: _dropdownNullValueSite, //_dropdownValueSite == null,
                      child: DropdownButton<CSite>(
                        focusNode: NodeSiteAssign,
                        value: _dropdownValueSite, //_dropdownValueSite == null ? _dropdownNullValueSite : _dropdownValueSite,
                        isDense: true,
                        items: snapshot.data.map((CSite value) {
                          return DropdownMenuItem<CSite>(
                            child: Text(value.siteCode, style: TextStyle(fontSize: 12.0, color: Colors.black)),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (CSite newValue) {
                          print('value change -> ' + newValue.siteCode);
                          setState(() {
                            _dropdownValueSite = newValue;
                          });
                          FocusScope.of(context).requestFocus(NodeCmdSave);
                        },
                      ),
                    ),
                  ],
                ),
              );
            }
            else{
              print('No Data');
              return DropdownButton(
                  hint: Text("Site",
                      style: TextStyle(fontSize: 12.0, color: Colors.grey))
              );
            }
          }
          else{
            print('E');
            return Center(child: CircularProgressIndicator() );
          }
        }
    );
  }
  Widget Chk_IsAdmin(BuildContext context){
    return Padding(
      padding: EdgeInsets.only(left: 20.0),
      child: SwitchListTile(
          title: const Text('IsAdmin',
              style: TextStyle(fontSize: 12.0, color: Colors.grey)
          ),
          value: isAdmin,
          onChanged: (bool val) =>
              setState(() => isAdmin = val)
      )
    );
  }
  Widget Chk_IsAcative(BuildContext context){
    return Padding(
        padding: EdgeInsets.only(left: 20.0),
        child: SwitchListTile(
            title: const Text('IsActive',
                style: TextStyle(fontSize: 12.0, color: Colors.grey)
            ),
            value: isActive,
            onChanged: (bool val) =>
                setState(() => isActive = val)
        )
    );
  }
  Widget Chk_ChangePassword(BuildContext context, CUser objUser, CUser objLoginAcc){
    return objUser != null ?
      Column(
        children: [
        Padding(
            padding: EdgeInsets.only(top: 5.0, left: 45.0, bottom: 5.0),
            child: Row(
              children: [
                Checkbox(
                  value: checkChangePassword, //false, //this.value,
                  onChanged: (bool value) {
                    setState(() {
                        // clear new password
                        if(value == true){
                          _ctrlPassword.clear();
                        }
                        checkChangePassword = value;
                    });
                  },
                ),
                Text('Change Password',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12.0,
                  ),
                ),
              ],
            )
        )
      ],
    ) :
    Column();
    ;
  }

  Widget Cmd_Save(BuildContext context, CUser objUser, CUser objLoginAcc) {
    return InkWell(
        focusNode: NodeCmdSave,
        onTap: () {
          if (formUserInfoKey.currentState.validate()) {
              formUserInfoKey.currentState.save();

                //print("strUsername -> " + strUsername);
                //print("strPassword -> " + strPassword);
                //print("dsksiteId   -> " + _dropdownValueSite.siteId.toString());

                saveUser(
                    CUserSaveReq(
                        dskusername: strUsername,
                        dskPhoneNo: strPhoneNo,
                        dskEmail: strEmail,
                        dsksiteId: _dropdownValueSite.siteId,
                        dskisActive: isActive == true ? 'Y': 'N',
                        dskisAdmin: isAdmin == true ? 'Y' : 'N',
                        dskCreatedBy: objLoginAcc.username
                    ),
                    objUser,
                    objLoginAcc
                );
          }
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Save',
                    style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel', style: TextStyle(fontSize: 14.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget _buildFormEditUser(BuildContext context, CUser objUser, CUser objLoginAcc) {

    print('_buildFormEditUser ->' + objLoginAcc.isAdmin);
    return Form(
        key: formUserInfoKey,
        child: Column(
          children: <Widget>[
            Lbl_Title(context),
            Divider(),
            Txt_Username(context),
            Chk_ChangePassword(context, objUser, objLoginAcc),
            Txt_Password(context, objUser, objLoginAcc),
            Txt_Phone(context),
            Txt_Email(context),
            Cbo_SiteAssign(context, objUser),
            objLoginAcc.isAdmin == "Y" ? Chk_IsAcative(context) : Column(),
            objLoginAcc.isAdmin == "Y" ? Chk_IsAdmin(context)   : Column(),
            SizedBox(height: 10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Cmd_Cancel(context),
                Cmd_Save(context, objUser, objLoginAcc)
              ],
            ),
          ],
        )
    );
  }

  /*
  @override
  void didUpdateWidget(UserFormPage oldWidget) {
    if (this._dropdownValueSite != widget.objUser.objSite) {
      setState(() {
        this._dropdownValueSite = widget.objUser.objSite;
      });
    }
    super.didUpdateWidget(oldWidget);
  }
  */

  @override
  void initState() {
    setState(() {
      _passwordVisible = false;

      if(widget.objUser == null ){
        _ctrlUsername   = TextEditingController();
        _ctrlPhone      = TextEditingController();
        _ctrlEmail      = TextEditingController();
        _ctrlSiteAssign = TextEditingController();
        isActive = true;
        isAdmin  = false;
      }
      else{
        _ctrlUsername   = TextEditingController(text: widget.objUser.username);
        _ctrlPhone      = TextEditingController(text: widget.objUser.phonenumber);
        _ctrlEmail      = TextEditingController(text: widget.objUser.emailaddress);
        _ctrlSiteAssign = TextEditingController(text: widget.objUser.objSite.siteCode);
        isActive = widget.objUser.isActive == 'Y' ? true : false;
        isAdmin  = widget.objUser.isAdmin  == 'Y' ? true : false;
        // init value
        _dropdownValueSite = widget.objUser.objSite;
      }
      // load site list
      _futureSite = loadSiteList(widget.objLoginAcc);

    });
    super.initState();
  }

  @override
  void dispose() {
    _ctrlUsername.dispose();
    _ctrlPhone.dispose();
    _ctrlEmail.dispose();
    _ctrlSiteAssign.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: const EdgeInsets.all(0.0),
      color: Colors.transparent,
      width: 500.0,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 5.0, right: 5.0),
        children: <Widget>[
          _buildFormEditUser(context, widget.objUser, widget.objLoginAcc)
        ],
      ),
    );


  }
}
