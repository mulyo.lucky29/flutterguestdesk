import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/screen/user/UserFormPage.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/widget/BadgeInfo.dart';
import 'package:myggguestdesk/service/svcUser.dart';

class UserPage extends StatefulWidget {
  final TabController objTabMenuController;
  final CUser objLoginAcc;

  UserPage({Key key,
    @required this.objTabMenuController,
    @required this.objLoginAcc
  }) : super(key: key);

  @override
  _UserPage createState() => new _UserPage();
}

class _UserPage extends State<UserPage> {
  bool _isFiltered = false;
  List<CUser> _searchResult = [];
  List<CUser> _userList = [];

  var refreshKey = GlobalKey<RefreshIndicatorState>();
  List<String> tableHeader = ['username', 'phone', 'email','isActive','isAdmin','Site Code'];
  List<TextEditingController> TxtController = [];
  List<FocusNode> TxtNode = [];

  Future<void> loadUser(CUser objLoginAcc) async{
    print("loadUser");
    final svcUser  userService = svcUser();

    await userService.getAllUserList().then((objResult) {
      if (objResult != null) {
        setState(() {
          _userList = objLoginAcc.username == 'admin' ? objResult :
                      objResult.where((CUser) => CUser.username != 'admin' && CUser.objSite.siteCode == objLoginAcc.objSite.siteCode).toList();
        });
        doFilter();
      }
    }).catchError((e) {
      print("Error Load User List -> " + e.toString());
    });
  }
  Future<void> deleteUser(CUser objUser) async{
    final svcUser userService = svcUser();
    // save data contact using service
    userService.deleteUser(objUser.userId.toString()).then((objResult) {
      if (objResult != null) {
        doRefresh();
      }
    }).catchError((e) {
      print("Error Delete User -> " + e.toString());
    });
  }

  Widget separator(BuildContext context){
    return Column(
      children: [
        SizedBox(height: 8),
        Container(height: 8, color: Colors.grey[200]),
        SizedBox(height: 8),
      ],
    );
  }
  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'User List')
        ],
      ),
    );
  }

  Widget Cmd_Refresh(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.refresh, color: Colors.blue),
        tooltip: 'Refresh',
        onPressed: (){
          doRefresh();
        },
      ),
    );
  }
  Widget Cmd_ClearAllFilter(BuildContext context){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.tune),
        tooltip: 'Filter Clear',
        onPressed: (){
          doClearFilter();
        },
      ),
    );
  }
  Widget Cmd_Add(BuildContext context, CUser objLoginAcc) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.add, color: Colors.blue),
        tooltip: 'Add User',
        onPressed: (){
          _buildMessageDialog(context, null, objLoginAcc)
              .then((void value) => doRefresh());
        },
      ),
    );
  }
  Widget Cmd_Edit(BuildContext context, CUser objUser, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.edit),
        tooltip: 'Edit',
        onPressed: (){
          _buildMessageDialog(context, objUser, objLoginAcc).then((void value) => doRefresh());
        },
      ),
    );
  }
  Widget Cmd_Delete(BuildContext context, CUser objUser, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.delete),
        tooltip: 'Delete',
        onPressed: (){
            deleteUser(objUser);
        },
      ),
    );
  }

  Future<void> _buildMessageDialog(BuildContext context, CUser objUser, CUser objLoginAcc){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  //color: Colors.transparent,
                  width: 500.0,
                  height: double.infinity,
                  child: UserFormPage(
                      objUser: objUser,
                      objLoginAcc: objLoginAcc),
                ),
              )
          );
        });
  }

  Widget _buildTitleUser(BuildContext context, CUser objLoginAcc){
    return  Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      BadgeInfo(objSite: objLoginAcc.objSite),
                    ]
                ),
              ],
            ),
            Row(
              children: [
                Cmd_Add(context, objLoginAcc),
                SizedBox(width: 10.0),
                Cmd_Refresh(context)
              ],
            )
          ],
        )
      ],
    );
  }

  onSearchTextChanged(int index, String str) async {
    doFilter();
    setState(() {});
  }

  doFilter(){
    setState(() {
      _searchResult.clear();
      _searchResult = _userList
          .where((CUser) =>
                  (TxtController[0].text.isEmpty ? true : CUser.username.toUpperCase().contains(TxtController[0].value.text.toUpperCase())) &&
                  (TxtController[1].text.isEmpty ? true : CUser.phonenumber.toUpperCase().contains(TxtController[1].value.text.toUpperCase())) &&
                  (TxtController[2].text.isEmpty ? true : CUser.emailaddress.toUpperCase().contains(TxtController[2].value.text.toUpperCase())) &&
                  (TxtController[3].text.isEmpty ? true : CUser.isActive.toUpperCase().contains(TxtController[3].value.text.toUpperCase())) &&
                  (TxtController[4].text.isEmpty ? true : CUser.isAdmin.toUpperCase().contains(TxtController[4].value.text.toUpperCase())) &&
                  (TxtController[5].text.isEmpty ? true : CUser.objSite.siteCode.toUpperCase().contains(TxtController[4].value.text.toUpperCase()))
      ).toList();
    });
  }

  doRefresh(){
    loadUser(widget.objLoginAcc);
    setState(() {});
  }

  doSort(){
    setState(() {
      _userList.sort((a, b) {
        return b.userId.compareTo(a.userId);
      });
    });
  }

  doClearFilter(){
    TxtController.forEach((txtController) {
      txtController.clear();
    });
    doFilter();
  }

  bool isFiltered(){
    int found = 0;
    TxtController.forEach((txtController) {
      if(txtController.text.isNotEmpty) {
        found = found +1;
      }
    });
    if(found > 0){
      return true;
    }
    else{
      return false;
    }
  }

  Widget _buildTxtSearch(BuildContext context, int index){
    return Transform.translate(
          offset: Offset(-5, 0), //Offset(-16, 0),
          child: Transform.scale(
              scale: 0.9, //0.8,
              child: TextFormField(
                  controller: TxtController[index],
                  focusNode:  TxtNode[index],
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: tableHeader[index],
                      hintText: tableHeader[index],
                      isDense: true,
                      contentPadding: EdgeInsets.all(5),
                      suffixIcon: Transform.translate(
                          offset: Offset(0,0),
                          child:Transform.scale(
                              scale: 0.75,
                              child: IconButton(icon: Icon(Icons.cancel),
                              tooltip: 'Clear',
                              onPressed: () {
                                  TxtController[index].clear();
                                  onSearchTextChanged(index, '');
                              }),
                          )
                      )
                  ),
                  style: TextStyle(fontSize: 12.0, height: 2.0, color: Colors.black),
                  onChanged: (string) {
                      onSearchTextChanged(index, string);
                  },
                  onFieldSubmitted: (_) {
                      if(index+1 == tableHeader.length) {
                        FocusScope.of(context).requestFocus(TxtNode[0]);
                      }
                      else{
                        FocusScope.of(context).requestFocus(TxtNode[index+1]);
                      }
                  },
              ),
          ),
    );
  }

  Widget _buildRowHeaderUser(BuildContext context){
    return  Padding(
        padding: EdgeInsets.only(top: 2.5),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(flex: 1, child: Cmd_ClearAllFilter(context)),
                    Expanded(flex: 3, child: _buildTxtSearch(context,0)),  // username
                    Expanded(flex: 3, child: _buildTxtSearch(context,1)),  // phone
                    Expanded(flex: 3, child: _buildTxtSearch(context,2)),  // email
                    Expanded(flex: 2, child: _buildTxtSearch(context,3)),  // isactive
                    Expanded(flex: 2, child: _buildTxtSearch(context,4)),  // isadmin
                    Expanded(flex: 3, child: _buildTxtSearch(context,5)),  // sitecode
                    //Expanded(flex: 3, child: Text("Last Login", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0,))),
                    Expanded(flex: 3, child: Text("Action", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0,))),
                  ]
              ),
            ]
        )
    );
  }

  Widget _buildRowListContact(BuildContext context, CUser objUser, CUser objLoginAcc){
    return  Padding(
        padding: EdgeInsets.only(bottom: 0.5, top: 0.5),
        child:
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(flex: 1, child: Text(objUser.userId.toString(),
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objUser.username == null ? '-'  : objUser.username,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objUser.phonenumber == null ? '-' : objUser.phonenumber,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Text(objUser.emailaddress == null ? '-' : objUser.emailaddress,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 2, child: Text(objUser.isActive  == null ? '-' : objUser.isActive,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  Expanded(flex: 2, child: Text(objUser.isAdmin  == null ? '-' : objUser.isAdmin,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  Expanded(flex: 3, child: Text(objUser.objSite.siteCode  == null ? '-' : objUser.objSite.siteCode,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  //Expanded(flex: 3, child: Text(objUser  == null ? '-' : objUser.objSite.siteCode,
                  //    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                  Expanded(flex: 3, child: Row(
                    children: [
                      Cmd_Edit(context, objUser, objLoginAcc),
                      SizedBox(width: 15.0),
                      Cmd_Delete(context, objUser, objLoginAcc)
                    ],
                  ))
                ]
            ),
            Divider(),
          ],
        )
    );
  }

  Widget _buildListViewUser(BuildContext context, CUser objLoginAcc){
    return Expanded(
      //child: _searchResult.length != 0 || controller.text.isNotEmpty ?
      child: _searchResult.length != 0 || isFiltered() ?
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, i) {
          return _buildRowListContact(context, _searchResult[i],objLoginAcc);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _userList.length,
        itemBuilder: (context, index) {
          return _buildRowListContact(context, _userList[index],objLoginAcc);
        },
      ),
    );
  }

  Widget buildList(BuildContext context, CUser objLoginAcc) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTitleUser(context, objLoginAcc),
        separator(context),
        _buildRowHeaderUser(context),
        separator(context),
        _buildListViewUser(context, objLoginAcc)
      ],
    );
  }


  @override
  void initState() {
    super.initState();
    // init txtController
    TxtController = List.generate(tableHeader.length, (i) => TextEditingController());
    // init focusNode
    TxtNode = List.generate(tableHeader.length, (i) => FocusNode());
    loadUser(widget.objLoginAcc);
  }

  @override
  void dispose() {
    // dispose txtController
    TxtController.forEach((c) => c.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          height: double.infinity,
          child: buildList(context, widget.objLoginAcc),
        ),
      ),
    );
  }

}