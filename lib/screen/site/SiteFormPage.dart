import 'dart:ui';
import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:myggguestdesk/model/CSiteSaveReq.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CApplication.dart';
import 'package:myggguestdesk/model/CQuestionaireH.dart';
import 'package:myggguestdesk/validation/valContactForm.dart';
import 'package:myggguestdesk/service/svcSite.dart';
import 'package:myggguestdesk/service/svcQuestionaireH.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:typed_data';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
//import 'package:path_provider/path_provider.dart';
//import 'package:share_plus/share_plus.dart';


class SiteFormPage extends StatefulWidget {
  final CSite objSite;
  final CUser objLoginAcc;

  // constructor
  SiteFormPage({Key key,
    @required this.objSite,
    @required this.objLoginAcc
  }) : super(key: key);

  @override
  _SiteFormPage createState() => new _SiteFormPage();
}

class _SiteFormPage extends State<SiteFormPage> with valContactForm {
  final key = GlobalKey();
  final _formSiteKey  = GlobalKey<FormState>(); // globalkey for validation
  Future<List<CQuestionaireH>> _futureQuestionList;
  Future<CApplication> _futureAppSetting;

  CQuestionaireH _dropdownValueQue;
  File file;
  FocusNode NodeSiteCode         = FocusNode();
  FocusNode NodeSiteName         = FocusNode();
  FocusNode NodeCmdSave          = FocusNode();
  FocusNode NodeCmdCancel        = FocusNode();
  FocusNode NodeQuestionaire     = FocusNode();

  TextEditingController _ctrlSiteCode = new TextEditingController();
  TextEditingController _ctrSiteName  = new TextEditingController();

  String strSiteCode     = '';
  String strSiteName     = '';
  bool isActive = false;
  bool checkUseQuestionaire   = false;

  Future <CApplication> do_read_session() async{
    CApplication result;

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      try{
        if(preferences.getString("appsmobileurl") != null) {
          result = CApplication(
              appsname: preferences.getString("appsname"),
              appsdescription: preferences.getString("appsdescription"),
              appversion: preferences.getString("appversion"),
              apiversion: preferences.getString("apiversion"),
              appscredit: preferences.getString("appscredit"),
              appsmobileurl: preferences.getString("appsmobileurl"),
              appsstatus: preferences.getString("appsstatus")
          );
        }
        else{
          print('No Session Saved');
        }
      }
      catch(exception){
      }
    });
    return result;
  }

  Future<List<CQuestionaireH>> loadListForm(CUser objLoginAcc) async{
    List<CQuestionaireH> result = [];
    svcQuestionaireH queHService = new svcQuestionaireH();
    await queHService.getListForm().then((List<CQuestionaireH> objListQue) {
          result = objListQue;
    }).catchError((e){
      print('loadQueList Err -> ' + e);
    });
    return result;
  }
  Future<CQuestionaireH> loadQuestionaire(String pqueId) async{
    CQuestionaireH result;
    svcQuestionaireH queHService = new svcQuestionaireH();
    await queHService.getQuestionaireById(pqueId).then((CQuestionaireH objQue) {
        result = objQue;
        setState(() {
          _dropdownValueQue = objQue;
        });
      }).catchError((e){
          print('loadQuestionaire Err -> ' + e);
      });
    return result;
  }

//

  Future<void> saveSite(CSiteSaveReq objSaveSite) async{
    final svcSite siteService = svcSite();
    // save data Site using service
    siteService.saveSite(objSaveSite).then((objResult) {
      if (objResult != null) {
        Navigator.of(context).pop();
      }
    }).catchError((e) {
      print("Error Save Site -> " + e.toString());
    });
  }
  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          //strOperation == 'Edit' ? TextSpan(text: 'Edit Site') :  TextSpan(text: 'Site')
          TextSpan(text: 'Site')
        ],
      ),
    );
  }
  Widget SiteBadge(BuildContext context, CSite objSite) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: InkWell(
            child: new Container(
              width: 130.0,
              height: 20.0,
              decoration: new BoxDecoration(
                color: Colors.blue,
                border: new Border.all(color: Colors.white, width: 2.0),
                borderRadius: new BorderRadius.circular(10.0),
              ),
              child: new Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(objSite.siteName, style: TextStyle(fontSize: 10.0, color: Colors.white ))
                  ],
                ),
              ),
            ))
    );
  }


  /*
  Widget Cmd_ShareQR(BuildContext context){
   return ElevatedButton(
     child: Text('Share'),
     onPressed: () async {
       try {
          RenderRepaintBoundary boundary = key.currentContext.findRenderObject() as RenderRepaintBoundary;
          //captures qr image
          var image = await boundary.toImage();
          ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
          Uint8List pngBytes = byteData.buffer.asUint8List();
          //app directory for storing images.
          final appDir = await getApplicationDocumentsDirectory();
          //current time
          var datetime = DateTime.now();
          //qr image file creation
          file = await File('${appDir.path}/$datetime.png').create();
          //appending data
          await file?.writeAsBytes(pngBytes);
          //Shares QR image
          await Share.shareFiles(
                [file.path],
                mimeTypes: ["image/png"],
                text: "Share the QR Code",
          );
       } catch (e) {
          print(e.toString());
       }
     },
   );
  }
  */

  Widget Cmd_Save(BuildContext context, CUser objLoginAcc) {
    return InkWell(
        focusNode: NodeCmdSave,
        onTap: () {
          if (_formSiteKey.currentState.validate()) {
            _formSiteKey.currentState.save();

            saveSite(
                CSiteSaveReq(
                    siteCode: strSiteCode,
                    siteName: strSiteName,
                    isActive: isActive == true ? 'Y' : 'N',
                    formaccessmentid: checkUseQuestionaire == true ? _dropdownValueQue.quehid : -1,
                    createdBy: objLoginAcc.username
                )
            );
          }
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Save', style: TextStyle(fontSize: 14.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel', style: TextStyle(fontSize: 14.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_CopyToClipboard(BuildContext context, String strData) {
    return
    Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.copy, color: Colors.grey),
        tooltip: 'Copy to clipboard',
        onPressed: (){
            Clipboard.setData(ClipboardData(text: strData));
        },
      ),
    );
  }

  Widget Txt_SiteCode(BuildContext context) {
    return TextFormField(
      controller: _ctrlSiteCode,
      focusNode: NodeSiteCode,
      keyboardType: TextInputType.text,
      validator: validateContactName,
      onSaved: (String value) {
        strSiteCode = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.location_city_rounded),
        labelText: 'Site Code',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeSiteName);
      },
    );
  }
  Widget Txt_SiteName(BuildContext context) {
    return TextFormField(
      controller: _ctrSiteName,
      focusNode: NodeSiteName,
      keyboardType: TextInputType.text,
      validator: validateCompany,
      onSaved: (String value) {
        strSiteName = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.location_city),
        labelText: 'Site Name',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSave);
      },
    );
  }
  Widget Chk_IsAcative(BuildContext context){
    return Padding(
        padding: EdgeInsets.only(left: 20.0),
        child: SwitchListTile(
            title: const Text('IsActive',
                style: TextStyle(fontSize: 12.0, color: Colors.grey)
            ),
            value: isActive,
            onChanged: (bool val) =>
                setState(() => isActive = val)
        )
    );
  }
  Widget Chk_UseQuestionaire(BuildContext context, CSite objSite, CUser objLoginAcc){
    //return objSite != null ?
    return
    Column(
      children: [
        Padding(
            padding: EdgeInsets.only(top: 5.0, left: 25.0, bottom: 5.0),
            child: Row(
              children: [
                Checkbox(
                  value: checkUseQuestionaire, //false, //this.value,
                  onChanged: (bool value) {
                    setState(() {
                      checkUseQuestionaire = value;
                      //if(value == true){
                      //  _futureQuestionList = null;
                      //}
                    });
                  },
                ),
                Text('Use Questionaire',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12.0,
                  ),
                ),
              ],
            )
        )
      ],
    );
  }

  Widget Cbo_QueAssign(BuildContext context) {
    return checkUseQuestionaire == true ?
      FutureBuilder(
        future: _futureQuestionList,
        builder: (BuildContext context, AsyncSnapshot<List<CQuestionaireH>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          else if(snapshot.connectionState == ConnectionState.done){
            print('snapshot: $snapshot');
            if(snapshot.hasData){
              return DropdownButtonHideUnderline(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    InputDecorator(
                      decoration: InputDecoration(
                        filled: false,
                        icon: const Icon(Icons.dynamic_form),
                        labelText: 'Questionaire Assign',
                      ),
                      isEmpty: _dropdownValueQue == null,
                      child: DropdownButton<CQuestionaireH>(
                        focusNode: NodeQuestionaire,
                        value: _dropdownValueQue,
                        isDense: true,
                        items: snapshot.data.map((CQuestionaireH value) {
                          return DropdownMenuItem<CQuestionaireH>(
                            child: Text(value.quehname, style: TextStyle(fontSize: 12.0, color: Colors.black)),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (CQuestionaireH newValue) {
                          print('value change -> ' + newValue.quehname);
                          setState(() {
                            _dropdownValueQue = newValue;
                          });
                          FocusScope.of(context).requestFocus(NodeCmdSave);
                        },
                      ),
                    ),
                  ],
                ),
              );
            }
            else{
              print('No Data');
              return DropdownButton(
                  hint: Text("Questionaire",
                      style: TextStyle(fontSize: 12.0, color: Colors.grey))
              );
            }
          }
          else{
            print('E');
            return Center(child: CircularProgressIndicator() );
          }
        }
    ) :
    Column();
  }

  Widget _buildFormEditSite(BuildContext context, CSite objSite, CApplication objApp, CUser objLoginAcc){

    return Form(
        key: _formSiteKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              children: [
                Lbl_Title(context),
                //SiteBadge(context,)
                Divider(),
                Txt_SiteCode(context),
                SizedBox(height: 5.0),
                Txt_SiteName(context),
                SizedBox(height: 5.0),
                Chk_IsAcative(context),
                SizedBox(height: 5.0),
                Chk_UseQuestionaire(context, objSite, objLoginAcc),
                Cbo_QueAssign(context),
                SizedBox(height: 10.0),
                Row(
                  children: [
                    objSite == null ?
                        Column() :
                        RepaintBoundary(
                          key: key,
                          child: Container(
                            color: Colors.white,
                            child: QrImage(
                                data: objApp.appsmobileurl + objSite.siteCode,  //baseWebUrl + objSite.siteCode,
                                version: QrVersions.auto,
                                size: 98.0,
                                gapless: false,
                            ),
                          )
                        ),
                    objSite == null ?
                        Column() :
                        Expanded(flex: 2, child: Text(objApp.appsmobileurl + objSite.siteCode)),
                    //Cmd_ShareQR(context),
                    //Cmd_CopyToClipboard(context, baseWebUrl + objSite.siteCode)
                    objSite == null ?
                        Column() :
                        Cmd_CopyToClipboard(context, objApp.appsmobileurl + objSite.siteCode)
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Cmd_Cancel(context),
                    Cmd_Save(context, objLoginAcc)
                  ],
                ),
                SizedBox(height: 5.0),
              ],
            ),
          ],
        )
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _futureAppSetting = do_read_session();

      // init controller
      if(widget.objSite == null){
         print('initState -> site null');
         _ctrlSiteCode        = TextEditingController();
         _ctrSiteName         = TextEditingController();
         isActive             = true;
         checkUseQuestionaire = false;
      }
      else{
         print('initState ->  site not null');
         _ctrlSiteCode        = TextEditingController(text: widget.objSite.siteCode);
         _ctrSiteName         = TextEditingController(text: widget.objSite.siteName);
         isActive             = widget.objSite.isActive == 'Y' ? true : false;
         checkUseQuestionaire = widget.objSite.formaccessmentid <= 0 ? false : true;
         if(widget.objSite.formaccessmentid > 0) {
            loadQuestionaire(widget.objSite.formaccessmentid.toString());
         }
      }

      // load
      _futureQuestionList = loadListForm(widget.objLoginAcc);
    });
  }

  @override
  void dispose() {
    _ctrlSiteCode.dispose();
    _ctrSiteName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('build');

    //return _buildFormEditSite(context, widget.objSite, widget.objLoginAcc);
    return FutureBuilder(
        future: _futureAppSetting,
        builder: (ctx,  snapshot) {
          //return Text(widget.objLoginAcc.objSite.siteCode);

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          else if(snapshot.connectionState == ConnectionState.done){
            print('snapshot: $snapshot');
            if (snapshot.hasData) {
              print('C');
              return _buildFormEditSite(context, widget.objSite, snapshot.data, widget.objLoginAcc);
            }
            else{
              print('D');
              return Text('No Data');
            }
          }
          else {
            print('E');
            return Center(child: CircularProgressIndicator() );
          }
        });
  }

}
