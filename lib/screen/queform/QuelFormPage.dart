import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CQuestionaire.dart';
import 'package:myggguestdesk/model/CQuestionaireSaveReq.dart';
import 'package:myggguestdesk/service/svcQuestionaire.dart';
import 'package:myggguestdesk/validation/valUserProfile.dart';
import 'package:myggguestdesk/widget/NumberCounter.dart';

class QuelFormPage extends StatefulWidget {
  final CQuestionaire objQue;
  final CUser objLoginAcc;

  QuelFormPage({Key key,
    @required this.objQue,
    @required this.objLoginAcc,
  }) : super(key: key);

  @override
  _QuelFormPage createState() => _QuelFormPage();
}

class _QuelFormPage extends State<QuelFormPage> with valUserProfile{
  final formQuelKey = GlobalKey<FormState>();

  TextEditingController _ctrlPointText   = new TextEditingController();
  TextEditingController _ctrlPointNo      = new TextEditingController();

  FocusNode NodeCmdSave     = FocusNode();
  FocusNode NodePointNo     = FocusNode();
  FocusNode NodePointText   = FocusNode();

  String strPointText;
  String strPointNo;
  int lastSequence = 0;

  Future<void> saveForm(CQuestionaireSaveReq objQPointSave) async{
    final svcQuestionaire qPointService = svcQuestionaire();
    // save data user using service
    qPointService.saveQuePoint(objQPointSave).then((objQuePoint) {
      if (objQuePoint != null) {
      }
      Navigator.of(context).pop();
    }).catchError((e) {
      print("Error Save Point Question -> " + e.toString());
    });
  }

  Future<void> getLastSequencePoint(String paramQuehId) async{
    print("getLastSequencePoint -> " + paramQuehId);
    final svcQuestionaire  queService = svcQuestionaire();
    await queService.getListQuestionaireById(paramQuehId).then((objResult) {
      setState(() {
        lastSequence = objResult.length + 1;
      });
    }).catchError((e) {
      print("Error Load Questionaire List -> " + e.toString());
    });
  }


  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          TextSpan(text: 'Point Questionaire')
        ],
      ),
    );
  }
  Widget Txt_Point_No(BuildContext context) {
    return TextFormField(
      //readOnly: widget.objQue == null ? false : true,
      controller: _ctrlPointNo,
      focusNode: NodePointNo,
      keyboardType: TextInputType.text,
      validator: validateUsername,
      onSaved: (String value) {
        strPointNo = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.format_list_numbered),
        labelText: 'No',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePointText);
      },
    );
  }
  Widget Txt_Point_Text(BuildContext context) {
    return TextFormField(
      controller: _ctrlPointText,
      focusNode: NodePointText,
      keyboardType: TextInputType.text,
      validator: validatePhoneNo,
      maxLines: null,
      onSaved: (String value) {
        strPointText = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.question_answer),
        labelText: 'Question',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSave);
      },
    );
  }

  Widget Cmd_Save(BuildContext context,  CQuestionaire objQue, CUser objLoginAcc) {
    return InkWell(
        focusNode: NodeCmdSave,
        onTap: () {
          if (formQuelKey.currentState.validate()) {
            formQuelKey.currentState.save();
            saveForm(
                CQuestionaireSaveReq(
                    quehid: objQue.quehid,
                    quedid: objQue.quedid == null ? 0 : objQue.quedid,
                    quelineno: strPointNo == '' ? 0 : int.parse(strPointNo),
                    quetext: strPointText,
                    createdBy: objLoginAcc.username
                )
            );
          }
        },
        child: new Container(
          width: 150.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Save',
                    style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel', style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget _buildFormEditQuestion(BuildContext context, CQuestionaire objQue, CUser objLoginAcc) {

    print('_buildFormQueL ->' + objLoginAcc.isAdmin);
    return Form(
        key: formQuelKey,
        child: Column(
          children: <Widget>[
            Lbl_Title(context),
            Divider(),
            //NumberCounter(),
            Txt_Point_No(context),
            Txt_Point_Text(context),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Cmd_Cancel(context),
                Cmd_Save(context, objQue, objLoginAcc)
              ],
            ),
          ],
        )
    );
  }

  @override
  void initState() {
    setState(() {
      if(widget.objQue != null){
        // check last sequence of point questionaire first
        getLastSequencePoint(widget.objQue.quehid.toString()).then((value) {
          if(widget.objQue.quedid == null ){
            print("A");
            _ctrlPointNo    = TextEditingController(text: lastSequence.toString());
            _ctrlPointText  = TextEditingController();
          }
          else{
            print("B");
            _ctrlPointNo    = TextEditingController(text: widget.objQue.quelineno.toString());
            _ctrlPointText  = TextEditingController(text: widget.objQue.quetext);
          }
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _ctrlPointNo.dispose();
    _ctrlPointText.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _buildFormEditQuestion(context, widget.objQue, widget.objLoginAcc);
  }
}
