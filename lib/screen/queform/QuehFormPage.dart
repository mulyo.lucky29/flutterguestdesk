import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CQuestionaireH.dart';
import 'package:myggguestdesk/model/CQuestionaireHSaveReq.dart';
import 'package:myggguestdesk/service/svcQuestionaireH.dart';
import 'package:myggguestdesk/validation/valFormQue.dart';

class QuehFormPage extends StatefulWidget {
  final CQuestionaireH objForm;
  final CUser objLoginAcc;

  QuehFormPage({Key key,
    @required this.objForm,
    @required this.objLoginAcc,
  }) : super(key: key);

  @override
  _QuehFormPage createState() => _QuehFormPage();
}

class _QuehFormPage extends State<QuehFormPage> with valFormQue{
  final formQuehKey = GlobalKey<FormState>();

  TextEditingController _ctrlFormName   = new TextEditingController();
  TextEditingController _ctrlFormDesc   = new TextEditingController();

  FocusNode NodeCmdSave     = FocusNode();
  FocusNode NodeFormName    = FocusNode();
  FocusNode NodeFormDesc    = FocusNode();

  String strFormName;
  String strFormDesc;

  Future<void> saveForm(CQuestionaireHSaveReq objQFormSave) async{
    final svcQuestionaireH qFromService = svcQuestionaireH();
    qFromService.saveQuehForm(objQFormSave).then((objFormQ) {
         if (objFormQ != null) { }
          Navigator.of(context).pop();
    }).catchError((e) {
      print("Error Save Form -> " + e.toString());
    });
  }

  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          TextSpan(text: 'Form Questionaire')
        ],
      ),
    );
  }
  Widget Txt_Form_Name(BuildContext context) {
    return TextFormField(
      //readOnly: widget.objForm == null ? false : true,
      controller: _ctrlFormName,
      focusNode: NodeFormName,
      keyboardType: TextInputType.text,
      validator: validateFormName,
      onSaved: (String value) {
        strFormName = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.dynamic_form),
        labelText: 'Form Name',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeFormDesc);
      },
    );
  }
  Widget Txt_Form_Description(BuildContext context) {
    return TextFormField(
      controller: _ctrlFormDesc,
      focusNode: NodeFormDesc,
      keyboardType: TextInputType.text,
      onSaved: (String value) {
        strFormDesc = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.description),
        labelText: 'Form Description',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSave);
      },
    );
  }

  Widget Cmd_Save(BuildContext context,  CQuestionaireH objForm, CUser objLoginAcc) {
    return InkWell(
        focusNode: NodeCmdSave,
        onTap: () {
          if (formQuehKey.currentState.validate()) {
            formQuehKey.currentState.save();
            print("Cmd_Save");
            print("quehId   -> " + (objForm == null ? "0" : objForm.quehid.toString()));
            print("strFormName   -> " + strFormName);
            print("strFormDesc   -> " + strFormDesc);

            saveForm(
                CQuestionaireHSaveReq(
                    quehid: objForm == null ? 0 : objForm.quehid,
                    quehname: strFormName,
                    quehdesc: strFormDesc,
                    createdBy: objLoginAcc.username
                ),
            );
          }
        },
        child: new Container(
          width: 150.0,
          height: 30.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Save',
                    style: TextStyle(fontSize: 12.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel', style: TextStyle(fontSize: 14.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }

  Widget _buildFormEditForm(BuildContext context, CQuestionaireH objForm, CUser objLoginAcc) {
    return Form(
        key: formQuehKey,
        child: Column(
          children: <Widget>[
            Lbl_Title(context),
            Divider(),
            Txt_Form_Name(context),
            Txt_Form_Description(context),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Cmd_Cancel(context),
                Cmd_Save(context, objForm, objLoginAcc)
              ],
            ),
          ],
        )
    );
  }

  @override
  void initState() {
    setState(() {
      if(widget.objForm == null ){
        _ctrlFormName   = TextEditingController();
        _ctrlFormDesc   = TextEditingController();
      }
      else{
        _ctrlFormName   = TextEditingController(text: widget.objForm.quehname);
        _ctrlFormDesc   = TextEditingController(text: widget.objForm.quehdesc);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _ctrlFormName.dispose();
    _ctrlFormDesc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _buildFormEditForm(context, widget.objForm, widget.objLoginAcc);
  }
}
