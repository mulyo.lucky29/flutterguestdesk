import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/screen/queform/QuehFormPage.dart';
import 'package:myggguestdesk/screen/queform/QuelFormPage.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CQuestionaire.dart';
import 'package:myggguestdesk/model/CQuestionaireH.dart';
import 'package:myggguestdesk/service/svcQuestionaire.dart';
import 'package:myggguestdesk/service/svcQuestionaireH.dart';
import 'package:myggguestdesk/widget/BadgeInfo.dart';

class QuePage extends StatefulWidget {
  final TabController objTabMenuController;
  final CUser objLoginAcc;

  QuePage({Key key,
    @required this.objTabMenuController,
    @required this.objLoginAcc
  }) : super(key: key);

  @override
  _QuePage createState() => new _QuePage();
}

class _QuePage extends State<QuePage> {
  bool _isFiltered = false;
  List<CQuestionaireH> _formList = [];
  List<CQuestionaire> _queList = [];
  Future<List<CQuestionaireH>> _futureListForm;
  CQuestionaireH _dropdownValueQue;

  FocusNode NodeQuestionaire     = FocusNode();

  var refreshKey = GlobalKey<RefreshIndicatorState>();
  //List<String> tableHeader = ['username', 'phone', 'email','isActive','isAdmin','Site Code'];
  //List<TextEditingController> TxtController = [];

  Future<List<CQuestionaireH>> loadListForm(CUser objLoginAcc) async{
    List<CQuestionaireH> result = [];
    svcQuestionaireH queHService = new svcQuestionaireH();
    await queHService.getListForm().then((List<CQuestionaireH> objListQue) {
      if (objListQue != null) {
        setState(() {
          _formList = objListQue;
          result = objListQue;
        });
      }
    }).catchError((e){
        print('loadQueList Err -> ' + e);
    });
    return result;
  }
  Future<void> loadListQuestion(CQuestionaireH objQueH) async{
    print("loadListQuestion");
    final svcQuestionaire  queService = svcQuestionaire();
    await queService.getListQuestionaireById(objQueH.quehid.toString()).then((objResult) {
      if (objResult != null) {
        setState(() {
          _queList = objResult;
        });
      }
    }).catchError((e) {
      print("Error Load Questionaire List -> " + e.toString());
    });
  }

  Future<void> deleteForm(String strQuehId, CUser objLoginAcc) async{
    print("deleteForm");
    final svcQuestionaireH qFromService = svcQuestionaireH();
    qFromService.deleteQuehForm(strQuehId).then((objResult) {
      if (objResult != null) {
        setState(() {
           // move to first before refresh
          _dropdownValueQue = _formList[0];
        });
      }
        doRefresh();
    }).catchError((e) {
      print("Error Delete Form -> " + e.toString());
    });
  }

  Future<void> deletePointQuestionaire(String strQuePointId) async{
    print("deleteForm");
    final svcQuestionaire qPointService = svcQuestionaire();
    qPointService.deleteQuePoint(strQuePointId).then((objResult) {
      if (objResult != null) {

      }
      doRefresh();
    }).catchError((e) {
      print("Error Delete Point -> " + e.toString());
    });
  }

  Widget separator(BuildContext context){
    return Column(
      children: [
        SizedBox(height: 8),
        Container(height: 8, color: Colors.grey[200]),
        SizedBox(height: 8),
      ],
    );
  }
  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Questionaire Form')
        ],
      ),
    );
  }

  Widget Cmd_Refresh(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.refresh, color: Colors.blue),
        tooltip: 'Refresh',
        onPressed: (){
          doRefresh();
        },
      ),
    );
  }
  Widget Cmd_New_Form(BuildContext context, CUser objLoginAcc) {
    print("Cmd_New_Form");
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.add_comment_outlined, color: Colors.blue),
        tooltip: 'New Form',
        onPressed: (){
          _buildMessageDialogForm(context, null, objLoginAcc)
              .then((void value) {
                doRefresh() ;
            }
          );
        },
      ),
    );
  }
  Widget Cmd_Edit_Form(BuildContext context, CQuestionaireH objForm, CUser objLoginAcc) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.edit, color: Colors.blue),
        tooltip: 'Edit Form',
        onPressed: (){
          _buildMessageDialogForm(context, objForm, objLoginAcc)
              .then((void value) => doRefresh());
        },
      ),
    );
  }
  Widget Cmd_Delete_Form(BuildContext context, CQuestionaireH objForm, CUser objLoginAcc) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.delete, color: Colors.blue),
        tooltip: 'Delete Form',
        onPressed: (){
          deleteForm(objForm.quehid.toString(), objLoginAcc);
        },
      ),
    );
  }

  Widget Cmd_Add(BuildContext context, CUser objLoginAcc) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.add_comment, color: Colors.blue),
        tooltip: 'Add Question',
        onPressed: (){
          if(_dropdownValueQue == null) {
          }
          else {
            // selected form type already
            _buildMessageDialog(context,
                CQuestionaire(
                    quehid: _dropdownValueQue.quehid,
                    quehname: _dropdownValueQue.quehname,
                    quehdesc: _dropdownValueQue.quehdesc
                )
                , objLoginAcc)
                .then((void value) => doRefresh());
          }
        },
      ),
    );
  }
  Widget Cmd_Edit(BuildContext context, CQuestionaire objQue, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.edit),
        tooltip: 'Edit',
        onPressed: (){
          if(_dropdownValueQue == null) {
          }
          else {
            // selected form type already
            _buildMessageDialog(context, objQue, objLoginAcc)
                .then((void value) => doRefresh());
          }
        },
      ),
    );
  }
  Widget Cmd_Delete(BuildContext context, CQuestionaire objQue, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.delete),
        tooltip: 'Delete',
        onPressed: (){
            deletePointQuestionaire(objQue.quedid.toString());
        },
      ),
    );
  }
  Widget Cbo_QueHeader(BuildContext context) {
    return FutureBuilder(
        future: _futureListForm,
        builder: (BuildContext context, AsyncSnapshot<List<CQuestionaireH>> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          else if(snapshot.connectionState == ConnectionState.done){
            print('snapshot: $snapshot');
            if(snapshot.hasData){
              return
                Flexible(
                  child:  Container(
                            width: 450, //double.maxFinite,
                            height: 60,
                            child: DropdownButtonHideUnderline(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    InputDecorator(
                                      decoration: InputDecoration(
                                        filled: false,
                                        icon: const Icon(Icons.dynamic_form),
                                        labelText: 'Questionaire Form Name',
                                      ),
                                      isEmpty: _dropdownValueQue == null,
                                      child: DropdownButton<CQuestionaireH>(
                                        focusNode: NodeQuestionaire,
                                        value: _dropdownValueQue,
                                        isDense: true,
                                        items: snapshot.data.map((CQuestionaireH value) {
                                          return DropdownMenuItem<CQuestionaireH>(
                                            child: Text(value.quehname, style: TextStyle(fontSize: 12.0, color: Colors.black)),
                                            value: value,
                                          );
                                        }).toList(),
                                        onChanged: (CQuestionaireH newValue) {
                                          print('value change -> ' + newValue.quehname);
                                          setState(() {
                                            _dropdownValueQue = newValue;
                                          });
                                          // load List Questionaire
                                          loadListQuestion(newValue);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              )
                             )
                );
            }
            else{
              print('No Data');
              return DropdownButton(
                  hint: Text("Questionaire",
                      style: TextStyle(fontSize: 12.0, color: Colors.grey))
              );
            }
          }
          else{
            print('E');
            return Center(child: CircularProgressIndicator() );
          }
        }
    );
  }

  Future<void> _buildMessageDialogForm(BuildContext context, CQuestionaireH objForm, CUser objLoginAcc){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  //color: Colors.transparent,
                  width: 500.0,
                  height: double.infinity,
                  child: QuehFormPage(
                      objForm: objForm,
                      objLoginAcc: objLoginAcc),
                ),
              )
          );
        });
  }
  Future<void> _buildMessageDialog(BuildContext context, CQuestionaire objQue, CUser objLoginAcc){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  //color: Colors.transparent,
                  width: 500.0,
                  height: double.infinity,
                  child: QuelFormPage(
                      objQue: objQue,
                      objLoginAcc: objLoginAcc),
                ),
              )
          );
        });
  }

  Widget _buildTitleQuestionaire(BuildContext context, CUser objLoginAcc){
    return  Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      BadgeInfo(objSite: objLoginAcc.objSite),
                    ]
                ),
              ],
            ),
            Row(
              children: [
                Cmd_Add(context, objLoginAcc),
                SizedBox(width: 10.0),
                Cmd_Refresh(context)
              ],
            )
          ],
        )
      ],
    );
  }

  doRefresh(){
    setState(() {
      _futureListForm = loadListForm(widget.objLoginAcc);
      loadListQuestion(_dropdownValueQue);
    });
  }

  Widget _buildRowHeaderQuestionaire(BuildContext context, CUser objLoginAcc){
    return Row(
      children: [
        Cbo_QueHeader(context),
        Cmd_New_Form(context,objLoginAcc),
        Cmd_Edit_Form(context, _dropdownValueQue, objLoginAcc),
        Cmd_Delete_Form(context, _dropdownValueQue, objLoginAcc),
      ],
    );
  }

  Widget _buildRowListQuestionaire(BuildContext context, CQuestionaire objQue, CUser objLoginAcc){
    return  Padding(
        padding: EdgeInsets.only(bottom: 0.5, top: 0.5),
        child:
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(flex: 1, child: Text(objQue.quedid.toString(),
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 1, child: Text(objQue.quelineno == null ? '-'  : objQue.quelineno.toString(),
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 7, child: Text(objQue.quetext == null ? '-' : objQue.quetext,
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                  Expanded(flex: 3, child: Row(
                    children: [
                      Cmd_Edit(context, objQue, objLoginAcc),
                      SizedBox(width: 15.0),
                      Cmd_Delete(context, objQue, objLoginAcc)
                    ],
                  ))
                ]
            ),
            Divider(),
          ],
        )
    );
  }

  Widget _buildListViewQuestionaire(BuildContext context, CUser objLoginAcc){
    return ListView.builder(
      padding: EdgeInsets.only(top: 10.0),
      scrollDirection: Axis.vertical,
      physics: const AlwaysScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: _queList.length,
      itemBuilder: (context, index) {
        return _buildRowListQuestionaire(context, _queList[index],objLoginAcc);
      },
    );
  }

  Widget buildList(BuildContext context, CUser objLoginAcc) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildTitleQuestionaire(context, objLoginAcc),
        separator(context),
        _buildRowHeaderQuestionaire(context, objLoginAcc),
        //Cbo_QueHeader(context),
        separator(context),
        _buildListViewQuestionaire(context, objLoginAcc)
      ],
    );
  }


  @override
  void initState() {
    super.initState();
    doRefresh();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          height: double.infinity,
          child: buildList(context, widget.objLoginAcc),
        ),
      ),
    );
  }

}