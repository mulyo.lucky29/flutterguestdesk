import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/model/CContact.dart';
import 'package:myggguestdesk/model/CContactSaveReq.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/validation/valContactForm.dart';
import 'package:myggguestdesk/service/svcContact.dart';

class ContactFormPage extends StatefulWidget {
  final CContact objContact;
  final CUser objLoginAcc;

  // constructor
  ContactFormPage({Key key,
      @required this.objContact,
      @required this.objLoginAcc
  }) : super(key: key);


  @override
  _ContactFormPageState createState() => new _ContactFormPageState();
}

class _ContactFormPageState extends State<ContactFormPage> with valContactForm {
  final _formContactKey  = GlobalKey<FormState>(); // globalkey for validation

  FocusNode NodeSiteSpesific     = FocusNode();
  FocusNode NodeCompany          = FocusNode();
  FocusNode NodeContactName      = FocusNode();
  FocusNode NodeExtension        = FocusNode();
  FocusNode NodePhone            = FocusNode();
  FocusNode NodeEmail            = FocusNode();
  FocusNode NodeCmdSave          = FocusNode();
  FocusNode NodeCmdCancel        = FocusNode();

  TextEditingController _ctrlSiteSpesific    = new TextEditingController();
  TextEditingController _ctrlCompany         = new TextEditingController();
  TextEditingController _ctrlContactName     = new TextEditingController();
  TextEditingController _ctrlExtension       = new TextEditingController();
  TextEditingController _ctrlPhone           = new TextEditingController();
  TextEditingController _ctrlEmail           = new TextEditingController();

  String strSiteSpesific     = '';
  String strCompany          = '';
  String strContactName      = '';
  String strExtension        = '';
  String strPhone            = '';
  String strEmail            = '';

  Future<void> saveContact(CContactSaveReq objContact) async{
    final svcContact contactService = svcContact();
    // save data contact using service
    contactService.saveContact(objContact).then((objResult) {
      if (objResult != null) {
        //Navigator.popUntil(context,ModalRoute.withName('/Customer'));
        Navigator.of(context).pop();
      }
    }).catchError((e) {
      print("Error Save Contact -> " + e.toString());
    });
  }

  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
              //strOperation == 'Edit' ? TextSpan(text: 'Edit Contact') :  TextSpan(text: 'Contact')
              TextSpan(text: 'Contact')
        ],
      ),
    );
  }
  Widget SiteBadge(BuildContext context, CSite objSite) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: InkWell(
            child: new Container(
              width: 130.0,
              height: 20.0,
              decoration: new BoxDecoration(
                color: Colors.blue,
                border: new Border.all(color: Colors.white, width: 2.0),
                borderRadius: new BorderRadius.circular(10.0),
              ),
              child: new Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(objSite.siteName, style: TextStyle(fontSize: 10.0, color: Colors.white ))
                  ],
                ),
              ),
            ))
    );
  }

  Widget Txt_ContactName(BuildContext context) {
    return TextFormField(
      controller: _ctrlContactName,
      focusNode: NodeContactName,
      keyboardType: TextInputType.text,
      validator: validateContactName,
      onSaved: (String value) {
        strContactName = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.person),
        labelText: 'Contact Name',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCompany);
      },
    );
  }
  Widget Txt_Company(BuildContext context) {
    return TextFormField(
      controller: _ctrlCompany,
      focusNode: NodeCompany,
      keyboardType: TextInputType.text,
      validator: validateCompany,
      onSaved: (String value) {
        strCompany = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.business),
        labelText: 'Company',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeSiteSpesific);
      },
    );
  }
  Widget Txt_SiteSpesific(BuildContext context) {
    return TextFormField(
      controller: _ctrlSiteSpesific,
      focusNode: NodeSiteSpesific,
      keyboardType: TextInputType.text,
      validator: validateSiteSpesific,
      onSaved: (String value) {
        strSiteSpesific = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.my_location),
        labelText: 'Site Spesific',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePhone);
      },
    );
  }
  Widget Txt_Phone(BuildContext context) {
    return TextFormField(
      controller: _ctrlPhone,
      focusNode: NodePhone,
      keyboardType: TextInputType.text,
      //validator: validateContactName,
      onSaved: (String value) {
        strPhone = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.phone),
        labelText: 'Phone',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeExtension);
      },
    );
  }
  Widget Txt_Extension(BuildContext context) {
    return TextFormField(
      controller: _ctrlExtension,
      focusNode: NodeExtension,
      keyboardType: TextInputType.text,
      //validator: validateContactName,
      onSaved: (String value) {
        strExtension = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.mobile_friendly ),
        labelText: 'Extension',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeEmail);
      },
    );
  }
  Widget Txt_Email(BuildContext context) {
    return TextFormField(
      controller: _ctrlEmail,
      focusNode: NodeEmail,
      keyboardType: TextInputType.text,
      //validator: validateContactName,
      onSaved: (String value) {
        strEmail = value;
      },
      style: TextStyle(fontSize: 12.0, color: Colors.black),
      autofocus: false,
      decoration: InputDecoration(
        icon: const Icon(Icons.email),
        labelText: 'Email',
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdSave);
      },
    );
  }

  Widget Cmd_Save(BuildContext context, CContact objContact, CUser objLoginAcc) {
    return InkWell(
        focusNode: NodeCmdSave,
        onTap: () {
          if (_formContactKey.currentState.validate()) {
            _formContactKey.currentState.save();
            saveContact(
                CContactSaveReq(
                    contactId: widget.objContact == null ? 0 : widget.objContact.contactId,
                    siteCode: objLoginAcc.objSite.siteCode,   //widget.objSite.siteCode,
                    siteSpecific: strSiteSpesific,
                    tenantCompany: strCompany,
                    contactType: widget.objContact == null ? 'Person' : widget.objContact.contactType,
                    contactName: strContactName,
                    contactPhone: strPhone,
                    contactEmail: strEmail,
                    contactExt: strExtension,
                    username: objLoginAcc.username
                )
            );
          }
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.lightGreen,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Save', style: TextStyle(fontSize: 14.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }
  Widget Cmd_Cancel(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: new Container(
          width: 100.0,
          height: 25.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            border: new Border.all(color: Colors.white, width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Cancel', style: TextStyle(fontSize: 14.0, color: Colors.black ))
              ],
            ),
          ),
        ));
  }

  Widget _buildFormEditContact(BuildContext context, CContact objContact,CUser objLoginAcc){
    return Form(
        key: _formContactKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //widget.operation == 'View' ?
            //Column():
            Column(
              children: [
                Lbl_Title(context),
                //SiteBadge(context,)
                Divider(),
                Txt_ContactName(context),
                SizedBox(height: 5.0),
                Txt_Company(context),
                SizedBox(height: 5.0),
                Txt_SiteSpesific(context),
                SizedBox(height: 5.0),
                Txt_Phone(context),
                SizedBox(height: 5.0),
                Txt_Extension(context),
                SizedBox(height: 5.0),
                Txt_Email(context),
                SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                      Cmd_Cancel(context),
                      Cmd_Save(context, objContact, objLoginAcc)
                  ],
                ),
                SizedBox(height: 5.0),
              ],
            ),
          ],
        )
    );
  }

  @override
  void initState() {
    super.initState();
    // init controller
    if(widget.objContact == null){
      _ctrlSiteSpesific     = TextEditingController();
      _ctrlCompany          = TextEditingController();
      _ctrlContactName      = TextEditingController();
      _ctrlExtension        = TextEditingController();
      _ctrlPhone            = TextEditingController();
      _ctrlEmail            = TextEditingController();
    }
    else{
      _ctrlSiteSpesific     = TextEditingController(text: widget.objContact.siteSpecific);
      _ctrlCompany          = TextEditingController(text: widget.objContact.tenantCompany);
      _ctrlContactName      = TextEditingController(text: widget.objContact.contactName);
      _ctrlExtension        = TextEditingController(text: widget.objContact.contactExt);
      _ctrlPhone            = TextEditingController(text: widget.objContact.contactPhone);
      _ctrlEmail            = TextEditingController(text: widget.objContact.contactEmail);
    }
  }

  @override
  void dispose() {
    _ctrlSiteSpesific.dispose();
    _ctrlCompany.dispose();
    _ctrlContactName.dispose();
    _ctrlExtension.dispose();
    _ctrlPhone.dispose();
    _ctrlEmail.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
      return _buildFormEditContact(context, widget.objContact, widget.objLoginAcc);
  }
}
