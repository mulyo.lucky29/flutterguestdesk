import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myggguestdesk/screen/contact/ContactFormPage.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CContact.dart';
import 'package:myggguestdesk/widget/BadgeInfo.dart';
import 'package:myggguestdesk/service/svcContact.dart';

class ContactPage extends StatefulWidget {
  final TabController objTabMenuController;
  final CUser objLoginAcc;

  ContactPage({Key key,
      @required this.objTabMenuController, 
      @required this.objLoginAcc
  }) : super(key: key);

  @override
  _ContactPageState createState() => new _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  bool _isFiltered = false;
  List<CContact> _searchResult = [];
  List<CContact> _contactList = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  List<String> tableHeader = ['Site Spesific', 'Company', 'Contact Name','Ekstensi','Phone','Email'];
  List<TextEditingController> TxtController = [];
  List<FocusNode> TxtNode = [];


  Future<void> loadContact(CSite objSite) async{
    final svcContact  contactService = svcContact();
    // load service log
    await contactService.getListContactBySite(objSite.siteCode).then((objResult) {
      if (objResult != null) {
        setState(() {
          _contactList = objResult;
        });
        doFilter();
      }
    }).catchError((e) {
      print("Error Load Contact List -> " + e.toString());
    });
  }
  Future<void> deleteContact(CContact objContact) async{
    final svcContact contactService = svcContact();
    // save data contact using service
    contactService.deleteContact(objContact.contactId.toString()).then((objResult) {
      if (objResult != null) {
        doRefresh();
      }
    }).catchError((e) {
      print("Error Delete Contact -> " + e.toString());
    });
  }

  Widget separator(BuildContext context){
    return Column(
      children: [
        SizedBox(height: 8),
        Container(height: 8, color: Colors.grey[200]),
        SizedBox(height: 8),
      ],
    );
  }
  Widget Lbl_Title(BuildContext context) {
    return RichText(
      textAlign: TextAlign.start,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 26.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: 'Contact List')
        ],
      ),
    );
  }

  Widget Cmd_Refresh(BuildContext context) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.refresh, color: Colors.blue),
        tooltip: 'Refresh',
        onPressed: (){
          doRefresh();
          /*
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => super.widget));
          */
        },
      ),
    );
  }
  Widget Cmd_ClearAllFilter(BuildContext context){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.tune),
        tooltip: 'Filter Clear',
        onPressed: (){
          doClearFilter();
        },
      ),
    );
  }
  Widget Cmd_Add(BuildContext context, CUser objLoginAcc) {
    return Transform.scale(
      scale: 1,
      child: IconButton(
        icon: Icon(Icons.person_add, color: Colors.blue),
        tooltip: 'Add Contact',
        onPressed: (){
          _buildMessageDialog(context, null, objLoginAcc).then((void value) => doRefresh());
        },
      ),
    );
  }
  Widget Cmd_Edit(BuildContext context, CContact objContact, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.edit),
        tooltip: 'Edit',
        onPressed: (){
          _buildMessageDialog(context, objContact, objLoginAcc).then((void value) => doRefresh());
        },
      ),
    );
  }
  Widget Cmd_Delete(BuildContext context, CContact objContact, CUser objLoginAcc){
    return Transform.scale(
      scale: 0.75,
      child: IconButton(
        icon: Icon(Icons.delete),
        tooltip: 'Delete',
        onPressed: (){
          deleteContact(objContact);
        },
      ),
    );
  }

  Future<void> _buildMessageDialog(BuildContext context, CContact objContact, CUser objLoginAcc){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                  child: Container(
                  margin: const EdgeInsets.all(10.0),
                  //color: Colors.transparent,
                  width: 500.0,
                  height: double.infinity,
                  child: ContactFormPage(
                            objContact: objContact,
                            objLoginAcc: objLoginAcc
                        ),
                  ),
              )
          );
        });
  }

  Widget _buildTitleContact(BuildContext context, CUser objLoginAcc){
    return  Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Row(
                      children: [
                        Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                                //Lbl_Title(context),
                                BadgeInfo(objSite: objLoginAcc.objSite),
                            ]
                        ),
                      ],
                    ),
                    Row(
                      children: [
                          Cmd_Add(context, objLoginAcc),
                          SizedBox(width: 10.0),
                          Cmd_Refresh(context)
                      ],
                    )
                ],
              )
          ],
    );
  }

  onSearchTextChanged(int index, String str) async {
    doFilter();
    setState(() {});
  }

  doFilter(){
    setState(() {
      _searchResult.clear();
      _searchResult = _contactList
          .where((CContact) =>
      (TxtController[0].text.isEmpty ? true : CContact.siteSpecific.toUpperCase().contains(TxtController[0].value.text.toUpperCase())) &&
          (TxtController[1].text.isEmpty ? true : CContact.tenantCompany.toUpperCase().contains(TxtController[1].value.text.toUpperCase())) &&
          (TxtController[2].text.isEmpty ? true : CContact.contactName.toUpperCase().contains(TxtController[2].value.text.toUpperCase())) &&
          (TxtController[3].text.isEmpty ? true : CContact.contactExt.toUpperCase().contains(TxtController[3].value.text.toUpperCase())) &&
          (TxtController[4].text.isEmpty ? true : CContact.contactPhone.toUpperCase().contains(TxtController[4].value.text.toUpperCase())) &&
          (TxtController[5].text.isEmpty ? true : CContact.contactEmail.toUpperCase().contains(TxtController[5].value.text.toUpperCase()))
      ).toList();

    });
  }

  doRefresh(){
    loadContact(widget.objLoginAcc.objSite);
    setState(() {});
  }

  doSort(){
    setState(() {
      _contactList.sort((a, b) {
        return b.contactId.compareTo(a.contactId);
      });
    });
  }

  doClearFilter(){
    TxtController.forEach((txtController) {
      txtController.clear();
    });
    doFilter();
  }

  bool isFiltered(){
    int found = 0;
    TxtController.forEach((txtController) {
      if(txtController.text.isNotEmpty) {
        found = found +1;
      }
    });
    if(found > 0){
      return true;
    }
    else{
      return false;
    }
  }

  Widget _buildTxtSearch(BuildContext context, int index){
    return  Transform.translate(
        offset: Offset(-5, 0), //Offset(-16, 0),
        child: Transform.scale(
            scale: 0.9, //0.8,
            child:   TextFormField(
                controller: TxtController[index],
                focusNode:  TxtNode[index],
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: tableHeader[index],
                    hintText: tableHeader[index],
                    isDense: true,
                    contentPadding: EdgeInsets.all(5),
                    suffixIcon: Transform.translate(
                    offset: Offset(0,0),
                    child:Transform.scale(scale: 0.75,
                        child: IconButton(
                            icon: Icon(Icons.cancel),
                            tooltip: 'Clear',
                            onPressed: () {
                                TxtController[index].clear();
                                onSearchTextChanged(index, '');
                            }),
                        )
                    )
              ),
              style: TextStyle(fontSize: 12.0, height: 2.0, color: Colors.black),
              onChanged: (string) {
                  onSearchTextChanged(index, string);
              },
              onFieldSubmitted: (_) {
                if(index+1 == tableHeader.length) {
                  FocusScope.of(context).requestFocus(TxtNode[0]);
                }
                else{
                  FocusScope.of(context).requestFocus(TxtNode[index+1]);
                }
              },
            ),
      ),
    );
  }

  Widget _buildRowHeaderContact(BuildContext context){
    return  Padding(
        padding: EdgeInsets.only(top: 2.5),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(flex: 1, child: Cmd_ClearAllFilter(context)),
                      Expanded(flex: 3, child: _buildTxtSearch(context,0)),
                      Expanded(flex: 3, child: _buildTxtSearch(context,1)),
                      Expanded(flex: 7, child: _buildTxtSearch(context,2)),
                      Expanded(flex: 3, child: _buildTxtSearch(context,3)),
                      Expanded(flex: 3, child: _buildTxtSearch(context,4)),
                      Expanded(flex: 3, child: _buildTxtSearch(context,5)),
                      Expanded(flex: 3, child: Text("Action", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0,))),
                    ]
                ),
              ]
          )
    );
  }

  Widget _buildRowListContact(BuildContext context, CContact objContact, CUser objLoginAcc){
    return  Padding(
              padding: EdgeInsets.only(bottom: 0.5, top: 0.5),
              child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(flex: 1, child: Text(objContact.contactId.toString(),
                                 style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                        Expanded(flex: 3, child: Text(objContact.siteSpecific == null ? '-'  : objContact.siteSpecific,
                                 style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                        Expanded(flex: 3, child: Text(objContact.tenantCompany == null ? '-' : objContact.tenantCompany,
                                 style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                        Expanded(flex: 7, child: Text(objContact.contactName == null ? '-' : objContact.contactName,
                                 style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0, decoration: TextDecoration.none))),
                        Expanded(flex: 3, child: Text(objContact.contactExt == null ? '-' : objContact.contactExt,
                                 style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                        Expanded(flex: 3, child: Text(objContact.contactPhone == null ? '-' : objContact.contactPhone,
                                 style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                        Expanded(flex: 3, child: Text(objContact.contactEmail == null ? '-' : objContact.contactEmail,
                                 style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,))),
                        Expanded(flex: 3, child: Row(
                          children: [
                            Cmd_Edit(context, objContact, objLoginAcc),
                            SizedBox(width: 15.0),
                            Cmd_Delete(context, objContact, objLoginAcc)
                          ],
                        ))
                      ]
                    ),
                    Divider(),
                  ],
                )
           );
  }

  Widget _buildListViewContact(BuildContext context, CUser objLoginAcc){
    return Expanded(
      //child: _searchResult.length != 0 || controller.text.isNotEmpty ?
      child: _searchResult.length != 0 || isFiltered() ?
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _searchResult.length,
        itemBuilder: (context, i) {
          return _buildRowListContact(context, _searchResult[i], objLoginAcc);
        },
      ) :
      ListView.builder(
        padding: EdgeInsets.only(top: 10.0),
        scrollDirection: Axis.vertical,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: _contactList.length,
        itemBuilder: (context, index) {
          return _buildRowListContact(context, _contactList[index], objLoginAcc);
        },
      ),
    );
  }

  Widget buildList(BuildContext context, CUser objLoginAcc) {
    return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                  _buildTitleContact(context, objLoginAcc),
                  separator(context),
                  _buildRowHeaderContact(context),
                  separator(context),
                  _buildListViewContact(context, objLoginAcc)
            ],
    );
  }


  @override
  void initState() {
    super.initState();
    // init txtController
    TxtController = List.generate(tableHeader.length, (i) => TextEditingController());
    // init focusNode
    TxtNode = List.generate(tableHeader.length, (i) => FocusNode());
    loadContact(widget.objLoginAcc.objSite);
  }

  @override
  void dispose() {
    // dispose txtController
    TxtController.forEach((c) => c.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(10.0),
          color: Colors.transparent,
          height: double.infinity,
          child: buildList(context, widget.objLoginAcc),
        ),
      ),
    );
  }
}