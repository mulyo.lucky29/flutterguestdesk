import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:ui';
import 'dart:html';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:myggguestdesk/utils/color_constants.dart';
import 'package:myggguestdesk/model/CMenu.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CApplication.dart';
import 'package:myggguestdesk/screen/contact/ContactPage.dart';
import 'package:myggguestdesk/screen/visit/VisitPage.dart';
import 'package:myggguestdesk/screen/site/SitePage.dart';
import 'package:myggguestdesk/screen/user/UserPage.dart';
import 'package:myggguestdesk/screen/user/UserFormPage.dart';
import 'package:myggguestdesk/screen/queform/QuePage.dart';
import 'package:myggguestdesk/screen/expired/ExpiredPage.dart';
import 'package:myggguestdesk/screen/visitor/VisitorPage.dart';
import 'package:myggguestdesk/screen/key/KeyFormPage.dart';

import 'package:myggguestdesk/tools/sessionClear.dart';
import 'package:myggguestdesk/tools/generatePSK.dart';
import 'package:giffy_dialog/giffy_dialog.dart';


class HomePage extends StatefulWidget {
  final CUser objLoginAcc;

  HomePage({Key key,
    @required this.objLoginAcc}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  final generatePSK generatorKey = generatePSK();
  final sessionClear xSession = sessionClear();

  TabController tabController;
  int active = 0;
  CSite objCSite;
  Future myFutureSite;
  List<CMenu> _menu;
  List<CMenu> _popupmenu;
  Future<CApplication> _futureAppSetting;

  /*
  Future <CSite> get_site_profile(BuildContext context, String siteId) async{
    print("get_site_profile ->" + siteId);

    CSite result;
    svcSite siteService = new svcSite();
    await siteService.getSiteInfoById(siteId)
        .then((CSite objResult) {
            if (objResult.siteId > 0) {
              setState(() {
                objCSite = objResult;
                result = objResult;
              });
            }
        });
    return result;
  }
  */

  Future <CApplication> do_read_session() async{
    CApplication result;

    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      try{
        if(preferences.getString("appsmobileurl") != null) {
          result = CApplication(
              appsname: preferences.getString("appsname"),
              appsdescription: preferences.getString("appsdescription"),
              appversion: preferences.getString("appversion"),
              apiversion: preferences.getString("apiversion"),
              appscredit: preferences.getString("appscredit"),
              appsmobileurl: preferences.getString("appsmobileurl"),
              appsstatus: preferences.getString("appsstatus")
          );
        }
        else{
          print('No Session Saved');
        }
      }
      catch(exception){
      }
    });
    return result;
  }


  Future <void> do_clear_session() async{
    print('do_clear_session');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear().then((value){

      // clear cache first
      xSession.deleteCacheDir();
      xSession.deleteAppDir();

      // move to login page again
      Navigator.pop(context);
    });
  }

  List<CMenu> _initMenu(){
    List<CMenu> _menu;
    _menu = [
      CMenu(
          menuNum: 0,
          menuName: 'Visit',
          menuIcon:  Icon(Icons.dashboard, color: Colors.blue),
          onlyAdmin: false
      ),
      CMenu(
          menuNum: 1,
          menuName: 'Contact',
          menuIcon:  Icon(Icons.person, color: Colors.blue),
          onlyAdmin: false
      ),
      CMenu(
          menuNum: 2,
          menuName: 'Site',
          menuIcon: Icon(Icons.location_on, color: Colors.blue),
          onlyAdmin: true
      ),
      CMenu(
          menuNum: 3,
          menuName: 'User',
          menuIcon: Icon(Icons.verified_user, color: Colors.blue),
          onlyAdmin: true
      ),
      CMenu(
          menuNum: 4,
          menuName: 'Form',
          menuIcon: Icon(Icons.app_registration, color: Colors.blue),
          onlyAdmin: true
      ),
      CMenu(
          menuNum: 5,
          menuName: 'Expired',
          menuIcon:  Icon(Icons.access_time, color: Colors.blue),
          onlyAdmin: false
      ),
      CMenu(
          menuNum: 6,
          menuName: 'Guest',
          menuIcon:  Icon(Icons.person_search, color: Colors.blue),
          onlyAdmin: false
      ),

    ];

    return _menu;
  }

  List<CMenu> _initPopUpMenu(){
    List<CMenu> _popupmenu;
    _popupmenu = [
        CMenu(
            menuNum: 1,
            menuName: 'My Profile',
            menuIcon:  Icon(Icons.account_circle, color: Colors.black, size: 22),
            onlyAdmin: false
        ),
        CMenu(
            menuNum: 2,
            menuName: 'Generate Key',
            menuIcon: Icon(Icons.vpn_key, color: Colors.black, size: 22),
            onlyAdmin: false
        ),
        CMenu(
            menuNum: 3,
            menuName: 'About',
            menuIcon: Icon(Icons.info, color: Colors.black, size: 22),
            onlyAdmin: false
        ),
        CMenu(
            menuNum: 4,
            menuName: 'Logout',
            menuIcon: Icon(Icons.logout, color: Colors.black, size: 22),
            onlyAdmin: false
        ),
      ];
    return _popupmenu;
  }

  //List<String> popupMenu = ['My Profile', 'Generated Key', 'About','Log Out'];

  Widget _buildPopupMenuItem(BuildContext context, int menuIdx, Icon menuIcon, String menuTitle){
    return  PopupMenuItem(
      child: new Container(
        width: 200.0,
        child:  Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            menuIcon, //Icon(Icons.account_circle, color: Colors.black, size: 22),
            SizedBox(width: 4),
            Text(menuTitle,
              style: TextStyle(
                fontSize: 12,
                color: Colors.black,
                fontFamily: 'HelveticaNeue',
              ),
            ),
          ],
        ),
      ),
      value: menuIdx, //1,
    );
  }


  Future<void> _showAboutDialog(BuildContext context, CApplication objApp){
    return showDialog(
                context: context,builder: (_) =>
                    AssetGiffyDialog(
                        title:
                        //Text('GG GuestDesk', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0,)),
                        Text(objApp.appsname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0,)),
                        description:
                        /*
                        Text('A Visitor Management Console for GG GuestLogBook\n\n\n ' +
                                          'Build Version 1.0.0\n\n' +
                                          'API Version 1.0.0\n\n' +
                                          'Gudang Garam Tbk 2021 ',
                            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,)),
                         */
                        Text(objApp.appsdescription +'\n\n\n ' +
                            'Build Version ' +  objApp.appversion + '\n\n' +
                            'API Version ' +  objApp.apiversion + '\n\n' +
                            objApp.appscredit,
                            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0,)),
                        image: Image.asset('assets/images/landing.png'),
                        onlyOkButton: true,
                        onOkButtonPressed: () {
                          Navigator.of(context).pop();
                        },
                      ));
  }

  Future<void> _buildMessageDialog(BuildContext context, CUser objLoginAcc, Widget objWidget){
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        context: context,
        builder: (context) {
          return  Padding(
              padding: EdgeInsets.all(15.0),
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  //color: Colors.transparent,
                  width: 500.0,
                  height: double.infinity,
                  child: objWidget // UserFormPage(objUser: objLoginAcc, objLoginAcc: objLoginAcc),
                ),
              )
          );
        });
  }

  Widget getMenuPopup (BuildContext context, List<CMenu> objListMenu, CApplication objApp, CUser objLoginAcc) {
    return PopupMenuButton(
        //color: Colors.grey,
        elevation: 0.0,
        shape: RoundedRectangleBorder(
            side: BorderSide(
              width: 0.5,
              color: Colors.black,
            )
        ),
        //icon: Icon(Icons.account_circle),
        child: Row(
          children: [
            Icon(Icons.account_circle),
            SizedBox(width: 10),
            Text(objLoginAcc.username),
            SizedBox(width: 100),
          ],
        ),
        itemBuilder: (context) => getPopupMenu(context, objListMenu, objLoginAcc),
        onSelected: (result) => onClickPopupMenu(context, result, objApp, objLoginAcc)
    );
  }

  onClickPopupMenu(BuildContext context, int Index, CApplication objApp, CUser objLoginAcc){
    if (Index == 1) {
      print("myprofile");
      _buildMessageDialog(context, objLoginAcc, UserFormPage(objUser: objLoginAcc, objLoginAcc: objLoginAcc));
    }
    else if(Index == 2) {
      print("generate key");
      //   final CSite objSite;
      //   final CUser objLoginAcc;

      _buildMessageDialog(context, objLoginAcc, KeyFormPage(objSite: objLoginAcc.objSite, objLoginAcc: objLoginAcc));

      /*
      _buildMessageDialog(context, objLoginAcc, Column(
        children: <Widget>[
          Icon(Icons.vpn_key, size: 16, color: Colors.white),
          SizedBox(height: 5.0),
          InkWell(
              child: Text(generatorKey.generate(null, objLoginAcc.objSite),
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.yellow,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'HelveticaNeue', //'Raleway',
                  )),
              onTap: () {
                Clipboard.setData(ClipboardData(text: generatorKey.generate(null, objLoginAcc.objSite)));
              }
          ),
        ],
      ));
      */
    }
    else if(Index == 3) {
      print("about");
      _showAboutDialog(context, objApp);
    }
    else if(Index == 4){
      print("logout");
      do_clear_session();
    }
  }

  List<PopupMenuEntry<int>> getPopupMenu(BuildContext context, List<CMenu> objListMenu, CUser objLoginAcc){
    List<PopupMenuEntry<int>> popupMenu;
      popupMenu = [
          getPopupMenuItem(context, objListMenu[0] , objLoginAcc),
          getPopupMenuItem(context, objListMenu[1] , objLoginAcc),
          getPopupMenuItem(context, objListMenu[2] , objLoginAcc),
          getPopupMenuItem(context, objListMenu[3] , objLoginAcc),
      ];
    return popupMenu;
  }

  Widget getPopupMenuItem(BuildContext context, CMenu objMenu, CUser objLoginAcc){
    return  PopupMenuItem(
      child: new Container(
        width: 200.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            objMenu.menuIcon,
            SizedBox(width: 4),
            Text(objMenu.menuName,
              style: TextStyle(
                fontSize: 12,
                color: Colors.black,
                fontFamily: 'HelveticaNeue',
              ),
            ),
          ],
        ),
      ),
      value: objMenu.menuNum //..3,
    );
  }

  Widget getMenu(bool drawerStatus, List<CMenu> objListMenu, CUser objLoginAcc) {
    return ListView(
      children: <Widget>[
          getMenuItem(context, objListMenu[0],drawerStatus, objLoginAcc),
          getMenuItem(context, objListMenu[1],drawerStatus, objLoginAcc),
          getMenuItem(context, objListMenu[2],drawerStatus, objLoginAcc),
          getMenuItem(context, objListMenu[3],drawerStatus, objLoginAcc),
          getMenuItem(context, objListMenu[4],drawerStatus, objLoginAcc),
          getMenuItem(context, objListMenu[5],drawerStatus, objLoginAcc),
          getMenuItem(context, objListMenu[6],drawerStatus, objLoginAcc),
      ],
    );
  }

  Widget getMenuItem(BuildContext context, CMenu objMenu, bool status, CUser objLoginAcc){
    return  ((objMenu.onlyAdmin == true && objLoginAcc.isAdmin.toString() == 'Y') ||
            ((objMenu.onlyAdmin == false && objLoginAcc.isAdmin != null))) ?
      Tooltip(message: objMenu.menuName,
          child: FlatButton(
                      color: tabController.index == objMenu.menuNum ? Colors.grey[100] : Colors.white,
                      onPressed: () {
                          tabController.animateTo(objMenu.menuNum);
                          status ? Navigator.pop(context) : print("");
                          },
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                              color: Colors.transparent,
                              padding: EdgeInsets.only(left: 10, top: 20, bottom: 20, right: 10),
                              child: objMenu.menuIcon,
                            // Icons.dashboard, color: Colors.blue), //IconData(57399, fontFamily: 'MaterialIcons') //objMenu.menuIcon,
                          )
                      ),
          )
    )
    : Column();
  }
  Widget getAppBar(BuildContext context, List<CMenu> objListMenu, CApplication objApps, CUser objLoginAcc) {
    return AppBar(
      automaticallyImplyLeading:
      MediaQuery.of(context).size.width < 1300 ? true : false,
      title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 32),
              child: Text("GG Guest Desk",
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'HelveticaNeue',
                ),
              ),
            ),
          ]
      ),
      actions: <Widget>[
        getMenuPopup(context, objListMenu, objApps, objLoginAcc),
      ],
      backgroundColor: ColorConstants.blue,
    );
  }
  Widget getDrawer(BuildContext context, List<CMenu> objListMenu,CUser objLoginAcc){
    return Padding(
        padding: EdgeInsets.only(top: 55),
        child: Drawer(
            child: getMenu(true, objListMenu, objLoginAcc)
        )
    );
  }
  Widget getBody(BuildContext context, List<CMenu> objListMenu, CUser objLoginAcc){
    double _mnubarSize = 60; // 200
    return Row(
      children: <Widget>[
        MediaQuery.of(context).size.width < 1300
            ? Container(color: Colors.black,)
            : Card(
          elevation: 2.0,
          child: Container(
              margin: EdgeInsets.all(0),
              height: MediaQuery.of(context).size.height,
              width: _mnubarSize, //200,
              color: Colors.white,
              child: getMenu(false, objListMenu, objLoginAcc)
          ),
        ),
        Container(
          width:
          MediaQuery.of(context).size.width < 1300 ?
          MediaQuery.of(context).size.width
              :
          MediaQuery.of(context).size.width - (_mnubarSize + 11),
          child: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: tabController,
            children: [
              VisitPage(objTabMenuController: tabController, objLoginAcc: objLoginAcc),
              ContactPage(objTabMenuController: tabController, objLoginAcc: objLoginAcc),
              SitePage(objTabMenuController: tabController, objLoginAcc: objLoginAcc),
              UserPage(objTabMenuController: tabController, objLoginAcc: objLoginAcc),
              QuePage(objTabMenuController: tabController, objLoginAcc: objLoginAcc),
              ExpiredPage(objTabMenuController: tabController, objLoginAcc: objLoginAcc),
              VisitorPage(objTabMenuController: tabController, objLoginAcc: objLoginAcc),
            ],
          ),
        )
      ],
    );
  }


  @override
  void initState() {
    super.initState();
    //setState(() {
    //  myFutureSite = get_site_profile(context, widget.objUser.objSite.siteId.toString());
    //});
    //document.documentElement.requestFullscreen();
    _futureAppSetting = do_read_session();

    _menu       = _initMenu();
    _popupmenu  = _initPopUpMenu();

    tabController = TabController(
        vsync: this,
        length: _menu.length, //5,
        initialIndex: 0)
      ..addListener(() {
        setState(() {
          active = tabController.index;
        });
      });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
         FutureBuilder(
        future: _futureAppSetting,
        builder: (ctx,  snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          else if(snapshot.connectionState == ConnectionState.done){
            print('snapshot: $snapshot');
            if (snapshot.hasData) {
              print('C');
              return Scaffold(
                  appBar: getAppBar(context, _popupmenu , snapshot.data, widget.objLoginAcc),
                  drawer: getDrawer(context, _menu,widget.objLoginAcc),
                  body: getBody(context, _menu, widget.objLoginAcc)
              );
            }
            else{
              print('D');
              return Text('No Data');
            }
          }
          else {
            print('E');
            return Center(child: CircularProgressIndicator() );
          }
        });
  }
}
