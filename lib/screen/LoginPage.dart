import 'package:flutter/material.dart';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:myggguestdesk/screen/HomePage.dart';
import 'package:myggguestdesk/utils/color_constants.dart';
import 'package:myggguestdesk/validation/valLogin.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CApplication.dart';
import 'package:myggguestdesk/model/CUserLoginReq.dart';
import 'package:myggguestdesk/service/svcUser.dart';
import 'package:myggguestdesk/service/svcApplication.dart';
import 'package:myggguestdesk/tools/sessionClear.dart';
import 'package:achievement_view/achievement_view.dart';

class LoginPage extends StatefulWidget {
  final String title;

  const LoginPage({Key key, this.title}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with valLogin{
  bool isChecked = false;
  bool isLoading = false;
  Future<CApplication> futureAppSetting;
  final sessionClear xSession = sessionClear();

  final formLoginKey  = GlobalKey<FormState>(); // globalkey for validation
  CApplication objCApps;

  TextEditingController _ctrlUsername  = new TextEditingController();
  TextEditingController _ctrlPassword  = new TextEditingController();

  FocusNode NodeUsername           = FocusNode();
  FocusNode NodePassword           = FocusNode();
  FocusNode NodeCmdLogin           = FocusNode();

  String strUsername = '';
  String strPassword = '';
  bool _passwordVisible;

  Future<CApplication> loadAppSetting() async{
    CApplication result;

    final svcApplication  appService = svcApplication();
    await appService.getAppSetting().then((objResult) {
      if (objResult != null) {
          result = objResult;
      }
    }).catchError((e) {
      print("Error Load Appsetting -> " + e.toString());
    });
    return result;
  }

  Future <void> do_save_app_session(CApplication objApp) async{
    print('sessionPage :: save_app_ession');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString("appsname", objApp.appsname);
      preferences.setString("appsdescription", objApp.appsdescription);
      preferences.setString("appversion", objApp.appversion);
      preferences.setString("apiversion", objApp.apiversion);
      preferences.setString("appscredit", objApp.appscredit);
      preferences.setString("appsmobileurl", objApp.appsmobileurl);
      preferences.setString("appsstatus", objApp.appsstatus);
      preferences.commit();
    });
  }

  Future<CUser> doLogin(BuildContext context, CUserLoginReq objParam) async{
    print("doLogin");
    CUser result;
    svcUser userService = new svcUser();

    setState((){ isLoading=true; });
    await userService.getUserLogin(objParam).then((CUser objLoginAcc) {
            result = objLoginAcc;
            if(objLoginAcc != null){
              print("here");
              if(objLoginAcc.isActive == 'Y'){
                // write session

                // redirect to home
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomePage(objLoginAcc: objLoginAcc)),
                );
              }
              else{
                Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Your Account Has Been Disabled or Inactive");
              }
            }
            else{
              Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Login Failed");
            }
        }).catchError((e){
          Toast_Popup(context, Icon(Icons.error, color: Colors.white,), Colors.red, "Cannot Login Exception");
          //print("here exception -> " + e.toString());
    }).then((value) =>
        setState((){ isLoading=false; })
    );
    return result;
  }

  /*
  Future<void> _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();
    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  Future<void> _deleteAppDir() async {
    final appDir = await getApplicationSupportDirectory();
    if(appDir.existsSync()){
      appDir.deleteSync(recursive: true);
    }
  }
  */

  Widget Toast_Popup(BuildContext context, Icon pIcon, Color pColor, String strContent){
    AchievementView(
      context,
      title: "", //strTitle,
      icon: pIcon, // Icon(Icons.done, color: Colors.white,)
      color: pColor, //Colors.blue,
      alignment: Alignment.bottomCenter,
      subTitle: strContent,
      listener: (status) {
        print(status);
      },
    )..show();
  }

  void EH_Login(BuildContext context){
    if (formLoginKey.currentState.validate()) {
      formLoginKey.currentState.save();
      doLogin(context,
          CUserLoginReq(
              dskUserName: strUsername,
              dskPassword: strPassword
          ));
    }
  }

  Widget Lbl_Title(BuildContext context, CApplication objApp){
    return Center(
        child: Column(
            children: [
              SizedBox(height: 10),
              Image.asset('assets/images/landing.png',
                  width: 300,
                  height: 250,
                  fit:BoxFit.fill ),
              //Text("GG Guest Desk", style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
              Text(objApp.appsname, style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              //Text("A Visitor Management Console Application For GuestLogBook", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
              Text(objApp.appsdescription, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
            ],
        ));
  }

  Widget Lbl_Register(BuildContext context){
    return FlatButton(
      child: Text('Register', style: TextStyle(color: Colors.black54),),
      onPressed: () {},
    );
  }
  Widget Txt_Username(BuildContext context){
    return TextFormField(
      controller: _ctrlUsername,
      focusNode: NodeUsername,
      keyboardType: TextInputType.text,
      validator: validateUsername,
      onSaved: (String value) {
        strUsername = value;
      },
      autofocus: false,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.verified_user),
        labelText: 'Username',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        suffixIcon: IconButton(icon: Icon(Icons.cancel),
            tooltip: 'Clear',
            onPressed: () {
              _ctrlUsername.clear();
            }),
      ),
      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodePassword);
      },
    );
  }
  Widget Txt_Password(BuildContext context){
    return TextFormField(
      controller: _ctrlPassword,
      focusNode: NodePassword,
      keyboardType: TextInputType.text,
      validator: validatePassword,
      onSaved: (String value) {
        strPassword = value;
      },
      autofocus: false,
      obscureText: !_passwordVisible,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.password),
        hintText: 'Password',
        labelText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        suffixIcon: IconButton(
          icon: Icon(
            _passwordVisible
                ? Icons.visibility
                : Icons.visibility_off,
            color: Theme.of(context).primaryColorDark,
          ),
          onPressed: () {
            setState(() {
              _passwordVisible = !_passwordVisible;
            });
          },
        ),
      ),

      onFieldSubmitted: (_) {
        FocusScope.of(context).requestFocus(NodeCmdLogin);
      },
    );
  }

  Widget Cmd_Login(BuildContext context){
    return  Container(
        width: 300, //MediaQuery.of(context).size.width / 2.5,
        child: ! isLoading ? ElevatedButton(
          focusNode: NodeCmdLogin,
          onPressed: () async {
              await EH_Login(context);
          },
          child: Text('Sign In',
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontWeight: FontWeight.w500)),
        ) :
        Center(child:CircularProgressIndicator())
    );
  }

  Widget form_login(BuildContext context, CApplication objApp){
    return  Form(
        key: formLoginKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 62.0),
            Lbl_Title(context, objApp),
            SizedBox(height: 48.0),
            Text("Login", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            SizedBox(height: 24.0),
            Txt_Username(context),
            SizedBox(height: 15.0),
            Txt_Password(context),
            SizedBox(height: 24.0),
            Cmd_Login(context),
            SizedBox(height: 40),
            //Text("App version " + appversion, style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500)),
            Text("App version " + objApp.appversion, style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500)),
            SizedBox(height: 5),
            //Text("Powered By IT Directorate 2022 © Gudang Garam Tbk - LM", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500)),
            Text(objApp.appscredit, style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500)),
          ],
        )
    );
  }

  @override
  void initState() {
    _passwordVisible = false;

    // clear cache first
    xSession.deleteCacheDir();
    xSession.deleteAppDir();

    futureAppSetting = loadAppSetting();
    super.initState();
  }

  @override
  void dispose() {
    _ctrlUsername.dispose();
    _ctrlPassword.dispose();
    super.dispose();
  }

  Widget getBody(BuildContext context, CApplication objApp){
    // save session
    do_save_app_session(objApp);
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height / 2,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
            ),
            Container(
              height: MediaQuery.of(context).size.height / 2,
              width: MediaQuery.of(context).size.width,
              color: ColorConstants.blue,
            ),
          ],
        ),
        Center(
          child: Card(
            elevation: 2.0,
            child: Container(
              margin: EdgeInsets.all(42),
              width: 500.0,
              height: double.infinity,
              child: Column(
                children: <Widget>[
                  form_login(context, objApp),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body:  FutureBuilder(
          future: futureAppSetting,
          builder: (ctx,  snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            }
            else if(snapshot.connectionState == ConnectionState.done){
              print('snapshot: $snapshot');
              if (snapshot.hasData) {
                print('C');
                return getBody(context, snapshot.data);
              }
              else{
                print('D');
                return Text('No Data');
              }
            }
            else {
              print('E');
              return Center(child: CircularProgressIndicator() );
            }
          })
    );
  }
}
