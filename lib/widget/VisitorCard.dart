import 'dart:async';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:myggguestdesk/model/CVisit.dart';
import 'package:qr_flutter/qr_flutter.dart';

class VisitorCard extends StatefulWidget {
  final CVisit objVisit;

  VisitorCard({Key key, @required this.objVisit}) : super(key: key);

  @override
  _VisitCardState createState() => new _VisitCardState();
}

class _VisitCardState extends State<VisitorCard> {

  Widget Image_QR(String value) {
    return Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Center(
          child: QrImage(
            data: value,
            version: QrVersions.auto,
            size: 98.0,
            gapless: false,
          )
      ),
    );
  }

  Widget _buildCard(BuildContext context, CVisit objVisit) {
  //Card_visitor(BuildContext context, CSite objSite, CGuest objGuest, CVisit objVisit) {
    return Card(
      elevation: 4.0,
      color: Colors.black ,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14),
      ),
      child: Container(
          height: 200,
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0, top: 20.0),
          child: Row(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('Visitor Card',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontFamily: 'CourrierPrime',
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      Text('PT. Gudang Garam Tbk',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: 'CourrierPrime'),
                      ),
                      Text(objVisit.objSite.siteName,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: 'CourrierPrime'),
                      ),
                      SizedBox(height: 40.0),
                      Text(objVisit.objGuest.guestName,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: 'CourrierPrime',
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      Text(objVisit.objGuest.guestCompany,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'CourrierPrime'),
                      ),
                      Text(objVisit.objGuest.guestPhone,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'CourrierPrime'),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10.0),
                Expanded(
                  flex: 3,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text('Scan QR for check-in/out', //objGuest.guestPhone,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              fontFamily: 'CourrierPrime'),
                        ),
                        SizedBox(height: 2.0),
                        Image_QR(objVisit.visitSessionCode),
                        SizedBox(height: 2.0),
                        Column(
                            children: [
                              Text(DateFormat('dd-MMM-yyyy').format(DateTime.parse(objVisit.visitDate)), //objGuest.guestPhone,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontFamily: 'CourrierPrime'),
                              ),
                            ]
                        ),
                        SizedBox(height: 5.0),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(objVisit.visitSessionCode, //objGuest.guestPhone,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontFamily: 'CourrierPrime'),
                              ),
                            ]
                        ),
                      ]
                  ),
                )
                //objVisirReq.visitId.toString()),
              ]
          )
      ), // end of container
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
      return _buildCard(context, widget.objVisit);
  }
}