import 'dart:ui';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:myggguestdesk/tools/generatePSK.dart';
import 'package:myggguestdesk/model/CSite.dart';


class BadgeInfo extends StatefulWidget {
  final CSite objSite;

  BadgeInfo({Key key, @required this.objSite}) : super(key: key);

  @override
  _BadgeInfo createState() => new _BadgeInfo();
}

class _BadgeInfo extends State<BadgeInfo> {
  final generatePSK generatorKey = generatePSK();

  String getCurrentDate(String strFormat){
    var now = DateTime.now();
    return DateFormat(strFormat).format(now);
  }

  Widget Authent_Key(CSite objCSite){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //crossAxisAlignment: CrossAxisAlignment.end,
      //mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(Icons.vpn_key, size: 16, color: Colors.white),
        SizedBox(height: 5.0),
        InkWell(
            child: Text(generatorKey.generate(null, objCSite),
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.yellow,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'HelveticaNeue', //'Raleway',
                      )),
            onTap: () {
              Clipboard.setData(ClipboardData(text: generatorKey.generate(null, objCSite)));
            }
        ),
      ],
    );
  }

  Widget _buildCardInfo(BuildContext context, CSite objCSite){
    return Card(
      elevation: 2,
      child: Container(
        padding: EdgeInsets.all(10),
        color: Colors.blue,
        width: MediaQuery.of(context).size.width < 1300 ?
               MediaQuery.of(context).size.width - 400
                :
               MediaQuery.of(context).size.width / 4.5,
        height: 60, //MediaQuery.of(context).size.height / 6,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(Icons.my_location, size: 16, color: Colors.white),
                  SizedBox(height: 5.0),
                  Text(objCSite.siteName, // "GG Tower",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Raleway',
                    ),
                  ),
                ]
            ),
            SizedBox(height: 5),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(getCurrentDate('EEEE, d MMM yyyy'),
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Raleway',
                  ),
                ),
              ],
            ),
            SizedBox(height: 5),
            Authent_Key(objCSite),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _buildCardInfo(context, widget.objSite);
  }
}