import 'package:hashids2/hashids2.dart';
import 'package:libcalendar/libcalendar.dart';
import 'package:myggguestdesk/model/CSite.dart';

class generatePSK {

  String generate(DateTime tgl, CSite objSite){
    String sharedKey;
      print('generatePSK');
      //var now = DateTime.now();
      var tanggal = tgl == null ? DateTime.now() : tgl;
      final hashids = HashIds();
      final int year = tanggal.year ;
      final int month = tanggal.month ;
      final int day = tanggal.day;

      final String seed = fromGregorianToCJDN(year, month, day).toString() + objSite.siteId.toString();
      sharedKey = hashids.encode(seed);

      return sharedKey;
  }

}