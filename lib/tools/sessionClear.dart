import 'package:path_provider/path_provider.dart';


class sessionClear {

  Future<void> deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();
    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  Future<void> deleteAppDir() async {
    final appDir = await getApplicationSupportDirectory();
    if(appDir.existsSync()){
      appDir.deleteSync(recursive: true);
    }
  }

}