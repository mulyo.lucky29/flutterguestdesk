import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:myggguestdesk/model/CSite.dart';
import 'package:myggguestdesk/model/CSiteSaveReq.dart';

class svcSite extends ChangeNotifier {

  Future<CSite> getSiteInfoById(String p_siteId) async {
    final String url = baseUrl + '/site/getSiteById/' + p_siteId;
    CSite result;
    print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
        print(response.body.toString());
        result = CSite.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }

  Future<CSite> getSiteInfoByCode(String p_siteCode) async {
    final String url = baseUrl + '/site/getSiteByCode/' + p_siteCode;
    CSite result;
    print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
        print(response.body.toString());
        result = CSite.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }

  Future<List<CSite>> getSiteList() async {
    List<CSite> result = [];
    final String url = baseUrl + '/site/getListSite';
    print(url);

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CSite.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CSite> saveSite(CSiteSaveReq param) async {
    CSite result;
    final url = baseUrl + '/site/saveSite';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("Save User -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CSite.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }


}