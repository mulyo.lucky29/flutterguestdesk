import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:myggguestdesk/model/CCheckLog.dart';
import 'package:myggguestdesk/model/CCheckLogReq.dart';

class svcCheckLog extends ChangeNotifier {

  Future<CCheckLog> setDoCheckLog(CCheckLogReq param) async {
    CCheckLog result;
    final url = baseUrl + '/checklog/setDoCheckLog';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("setDoCheckLog -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CCheckLog.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<List<CCheckLog>> getListCheckLogByVisitId(String visitId) async {
    final url = baseUrl + '/checklog/getListCheckLogByVisitId/' + visitId;
    List<CCheckLog> result = [];
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map areaPdu in responseJson) {
          result.add(CCheckLog.fromJson(areaPdu));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

}