
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:myggguestdesk/model/CQuestionaireH.dart';
import 'package:myggguestdesk/model/CQuestionaireHSaveReq.dart';

class svcQuestionaireH extends ChangeNotifier {

  Future<CQuestionaireH> getQuestionaireById(String p_queid) async {
    final String url = baseUrl + '/questionaireH/getQuestionaire/' + p_queid;
    CQuestionaireH result;
    print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
        print(response.body.toString());
        result = CQuestionaireH.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }

  Future<List<CQuestionaireH>> getListForm() async {
    List<CQuestionaireH> result = [];
    final String url = baseUrl + '/questionaireH/getListQuestionaire';
    print(url);

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CQuestionaireH.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CQuestionaireH> saveQuehForm(CQuestionaireHSaveReq param) async {
    CQuestionaireH result;
    final url = baseUrl + '/questionaireH/saveForm';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("setRegisterGuest -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CQuestionaireH.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CQuestionaireH> deleteQuehForm(String strQuehId) async {
    CQuestionaireH result;
    final url = baseUrl + '/questionaireH/delete/' + strQuehId;

    try {
      final response = await http.delete(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Accept": "application/json"
        },);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CQuestionaireH.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }



}