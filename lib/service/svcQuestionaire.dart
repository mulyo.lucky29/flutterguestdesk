import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:myggguestdesk/model/CQuestionaire.dart';
import 'package:myggguestdesk/model/CQuestionaireSaveReq.dart';

class svcQuestionaire extends ChangeNotifier {

  Future<List<CQuestionaire>> getListQuestionaireById(String p_queId) async {
    List<CQuestionaire> result = [];
    final String url = baseUrl + '/questionaire/getListQuestionaireById/' + p_queId;
    print(url);

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CQuestionaire.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CQuestionaire> saveQuePoint(CQuestionaireSaveReq param) async {
    CQuestionaire result;
    final url = baseUrl + '/questionaire/savePoint';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("setRegisterGuest -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CQuestionaire.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CQuestionaire> deleteQuePoint(String strQuePointId) async {
    CQuestionaire result;
    final url = baseUrl + '/questionaire/delete/' + strQuePointId;

    try {
      final response = await http.delete(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Accept": "application/json"
        },);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CQuestionaire.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

}