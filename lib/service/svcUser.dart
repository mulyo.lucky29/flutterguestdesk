import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:myggguestdesk/model/CUser.dart';
import 'package:myggguestdesk/model/CUserLoginReq.dart';
import 'package:myggguestdesk/model/CUserSaveReq.dart';
import 'package:myggguestdesk/model/CUserResetReq.dart';

class svcUser extends ChangeNotifier {

  Future<CUser> getUserProfile(String p_userId) async {
    final String url = baseUrl + '/deskuser/login/profile/' + p_userId;
    CUser result;
    print(url);
    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });

      if (response.statusCode == 200) {
        final responseJson = json.decode(response.body);
        print(response.body.toString());
        result = CUser.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch (err) {
      result = null;
      throw err;
    }
    return result;
  }

  Future<CUser> getUserLogin(CUserLoginReq param) async {
    CUser result;
    final String url = baseUrl + '/deskuser/login';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("getUserLogin -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CUser.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<List<CUser>> getUserList(String p_SiteId) async {
    List<CUser> result = [];
    final String url = baseUrl + '/deskuser/login/list/' + p_SiteId;
    print(url);

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CUser.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<List<CUser>> getAllUserList() async {
    List<CUser> result = [];
    final String url = baseUrl + '/deskuser/login/list/all';
    print(url);

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CUser.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CUser> saveUser(CUserSaveReq param) async {
    CUser result;
    final url = baseUrl + '/deskuser/login/save';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("Save User -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CUser.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CUser> deleteUser(String p_userId) async {
    CUser result;
    final url = baseUrl + '/deskuser/login/delete/' + p_userId;

    try {
      final response = await http.delete(Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Accept": "application/json"
        },);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CUser.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CUser> changePassword(CUserResetReq param) async {
    CUser result;
    final url = baseUrl + '/deskuser/login/changePassword';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("change Password -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CUser.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

}