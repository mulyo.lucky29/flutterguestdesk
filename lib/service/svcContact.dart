import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:myggguestdesk/model/CContact.dart';
import 'package:myggguestdesk/model/CContactSaveReq.dart';

class svcContact extends ChangeNotifier {

  Future<List<CContact>> getListContactBySite(String strSiteCode) async {
    var url;
    List<CContact> result = [];
    url = baseUrl + '/contact/getContactSite/' + strSiteCode;

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        for (Map contact in responseJson) {
          result.add(CContact.fromJson(contact));
        }
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CContact> saveContact(CContactSaveReq param) async {
    CContact result;
    final url = baseUrl + '/contact/saveContact';

    var bodyValue = param.toJson();
    var bodyRequest = json.encode(bodyValue);
    print("setRegisterGuest -> " + bodyRequest.toString());

    try {
      final response = await http.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },
          body: bodyRequest);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CContact.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

  Future<CContact> deleteContact(String strContactId) async {
    CContact result;
    final url = baseUrl + '/contact/' + strContactId;

    try {
      final response = await http.delete(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          },);
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
        result = CContact.fromJson(responseJson);
        notifyListeners();
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }

}