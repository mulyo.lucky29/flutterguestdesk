
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:myggguestdesk/Global/Settings.dart';
import 'package:myggguestdesk/model/CVisitCounter.dart';


class svcVisitCounter extends ChangeNotifier {

  Future <CVisitCounter> getVisitCounterByDate(String p_siteId, String p_visitDate) async {
    CVisitCounter result;
    final String url = baseUrl + '/visitcounter/getVisitCounter/' + p_siteId + "/" + p_visitDate;
    print(url);

    try {
      final response = await http.get(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json"
          });
      final responseJson = json.decode(response.body);
      print(response.body.toString());

      if (response.statusCode == 200) {
         result = CVisitCounter.fromJson(responseJson);
      }
      else {
        result = null;
        throw Exception('Unable to fetch data from API');
      }
    }
    catch(err){
      result = null;
      throw err;
    }
    return result;
  }



}