
class valRegistration {

  String validateName(String value) {
    if (value.isEmpty) {
      return 'required';
    }
    else {
      return null;
    }
  }
  String validateNip(String value) {
    if (value.isEmpty) {
      return 'required';
    }
    /*
    else {
      if(value.length != 16) {
        return 'No KTP Tidak Valid (16 Digit)';
      }
      return null;
    }
    */
  }
  String validateCompany(String value) {
    if (value.isEmpty) {
      return 'required';
    }
    else {
      return null;
    }
  }
  String validatePhoneNo(String value) {
    if (value.isEmpty) {
      return 'required';
    }
    else {
      return null;
    }
  }
  String validateEmail(String value) {
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    if (value.isEmpty) {
      return 'required';
    }
    else {
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value)) {
        return 'Format Email Tidak Valid';
      }
      else {
        return null;
      }
    }
  }
}
